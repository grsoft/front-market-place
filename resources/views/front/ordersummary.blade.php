{{view('front/headmeta')}}
<body class="index-opt-1 catalog-product-view catalog-view_op1 page-order">
{{view('front/header')->with('catemenu',$catemenu)}}

		<!-- MAIN -->
  <main class="site-main">

            <div class="columns container-fluid">
                <!-- Block  Breadcrumb-->
                        
                <ol class="breadcrumb no-hide">
                    <li><a href="#">Home    </a></li>
                    <li class="active">Your Orders</li>
                </ol><!-- Block  Breadcrumb-->

                <h2 class="page-heading">
                    <span class="page-heading-title2">Order Summary</span>
                </h2>
                @if (session('success'))
                      <div class="alert alert-success">
                          {{ session('success') }}
                      </div>
                @endif
                <div class="page-content page-order">
                    
                    <div class="heading-counter warning">
                        <div class="row">
                            <div class="col-md-4">

                                <h3>Order Number<br> {{$o_data[0]->unique_no}}</h3>
                            </div>
                            <div class="col-md-4">
                                <h4>Shiping Address</h4>
                                {{$ship_data[0]->first_name_s}} {{$ship_data[0]->last_name_s}}
                                <br>{{$ship_data[0]->address_s}}<br>
                                City: {{$ship_data[0]->city_s}}<br>
                                State: {{$ship_data[0]->state_s}}<br>
                                Pin Code: {{$ship_data[0]->postal_code_s}}<br>
                                Country: {{$ship_data[0]->country_s}}<br>
                                Mob. No {{$ship_data[0]->mobile_no_s}}<br>
                                {{$ship_data[0]->email_address_s}}
                            </div>
                            <div class="col-md-4">
                                <h4>Billing Address</h4>
                                {{$o_data[0]->first_name}} {{$o_data[0]->last_name}}
                                <br>{{$o_data[0]->address}}<br>
                                City: {{$o_data[0]->city}}<br>
                                State: {{$o_data[0]->state}}<br>
                                Pin Code: {{$o_data[0]->postal_code}}<br>
                                Country: {{$o_data[0]->country}}<br>
                                Mob. No {{$o_data[0]->mobile_no}}<br>
                                {{$o_data[0]->email_address}}
                            </div>
                        </div>
                    </div>
                    <?php $total=0; ?>
                    <div class="order-detail-content">
                        <div class="table-responsive">
                            <table class="table table-bordered  cart_summary">
                                <thead>
                                    <tr>
                                        <th class="cart-description">Image</th>
                                        <th class="cart-product-name">Product Name</th>
                                        <th class="cart-price">Price</th>
                                        <th class="cart-qty">Quantity</th>
                                        <th class="cart-total">Grandtotal</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                    @foreach ($cdata as $data)
                                    <tr class="cartpage">
                                        <td class="cart-image">
                                            <a class="entry-thumbnail" href="javascript:void(0)">
                                                <img src="{{ asset('product_image/'.$data->image) }}" width="70px" alt="">
                                            </a>
                                        </td>
                                        <td class="cart-product-name-info">
                                            <h4 class='cart-product-description'>
                                                <a href="javascript:void(0)">{{ $data->title }}</a>
                                            </h4>
                                        </td>
                                        <td class="cart-product-sub-total">
                                            <span class="cart-sub-total-price">{{ number_format($data->amount, 2) }}</span>
                                        </td>
                                        <td class="cart-product-quantity">
                                            <input type="hidden" class="product_id" value="{{ $data->product_id }}">
                                            
                                            <input type="number" readonly class="changeQuantity qty-input form-control" value="{{ $data->quantity }}" min="1" max="100"/>                                                     
                                        </td>
                                        <td class="cart-product-grand-total">
                                            <span class="cart-grand-total-price">{{ number_format($data->quantity * $data->amount, 2) }}</span>
                                        </td>
                                        
                                        @php $total = $total + ($data->quantity * $data->amount) @endphp
                                    </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td rowspan="1" colspan="1"></td>
                                        <td colspan="3">Total products (tax incl.)</td>
                                        <td colspan="1">{{$total}}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="4"><strong>Total</strong></td>
                                        <td colspan="1"><strong>{{$total}}</strong></td>
                                    </tr>
                                </tfoot>    
                            </table>
                        </div>
                        <div class="cart_navigation">
                            <a href="#" class="prev-btn">Continue shopping</a>
                            
                        </div>
                    </div>
                </div>
                <br>
            </div>


    </main>
  <!-- end MAIN --> 
  {{view('front/footer')}}