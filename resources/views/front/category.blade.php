<?php $p=$product;?>
{{view('front/headmeta')}}
<body class="index-opt-1 catalog-product-view catalog-view_op1 page-order">
{{view('front/header')->with('catemenu',$catemenu)}}

		<!-- MAIN -->
  <main class="site-main">
    <div class="columns container-fluid"> 
      <!-- Block  Breadcrumb-->
      
      <ol class="breadcrumb no-hide">
        <li><a href="{{URL('/')}}">Home </a></li>
        <?php 
        $click=$hometree[count($hometree)-1];
        for($i=0;$i<count($hometree);$i++){ ?>
        <li><a href="{{URL('/shop/category')}}/{{$hometree_url[$i]}}">{{$hometree[$i]}} </a></li>
        <?php } ?>
        <!-- <li class="active">Machine Pro</li> -->
      </ol>
      <!-- Block  Breadcrumb-->
      
      <div class="row"> 
        
        <!-- Main Content -->
        <div class="col-md-9 col-md-push-3  col-main"> 
          
          <!-- images categori -->
          <div class="category-view">
            <div class="owl-carousel " 
                                data-nav="true" 
                                data-dots="false" 
                                data-margin="0" 
                                data-items='1' 
                                data-autoplayTimeout="700" 
                                data-autoplay="true" 
                                data-loop="true">
              <div class="item " > <a href=""><img src="{{URL::asset('front-html')}}/images/media/category-images1.jpg" alt="category-images"></a> </div>
              <div class="item " > <a href=""><img src="{{URL::asset('front-html')}}/images/media/category-images2.jpg" alt="category-images"></a> </div>
            </div>
          </div>
          <!-- images categori --> 
          <?php  
          //$results = DB::select( "SELECT * FROM master_category WHERE id = 1");
          //print_r($results);
          ?>
          <!-- link categori -->
          <ul class="category-links">
            <li class="current-cate"><a href="">All Products</a></li>
            <li><a href="">Tops</a></li>
            <li><a href="">Dresses</a></li>
            <li><a href="">Bags & Shoes</a></li>
            <li><a href="">Scaves</a></li>
            <li><a href="">Pants</a></li>
            <li><a href="">Blouses</a></li>
          </ul>
          <!-- link categori --> 
          
          <!-- Toolbar -->
          <div class=" toolbar-products toolbar-top">
            <div class="btn-filter-products"> <span>Filter</span> </div>
            <h1 class="cate-title">Electronics</h1>
            <div class="modes"> <strong  class="label">View as:</strong> <strong  class="modes-mode active mode-grid" title="Grid"> <span>grid</span> </strong> <a  href="Category2.html" title="List" class="modes-mode mode-list"> <span>list</span> </a> </div>
            <!-- View as -->
            
            <div class="toolbar-option">
              <div class="toolbar-sorter ">
                <label    class="label">Short by:</label>
                <select class="sorter-options form-control" >
                  <option selected="selected" value="position">Position</option>
                  <option value="name">Name</option>
                  <option value="price">Price</option>
                </select>
                <a href="" class="sorter-action"></a> </div>
              <!-- Short by -->
              
              <div class="toolbar-limiter">
                <label   class="label"> <span>Show:</span> </label>
                <select class="limiter-options form-control" >
                  <option selected="selected" value="9">Show 18</option>
                  <option value="15">Show 15</option>
                  <option value="30">Show 30</option>
                </select>
              </div>
              <!-- Show per page --> 
              
            </div>
            <ul class="pagination">
              <li class="action"> <a href="#"> <span><i aria-hidden="true" class="fa fa-angle-left"></i></span> </a> </li>
              <li class="active"> <a href="#">1</a> </li>
              <li> <a href="#">2</a> </li>
              <li> <a href="#">3</a> </li>
              <li class="action"> <a href="#"> <span><i aria-hidden="true" class="fa fa-angle-right"></i></span> </a> </li>
            </ul>
          </div>
          <!-- Toolbar --> 
          
          <!-- List Products -->
          <div class="products  products-grid">
            <ol class="product-items row" id="productitems">
              
            </ol>
            <!-- list product --> 
          </div>
          <!-- List Products --> 
          
          <!-- Toolbar -->
          <div class=" toolbar-products toolbar-bottom">
            <div class="modes"> <strong  class="label">View as:</strong> <strong  class="modes-mode active mode-grid" title="Grid"> <span>grid</span> </strong> <a  href="category.html" title="List" class="modes-mode mode-list"> <span>list</span> </a> </div>
            <!-- View as -->
            
            <div class="toolbar-option">
              <!-- <div class="toolbar-sorter ">
                <label    class="label">Short by:</label>
                <select class="sorter-options form-control" >
                  <option selected="selected" value="position">Product name</option>
                  <option value="name">Name</option>
                  <option value="price">Price</option>
                </select>
                <a href="" class="sorter-action"></a> </div> -->
              <!-- Short by -->
              
              <!-- <div class="toolbar-limiter">
                <label   class="label"> <span>Show:</span> </label>
                <select class="limiter-options form-control perpage" >
                  <option selected="selected" value="2"> Show 2</option>
                  <option value="4">Show 4</option>
                  <option value="8">Show 8</option>
                </select>
              </div>  --> 
              <!-- Show per page --> 
              
            </div>
            <ul class="pagination" id="pagination">
              
            </ul> 
          </div>
          <!-- Toolbar --> 
          
        </div>
        <!-- Main Content --> 
        
        <!-- Sidebar -->
        <div class="col-md-3 col-md-pull-9  col-sidebar"> 
          
          <!-- Block  bestseller products-->
          <div class="block-sidebar block-sidebar-categorie">
            <div class="block-title"> <strong>PRODUCT TYPES</strong> </div>
            <div class="block-content">
              <ul class="items">
                <li class="parent active"> <a href="">{{$click}}</a> <span class="toggle-submenu"></span>
                  
                  
                  <ul class="subcategory">
                        <?php 
                        if(isset($leftcat[$click])){
                        for($i=0;$i<count($leftcat[$click]);$i++){ ?>
                        <li ><a href="{{url('/shop/category')}}{{$leftcat[$click][$i][1]}}">{{$leftcat[$click][$i][2]}}</a></li>
                        <?php } } ?>
                  </ul>
                </li>
                
                <!-- <li> <a href="">Bags</a> </li> -->
                
              </ul>
            </div>
          </div>
          <!-- Block  bestseller products--> 
          
          <!-- block filter products -->
          <div id="layered-filter-block" class="block-sidebar block-filter no-hide">
            <div class="close-filter-products"><i class="fa fa-times" aria-hidden="true"></i></div>
            <div class="block-title"> <strong>FILTER SELECTION</strong> </div>
            <div class="block-content"> 
              
              <!-- Filter Item  categori-->
              <!-- <div class="filter-options-item filter-options-categori">
                <div class="filter-options-title">Categories</div>
                <div class="filter-options-content">
                  <ol class="items">
                    <li class="item ">
                      <label>
                        <input type="checkbox">
                        <span>Tops <span class="count">(20)</span></span> </label>
                    </li>
                    <li class="item ">
                      <label>
                        <input type="checkbox">
                        <span>Network & Computer <span class="count">(20)</span></span> </label>
                    </li>
                    <li class="item ">
                      <label>
                        <input type="checkbox">
                        <span>Batteries & Chargers <span class="count">(20)</span></span> </label>
                    </li>
                    <li class="item ">
                      <label>
                        <input type="checkbox">
                        <span>Dresses <span class="count">(20)</span></span> </label>
                    </li>
                    <li class="item ">
                      <label>
                        <input type="checkbox">
                        <span>Knitted <span class="count">(20)</span></span> </label>
                    </li>
                    <li class="item ">
                      <label>
                        <input type="checkbox">
                        <span>Pants <span class="count">(20)</span></span> </label>
                    </li>
                    <li class="item ">
                      <label>
                        <input type="checkbox">
                        <span>Best selling <span class="count">(20)</span></span> </label>
                    </li>
                    <li class="item ">
                      <label>
                        <input type="checkbox">
                        <span>Headphone & Headset <span class="count">(20)</span></span> </label>
                    </li>
                  </ol>
                </div>
              </div> -->
              <!-- Filter Item  categori--> 
              
              <!-- filter price -->
              <div class="filter-options-item filter-options-price">
                <div class="filter-options-title">Price</div>
                <div class="filter-options-content">
                  <div class="slider-range">
                    <div id="slider-range" ></div>
                    <div class="action"> <span class="price"> <span>Range:</span> $<span id="amount-left"></span> -
                      $<span id="amount-right"></span> </span> </div>
                  </div>
                  <?php
                  
                  $datamrp = DB::select("SELECT MAX(mrp) as maxmrp FROM `product` WHERE FIND_IN_SET('$catid',indicate_root) AND verify_status='1' AND status='1'");
                  $max= $datamrp[0]->maxmrp;
                  $maxlen=strlen($max);
                  $mv="1";
                  for($i=0;$i<$maxlen;$i++)
                  {
                    $mv=$mv."0";
                  }
                  $incres= $mv/5;
                  ?>
                  <ol class="items mrpitems">
                    <li class="item ">
                      <label>
                        <input type="checkbox" name="mrpsearch" class="mrp-search" value="0-<?= $incres ?>" onclick="onlyOnemrp(this)">
                        <span>$0 - $ <?= $incres ?>  <span class="count">(20)</span> </span> </label>
                    </li>
                    <li class="item ">
                      <label>
                        <input type="checkbox" name="mrpsearch" class="mrp-search" value="<?= $incres ?>-<?= $incres*2 ?>" onclick="onlyOnemrp(this)">
                        <span>$<?= $incres ?> - $<?= $incres*2 ?> <span class="count">(20)</span> </span> </label>
                    </li>
                    <li class="item ">
                      <label>
                        <input type="checkbox" name="mrpsearch" class="mrp-search" value="<?= $incres*2 ?>-<?= $incres*3 ?>" onclick="onlyOnemrp(this)">
                        <span>$<?= $incres*2 ?> - $<?= $incres*3 ?> <span class="count">(20)</span> </span> </label>
                    </li>
                    <li class="item ">
                      <label>
                        <input type="checkbox" name="mrpsearch" class="mrp-search" value="<?= $incres*3 ?>-<?= $incres*4 ?>" onclick="onlyOnemrp(this)">
                        <span>$<?= $incres*3 ?> - $<?= $incres*4 ?> <span class="count">(20)</span> </span> </label>
                    </li>
                    <li class="item ">
                      <label>
                        <input type="checkbox" name="mrpsearch" class="mrp-search" value="<?= $incres*4 ?>-<?= $incres*5 ?>" onclick="onlyOnemrp(this)">
                        <span>$<?= $incres*4 ?> - $<?= $incres*5 ?> <span class="count">(20)</span> </span> </label>
                    </li>
                  </ol>
                </div>
              </div>
              <!-- filter price --> 
              
              <!-- filter brad-->
              <div class="filter-options-item filter-options-brand">
                <div class="filter-options-title">BRAND</div>
                <?php
                $brand = DB::select("SELECT brand,COUNT(id) as qty FROM `product` WHERE FIND_IN_SET('$catid',indicate_root) AND verify_status='1' AND status='1'");
                // $brand = DB::table('product')
                // ->select(array('brand', DB::raw('COUNT(id) as row')))
                // ->where('category_id', '=', $catid)
                // ->get();
                
                
                ?>
                <div class="filter-options-content">
                  <ol class="items branditems">
                  @foreach ($brand as $key => $value)
                    <li class="item ">
                      <label>
                        <input type="checkbox" name="bransearch" class="brand-search" value="<?= $value->brand ?>" onclick="onlyOnebrand(this)">
                        <span>{{$value->brand}} <span class="count">({{ $value->qty}})</span> </span> </label>
                    </li>
                  @endforeach  
                  </ol>
                </div>
              </div>
              <div class="text-center"><p id="filtermsg" class="text-danger"></p> <button type="button" id="filter" class="btn  bg-danger">FILTER </button> </div>
              <!-- Filter Item --> 
              
              <!-- filter color-->
              <!-- <div class="filter-options-item filter-options-color">
                <div class="filter-options-title">COLOR</div>
                <div class="filter-options-content">
                  <ol class="items">
                    <li class="item">
                      <label>
                        <input type="checkbox">
                        <span> <span class="img" style="background-color: #fca53c;"></span> <span class="count">(30)</span> </span> </label>
                    </li>
                    <li class="item">
                      <label>
                        <input type="checkbox">
                        <span> <span class="img" style="background-color: #6b5a5c;"></span> <span class="count">(20)</span> </span> </label>
                    </li>
                    <li class="item">
                      <label>
                        <input type="checkbox">
                        <span> <span class="img" style="background-color: #000000;"></span> <span class="count">(20)</span> </span> </label>
                    </li>
                    <li class="item">
                      <label>
                        <input type="checkbox">
                        <span> <span class="img" style="background-color: #3063f2;"></span> <span class="count">(20)</span> </span> </label>
                    </li>
                    <li class="item">
                      <label>
                        <input type="checkbox">
                        <span> <span class="img" style="background-color: #f9334a;"></span> <span class="count">(20)</span> </span> </label>
                    </li>
                    <li class="item">
                      <label>
                        <input type="checkbox">
                        <span> <span class="img" style="background-color: #964b00;"></span> <span class="count">(20)</span> </span> </label>
                    </li>
                    <li class="item">
                      <label>
                        <input type="checkbox">
                        <span> <span class="img" style="background-color: #faebd7;"></span> <span class="count">(20)</span> </span> </label>
                    </li>
                    <li class="item">
                      <label>
                        <input type="checkbox">
                        <span> <span class="img" style="background-color: #e84c3d;"></span> <span class="count">(20)</span> </span> </label>
                    </li>
                    <li class="item">
                      <label>
                        <input type="checkbox">
                        <span> <span class="img" style="background-color: #fccacd;"></span> <span class="count">(20)</span> </span> </label>
                    </li>
                  </ol>
                </div>
              </div> -->
              <!-- Filter Item --> 
              
              <!-- Filter Item  size-->
              <!-- <div class="filter-options-item filter-options-size">
                <div class="filter-options-title">SIZE</div>
                <div class="filter-options-content">
                  <ol class="items">
                    <li class="item ">
                      <label>
                        <input type="checkbox">
                        <span>X <span class="count">(20)</span></span> </label>
                    </li>
                    <li class="item ">
                      <label>
                        <input type="checkbox">
                        <span>XXL <span class="count">(20)</span></span> </label>
                    </li>
                    <li class="item ">
                      <label>
                        <input type="checkbox">
                        <span>XXL <span class="count">(20)</span></span> </label>
                    </li>
                    <li class="item ">
                      <label>
                        <input type="checkbox">
                        <span>m <span class="count">(20)</span></span> </label>
                    </li>
                    <li class="item ">
                      <label>
                        <input type="checkbox">
                        <span>L <span class="count">(20)</span></span> </label>
                    </li>
                    <li class="item ">
                      <label>
                        <input type="checkbox">
                        <span>32 <span class="count">(20)</span></span> </label>
                    </li>
                    <li class="item ">
                      <label>
                        <input type="checkbox">
                        <span>36 <span class="count">(20)</span></span> </label>
                    </li>
                    <li class="item ">
                      <label>
                        <input type="checkbox">
                        <span>37 <span class="count">(20)</span></span> </label>
                    </li>
                    <li class="item ">
                      <label>
                        <input type="checkbox">
                        <span>X <span class="count">(20)</span></span> </label>
                    </li>
                    <li class="item ">
                      <label>
                        <input type="checkbox">
                        <span>XXL <span class="count">(20)</span></span> </label>
                    </li>
                    <li class="item ">
                      <label>
                        <input type="checkbox">
                        <span>XXL <span class="count">(20)</span></span> </label>
                    </li>
                    <li class="item ">
                      <label>
                        <input type="checkbox">
                        <span>m <span class="count">(20)</span></span> </label>
                    </li>
                  </ol>
                </div>
              </div> -->
              <!-- Filter Item  size--> 
              
            </div>
          </div>
          <!-- Filter --> 
          
          <!-- block slide top -->
          <div class="block-sidebar block-banner-sidebar">
            <div class="owl-carousel" 
                                data-nav="false" 
                                data-dots="true" 
                                data-margin="0" 
                                data-items='1' 
                                data-autoplayTimeout="700" 
                                data-autoplay="true" 
                                data-loop="true">
              <div class="item item1" > <img src="{{URL::asset('front-html')}}/images/media/banner-sidebar1.jpg" alt="images"> </div>
              <div class="item item2" > <img src="{{URL::asset('front-html')}}/images/media/banner-sidebar1.jpg" alt="images"> </div>
              <div class="item item3" > <img src="{{URL::asset('front-html')}}/images/media/banner-sidebar1.jpg" alt="images"> </div>
            </div>
          </div>
          <!-- block slide top --> 
          
          <!-- Block  bestseller products-->
          <!-- <div class="block-sidebar block-sidebar-products">
            <div class="block-title"> <strong>SPECIAL PRODUCTS</strong> </div>
            <div class="block-content">
              <div class="product-item product-item-opt-1">
                <div class="product-item-info">
                  <div class="product-item-photo"> <a class="product-item-img" href=""><img alt="product name" src="{{URL::asset('front-html')}}/images/media/floor5-1.jpg"></a> </div>
                  <div class="product-item-detail"> <strong class="product-item-name"><a href="">Security Camera Size Flared</a></strong>
                    <div class="clearfix">
                      <div class="product-item-price"> <span class="price">$45.00</span> </div>
                      <div class="product-reviews-summary">
                        <div class="rating-summary">
                          <div title="70%" class="rating-result"> <span style="width:70%"> <span><span>70</span>% of <span>100</span></span> </span> </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="text-center"> <a href="" class="btn all-products">All products <i aria-hidden="true" class="fa fa-angle-double-right"></i></a> </div>
            </div>
          </div> -->
          <!-- Block  bestseller products--> 
          
          <!-- Block  tags-->
          <!-- <div class="block-sidebar block-sidebar-tags">
            <div class="block-title"> <strong>product tags</strong> </div>
            <div class="block-content">
              <ul>
                <li><a href="" class="lv2">actual</a></li>
                <li><a href="" class="lv1">adorable</a></li>
                <li><a href="" class="lv3">amaze</a></li>
                <li><a href="" class="lv5">change</a></li>
                <li><a href="" class="lv2">consider</a></li>
                <li><a href="" class="lv1">delivery</a></li>
                <li><a href="" class="lv1">Top</a></li>
                <li><a href="" class="lv4">flexib</a></li>
                <li><a href="" class="lv1">phenomenon </a></li>
              </ul>
            </div>
          </div> -->
          <!-- Block  tags--> 
          
          <!-- block slide top -->
          <!-- <div class="block-sidebar block-sidebar-testimonials">
            <div class="block-title"> <strong>Testimonials</strong> </div>
            <div class="block-content">
              <div class="owl-carousel" 
                                    data-nav="false" 
                                    data-dots="true" 
                                    data-margin="0" 
                                    data-items='1' 
                                    data-autoplayTimeout="700" 
                                    data-autoplay="true" 
                                    data-loop="true">
                <div class="item " > <strong class="name">Roverto & Maria</strong>
                  <div class="avata"> <img src="{{URL::asset('front-html')}}/images/media/avata.jpg" alt="avata"> </div>
                  <div class="des"> "Your product needs to improve more. To suit the needs and update your image up" </div>
                </div>
                <div class="item " > <strong class="name">Roverto & Maria</strong>
                  <div class="avata"> <img src="{{URL::asset('front-html')}}/images/media/avata.jpg" alt="avata"> </div>
                  <div class="des"> "Your product needs to improve more. To suit the needs and update your image up" </div>
                </div>
                <div class="item " > <strong class="name">Roverto & Maria</strong>
                  <div class="avata"> <img src="{{URL::asset('front-html')}}/images/media/avata.jpg" alt="avata"> </div>
                  <div class="des"> "Your product needs to improve more. To suit the needs and update your image up" </div>
                </div>
              </div>
            </div>
          </div> -->
          <!-- block slide top --> 
          
        </div>
        <!-- Sidebar --> 
        
      </div>
    </div>
  </main>
  <!-- end MAIN --> 
  
  <!-- jQuery --> 
<script type="text/javascript" src="{{ URL::asset('front-html/js/jquery.min.js') }}"></script> 
  <script type="text/javascript" src="{{url::asset('jquery.twbsPagination.min.js')}}"></script>
  
  <script type="text/javascript">

    var result=[];
    result=<?php echo json_encode($p) ?>;
    var $pagination = $('#pagination');
    var totalRecords = 0;
    var records = [];
    var displayRecords = [];
    var recPerPage = 18;
    var page = 1;
    var totalPages = 0;
    records = result;
    totalRecords = records.length;
    totalPages = Math.ceil(totalRecords / recPerPage);
    apply_pagination();
    function generate_product() {
          var product;var disc="";
          $('#productitems').html('');
          if(displayRecords.length>0)
          {
          for (var i = 0; i < displayRecords.length; i++) {
                if(displayRecords[i].discount>0){
                disc=`<span class="product-item-label label-price">${displayRecords[i].discount}% <span>off</span></span>
                      `;
                }
                var product=`<li class="col-sm-4 product-item " >
                <div class="product-item-opt-1">
                  <div class="product-item-info">
                    <div class="product-item-photo"> <a href="{{URL('/shop/detail/')}}/${displayRecords[i].id}" class="product-item-img"><img src="{{URL::asset('product_image')}}/${displayRecords[i].image}" alt="product name"></a>
                      <div class="product-item-actions"> <a href="" class="btn btn-wishlist"><span>wishlist</span></a> <a href="" class="btn btn-compare"><span>compare</span></a> <a href="" class="btn btn-quickview"><span>quickview</span></a> </div>
                      <button class="btn btn-cart" data-pid="${displayRecords[i].id}" data-qty="1" type="button"><span>Add to Cart</span></button>
                       ${disc} 
                      </div>
                    <div class="product-item-detail"> <strong class="product-item-name"><a href="{{URL('/shop/detail/')}}/${displayRecords[i].id}">${displayRecords[i].title}</a></strong>
                      <div class="clearfix">
                        <div class="product-item-price"> <span class="price">${displayRecords[i].sell_price}</span> <span class="old-price">${displayRecords[i].mrp}</span> </div>
                        <div class="product-reviews-summary">
                          <div class="rating-summary">
                            <div class="rating-result" title="80%"> <span style="width:80%"> <span><span>80</span>% of <span>100</span></span> </span> </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </li>`;
                $('#productitems').append(product);
          }
          }else{
            var product=`<li class="col-sm-12 product-item " >Records not available....</li>`;
            $('#productitems').append(product);
          }
    }
    function apply_pagination() { console.log('inpagi'+totalRecords);
      $pagination.twbsPagination({
            totalPages: totalPages,
            visiblePages: 5,
            onPageClick: function (event, page) {
                  displayRecordsIndex = Math.max(page - 1, 0) * recPerPage;
                  endRec = (displayRecordsIndex) + recPerPage;
                 
                  displayRecords = records.slice(displayRecordsIndex, endRec);
                  generate_product();
            }
      });
    }
</script>
<script>
    $(document).ready(function(){
      $('#filter').click(function (e) { 
          e.preventDefault();
          var mrp_range='';
          var brand='';
          $('input.mrp-search:checkbox:checked').each(function () {
            mrp_range = $(this).val();
          });
          $('input.brand-search:checkbox:checked').each(function () {
            brand = $(this).val();
          });
          
          if(mrp_range.length > 0 || brand.length > 0)
          {
            $('#filtermsg').text("");
            $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
              });
            var data = {
                'mrp_range':mrp_range,
                'brand':brand,
                'cat_id':'<?= $catid ?>',
            };
            
             $.ajax({
                url: "{{URL('/filter-left')}}",
                type: 'POST',
                data: data,
                success: function (response) { 
                    records = response;
                    totalRecords = records.length;
                    totalPages = Math.ceil(totalRecords / recPerPage);
                    displayRecordsIndex = Math.max(page - 1, 0) * recPerPage;
                    endRec = (displayRecordsIndex) + recPerPage;
                 
                    displayRecords = records.slice(displayRecordsIndex, endRec);
                    generate_product();

                }
            }); 
          }
          else{
            window.location.reload();
            //$('#filtermsg').text("Filter selection required. check any condition");
          }
        });
      
    });
  </script>
  <script>
    function onlyOnemrp(checkbox) {
      var checkboxes = document.getElementsByName('mrpsearch')
      checkboxes.forEach((item) => {
          if (item !== checkbox) item.checked = false
      })
    }
    function onlyOnebrand(checkbox) {
      var checkboxes = document.getElementsByName('bransearch')
      checkboxes.forEach((item) => {
          if (item !== checkbox) item.checked = false
      })
    }
  </script>
{{view('front/footer')}}