{{view('front/headmeta')}}
<body class="index-opt-1 catalog-product-view catalog-view_op1 page-order">
{{view('front/header')->with('catemenu',$catemenu)}}

		<!-- MAIN -->
  <main class="site-main">

            <div class="columns container-fluid">
                <!-- Block  Breadcrumb-->
                        
                <ol class="breadcrumb no-hide">
                    <li><a href="#">Home    </a></li>
                    <li class="active">Your shopping cart</li>
                </ol><!-- Block  Breadcrumb-->

                <h2 class="page-heading">
                    <span class="page-heading-title2">Shopping Cart Summary</span>
                </h2>

                <div class="page-content page-order">
                    <ul class="step">
                        <li class="current-step"><span>01. Summary</span></li>
                        <li><span>02. Sign in</span></li>
                        <li><span>03. Address</span></li>
                        <li><span>04. Shipping</span></li>
                        <li><span>05. Payment</span></li>
                    </ul>
                    <div class="heading-counter warning">Your shopping cart contains:
                        <span><?php $total=0; if(!empty($cart)){ echo count($cart);}else{echo "0";} ?> Product</span>
                    </div>
                    <div class="order-detail-content">
                        <div class="table-responsive">
                            @if(!empty($cart))  
                            <table class="table table-bordered  cart_summary">
                                <thead>
                                    <tr>
                                        <th class="cart-description">Image</th>
                                        <th class="cart-product-name">Product Name</th>
                                        <th class="cart-price">Price</th>
                                        <th class="cart-qty">Quantity</th>
                                        <th class="cart-total">Grandtotal</th>
                                        <th class="cart-romove">Remove</th>
                                    </tr>
                                </thead>
                                <tbody>
                                 
                                    @foreach ($cart as $data)
                                    <tr class="cartpage">
                                        <td class="cart-image">
                                            <a class="entry-thumbnail" href="javascript:void(0)">
                                                <img src="{{ asset('product_image/'.$data['item_image']) }}" width="70px" alt="">
                                            </a>
                                        </td>
                                        <td class="cart-product-name-info">
                                            <h4 class='cart-product-description'>
                                                <a href="javascript:void(0)">{{ $data['item_name'] }}</a>
                                            </h4>
                                        </td>
                                        <td class="cart-product-sub-total">
                                            <span class="cart-sub-total-price">{{ number_format($data['item_price'], 2) }}</span>
                                        </td>
                                        <td class="cart-product-quantity">
                                            <input type="hidden" class="product_id" value="{{ $data['item_id'] }}">
                                            <!-- <input type="text" minlength="1" maxlength="12" name="qty0" id="qty0" value="{{$data['item_quantity']}}" class="form-control input-sm">
                                            <span  data-field="qty0" data-type="minus" class=" decrement-btn"><i class="fa fa-caret-up "></i></span>
                                            <span  data-field="qty0" data-type="plus" class=" increment-btn"><i class="fa fa-caret-down"></i></span> -->
                                            
                                            <input type="number" class="changeQuantity qty-input form-control" value="{{ $data['item_quantity'] }}" min="1" max="100"/>                                                     
                                        </td>
                                        <td class="cart-product-grand-total">
                                            <span class="cart-grand-total-price">{{ number_format($data['item_quantity'] * $data['item_price'], 2) }}</span>
                                        </td>
                                        <td style="font-size: 20px;">
                                            <button type="button" class="delete_cart_data"><li class="fa fa-trash-o"></li> Delete</button>
                                        </td>
                                        @php $total = $total + ($data["item_quantity"] * $data["item_price"]) @endphp
                                    </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td rowspan="2" colspan="2"></td>
                                        <td colspan="3">Total products (tax incl.)</td>
                                        <td colspan="2">{{$total}}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="3"><strong>Total</strong></td>
                                        <td colspan="2"><strong>{{$total}}</strong></td>
                                    </tr>
                                </tfoot>    
                            </table>
                            @endif
                        </div>
                        <div class="cart_navigation">
                            <a href="#" class="prev-btn">Continue shopping</a>
                            <?php if(Session()->has('cid')){?>
                                <a href="{{URL('/shop/checkout')}}" class="next-btn">Proceed to checkout</a>
                            <?php
                            }else{ ?>
                            <a href="{{URL('/shop/shop-login')}}" class="next-btn">Proceed to checkout</a>
                        <?php } ?>
                        </div>
                    </div>
                </div>
                <br>
            </div>


    </main>
  <!-- end MAIN --> 
  {{view('front/footer')}}