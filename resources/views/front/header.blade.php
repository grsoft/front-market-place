<div class="wrapper"> 
  
  <!-- HEADER -->
  <header class="site-header header-opt-2 cate-show"> 
    
    <!-- header-top -->
    <div class="header-top">
      <div class="container-fluid"> 
        
        <!-- hotline -->
        <ul class=" nav-left" >
          <li ><span><i class="fa fa-phone" aria-hidden="true"></i>+ 00 123 456 789</span></li>
          <li ><span><i class="fa fa-envelope" aria-hidden="true"></i> Contact us today !</span></li>	
        </ul>
        <!-- hotline --> 
        
        <!-- heder links -->
        <ul class="nav-right">
          <li class="dropdown setting"> <a data-toggle="dropdown" role="button" href="#" class="dropdown-toggle "><span>@if(Session::has('cid')){{Session('user_name')}}@else {{'My Account'}} @endif</span> <i aria-hidden="true" class="fa fa-angle-down"></i></a>
            <div class="dropdown-menu  ">
              @if(Session::has('cid'))
              
              <ul class="account">
                <li><a href="#">My Profile</a></li>
                <li><a href="#">Wishlist</a></li>
                <li><a href="{{URL('/shop/orderlist')}}">Checkout</a></li>
                <li><a href="{{URL('/shop/shop-customer-logout')}}">Log Out</a></li>
              </ul>
              @else
              <div class="switcher1  switcher-language"> <strong class="title">Get started now</strong>
                <ul class="switcher-options">
                  <li class="switcher-option"> <a href="{{URL('/shop/shop-login')}}"> Sign In</a></li>
                  <li class="switcher-option text-center">or</li>
                  <li class="switcher-option"> <a href="{{URL('/shop/shop-register')}}"> Join Free</a></li>                                                      
          
                </ul>
              </div>
              <!-- <div class="switcher  switcher-currency"> <strong class="title">Continue with:</strong>
                <ul class="switcher-options ">
                  <li class="switcher-option"> <a href="#"> <img src="{{URL::asset('front-html')}}/images/fb.svg"></a> </li>
                  <li class="switcher-option"> <a href="#"> <img src="{{URL::asset('front-html')}}/images/googel.svg"></a> </li>
                  <li class="switcher-option"> <a href="#"> <img src="{{URL::asset('front-html')}}/images/in.svg"></a> </li>
                  <li class="switcher-option"> <a href="#"> <img src="{{URL::asset('front-html')}}/images/tw.svg"></a> </li>
                </ul>
              </div> -->
              @endif
            </div>
          </li>
          <li><a href="{{URL::asset('shop/seller')}}">Seller of market-place</a></li>
          <!-- <li><a href="#">Services </a></li> -->
        </ul>
        <!-- heder links --> 
        
      </div>
    </div>
    <!-- header-top --> 
    
    <!-- header-content -->
    <div class="header-content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3 nav-left"> 
            <!-- logo --> 
            <strong class="logo"> <a href="{{URL('/')}}"><img src="{{URL::asset('front-html')}}/images/logo.png" width="200" alt="logo"></a> </strong><!-- logo --> 
          </div>
          <div class=" nav-right"> 
            
            <!-- link  wishlish--> 
            <!-- link  wishlish--> 
            
            <!-- link  wishlish--> 
            <!-- <a href="wishList.html" class="link-wishlist"><span>wishlish</span></a>  -->
            <!-- link  wishlish--> 
            
            <!-- block mini cart -->
            <div class="block-minicart dropdown"> <a class="dropdown-toggle" href="{{URL('/shop/shoping_cart')}}" role="button" data-toggle="dropdown"> <span class="cart-icon"></span> <span class="cart-text">cart</span> <span class="counter qty"> <span class="counter-number">0</span> <span class="counter-label">0 <span>Item(s)</span></span> <span class="counter-price">00</span> </span> </a>
              <div class="dropdown-menu">
                <form >
                  <div  class="minicart-content-wrapper">
                    <div class="subtitle"> You have <span id="totalitem">0</span> item(s) in your cart </div>

                    <div class="minicart-items-wrapper">
                      <ol class="minicart-items" id="cartitem">
                        
                      </ol>
                    </div>
                    <div class="cartdiv">
                    <div class="subtotal"> <span class="label">Total</span> <span class="price" id="cart_total">0.00</span> </div>
                    <div class="actions"> 
                      <a class="btn btn-viewcart" href="{{URL('/shop/shoping_cart')}}">
                        <span>Shopping Cart</span>
                      </a> 

                      <?php if(Session()->has('cid')){?>
                              <a href="{{URL('/shop/checkout')}}" class="btn btn-checkout">Checkout</a>
                          <?php
                          }else{ ?>
                          <a href="{{URL('/shop/shop-login')}}" class="btn btn-checkout">Checkout</a>
                      <?php } ?>
                      
                    </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            <!-- block mini cart --> 
            
          </div>
          <div class=" nav-mind"> 
            
            <!-- block search -->
            <div class="block-search">
              <div class="block-title"> <span>Search</span> </div>
              <div class="block-content">
                <form action="{{URL('/shop/search')}}">
                  <div class="form-search">
                  
                    <div class="box-group">
                      <input type="text" class="form-control" name="title" placeholder="Search here...">
                      <button class="btn btn-search" type="submit"><span>search</span></button>
                    </div>
                  
                </div>
                <!-- <div class="categori-search  ">
                  <select data-placeholder="All Categories" class="categori-search-option">
                   
                    <option value="all">All Categories</option>
                    @foreach($catemenu[0] as $key=> $value)
                      <option value="{{$catemenu[0][$key]->id}}">{{$catemenu[0][$key]->title}}</option>
                    @endforeach
                  </select>
                </div> -->
                </form>
              </div>
            </div>
            <!-- block search --> 
            
          </div>
        </div>
      </div>
    </div>
    <!-- header-content -->

    <?php //echo "<pre>"; print_r($catemenu[0][0]->title); exit;?>
    <div class="header-nav mid-header">

        <div class="box-header-nav"> <span data-action="toggle-nav-cat" class="nav-toggle-menu nav-toggle-cat"><span>Categories</span><i aria-hidden="true" class="fa fa-bars"></i></span> 
          
          {{view('front/sidemenu')->with('catemenu',$catemenu)}}
          
          <!-- menu -->
          <div class="block-nav-menu">
            <div class="clearfix"><span data-action="close-nav" class="close-nav"><span>close</span></span></div>
            <ul class="ui-menu">
              <li> <a href="#">New Buyer Rewards</a></li>
			  <li class="parent parent-submenu"> <a href="Contact.html"> Sell on Afremarkete </a> <span class="toggle-submenu"></span>
                <div class="submenu drop-menu">
                  <ul>
                    <li><a href="#">For Global Seller</a></li>
                    <li><a href="#">For USA Seller</a></li>
                    <li><a href="#">For India Seller</a></li>
                    <li><a href="#">For UK Seller</a></li>
                    <li><a href="#">Partner Program</a></li>
                  </ul>
                </div>
              </li>
			  <li class="parent parent-submenu"> <a href="Contact.html"> Help's </a> <span class="toggle-submenu"></span>
                <div class="submenu drop-menu">
                  <ul>
                    <li><a href="#">For Buyers</a></li>
                    <li><a href="#">For Suppliers</a></li>
                    <li><a href="#">Submit a Dispute</a></li>
                    <li><a href="#">Report IPR Infringement</a></li>
                    <li><a href="#">Report Abuse</a></li>
                  </ul>
                </div>
              </li>
            </ul>
          </div>
          <!-- menu --> 
          
          <span data-action="toggle-nav" class="nav-toggle-menu"><span>Menu</span><i aria-hidden="true" class="fa fa-bars"></i></span>
          <div class="block-minicart dropdown "> <a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown"> <span class="cart-icon"></span> </a>
            <div class="dropdown-menu">
                <form >
                  <div  class="minicart-content-wrapper">
                    <div class="subtitle"> You have <span id="totalitems">0</span> item(s) in your cart </div>

                    <div class="minicart-items-wrapper">
                      <ol class="minicart-items" id="cartitems">
                        
                      </ol>
                    </div>
                    <div class="cartdivs">
                    <div class="subtotal"> <span class="label">Total</span> <span class="price" id="cart_totals">0.00</span> </div>
                    <div class="actions"> 
                      <a class="btn btn-viewcart" href="{{URL('/shop/shoping_cart')}}">
                        <span>Shopping Cart</span>
                      </a> 

                      <?php if(Session()->has('cid')){?>
                              <a href="{{URL('/shop/checkout')}}" class="btn btn-checkout">Checkout</a>
                          <?php
                          }else{ ?>
                          <a href="{{URL('/shop/shop-login')}}" class="btn btn-checkout">Checkout</a>
                      <?php } ?>
                      
                    </div>
                    </div>
                  </div>
                </form>
              </div>
          </div>
          <div class="block-search">
            <div class="block-title"> <span>Search</span> </div>
            <div class="block-content">
              <form action="{{URL('/shop/search')}}">
                <div class="form-search">
                
                  <div class="box-group">
                    <input type="text" class="form-control" name="title" placeholder="Search here...">
                    <button class="btn btn-search" type="submit"><span>search</span></button>
                  </div>
                  
                </div>
                <!-- <div class="categori-search  ">
                  <select data-placeholder="All Categories" class="categori-search-option">
                   
                    <option value="all">All Categories</option>
                    @foreach($catemenu[0] as $key=> $value)
                      <option value="{{$catemenu[0][$key]->id}}">{{$catemenu[0][$key]->title}}</option>
                    @endforeach
                  </select>
                </div> -->
              </form>
            </div>  
            </div>
          </div>
          <div class="dropdown setting"> <a data-toggle="dropdown" role="button" href="#" class="dropdown-toggle "><span>Settings</span> <i aria-hidden="true" class="fa fa-user"></i></a>
            <div class="dropdown-menu  ">
              @if(Session::has('cid'))
              
              <ul class="account">
                <li><a href="#">My Profile</a></li>
                <li><a href="#">Wishlist</a></li>
                <li><a href="{{URL('/shop/orderlist')}}">Checkout</a></li>
                <li><a href="{{URL('/shop/shop-customer-logout')}}">Log Out</a></li>
              </ul>
              @else
              <div class="switcher1  switcher-language"> <strong class="title">Get started now</strong>
                <ul class="switcher-options">
                  <li class="switcher-option"> <a href="{{URL('/shop/shop-login')}}"> Sign In</a></li>
                  <li class="switcher-option text-center">or</li>
                  <li class="switcher-option"> <a href="{{URL('/shop/shop-register')}}"> Join Free</a></li>                                                      
          
                </ul>
              </div>
              <!-- <div class="switcher  switcher-currency"> <strong class="title">Continue with:</strong>
                <ul class="switcher-options ">
                  <li class="switcher-option"> <a href="#"> <img src="{{URL::asset('front-html')}}/images/fb.svg"></a> </li>
                  <li class="switcher-option"> <a href="#"> <img src="{{URL::asset('front-html')}}/images/googel.svg"></a> </li>
                  <li class="switcher-option"> <a href="#"> <img src="{{URL::asset('front-html')}}/images/in.svg"></a> </li>
                  <li class="switcher-option"> <a href="#"> <img src="{{URL::asset('front-html')}}/images/tw.svg"></a> </li>
                </ul>
              </div> -->
              @endif
            </div>
          </div>
        </div>

    </div>
  </header>
  <!-- end HEADER --> 