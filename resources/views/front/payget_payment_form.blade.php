{{view('front/headmeta')}}
<body class="index-opt-1 catalog-product-view catalog-view_op1 page-order">


    <!-- MAIN -->
  <main class="site-main">
    <div class="columns container-fluid"> 
      <!-- Block  Breadcrumb-->
      
      <div class="page-content checkout-page">
        
        <div class="box-border">
          <div class="row">
            
            <div class="col-sm-12 text-center">
              <form action="https://secure.paygate.co.za/payweb3/process.trans" method="POST" >
                <h3 class="checkout-sep">Payment Process.....</h3>
                <input type="hidden" name="PAYGATE_ID" value="{{$option['PAYGATE_ID']}}">
                <input type="hidden" name="PAY_REQUEST_ID" value="{{$option['PAY_REQUEST_ID']}}">
                <input type="hidden" name="CHECKSUM" value="{{$option['CHECKSUM']}}">
                <img src="{{asset('front-html/loading-buffering.gif')}}">
                <br>
                <div style="display: none;">
                <button id="continue" type="submit" class="button"><h3>Continue</h3></button>
                </div>
              </form>
              
            </div>
          </div>
        </div>
        
      </div>
    </div>
  </main>
  <!-- end MAIN --> 
  <script type="text/javascript">
    document.getElementById("continue").click();
  </script>