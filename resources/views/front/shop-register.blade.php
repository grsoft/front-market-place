{{view('front/headmeta')}}
<body class="index-opt-1 catalog-product-view catalog-view_op1 page-order">
{{view('front/header')->with('catemenu',$catemenu)}}

		<!-- MAIN -->
  <main class="site-main">
    <div class="columns container-fluid"> 
      <!-- Block  Breadcrumb-->
      
      <ol class="breadcrumb no-hide">
        <li><a href="#">Home </a></li>
        <li class="active"> Registration</li>
      </ol>
      <!-- Block  Breadcrumb-->
      
      <h2 class="page-heading"> <span class="page-heading-title2"> New Registration </span> </h2>
      <div class="page-content checkout-page">
        <form method="post" action="{{URL('/shop/shop-register-save')}}" autocomplete="off">
        @csrf
        
        <h3 class="checkout-sep">1. Billing Infomations</h3>
        <div class="box-border">
          <ul>
            <li class="row">
              <div class="col-sm-6">
                <label for="first_name" class="required">First Name</label>
                <input class="input form-control" name="first_name" id="first_name" type="text" value="{{old('first_name')}}">
                @error('first_name')
                <label class="text-danger">{{ $message }}</label>
                @enderror
              </div>
              <div class="col-sm-6">
                <label for="last_name" class="required">Last Name</label>
                <input name="last_name" class="input form-control" id="last_name" type="text" value="{{old('last_name')}}">
                @error('last_name')
                <label class="text-danger">{{ $message }}</label>
                @enderror
              </div>
            </li>

            <li class="row">
              
              <div class="col-sm-6">
                <label for="email_address" class="required">Email Address</label>
                <input class="input form-control" name="email_address" id="email_address" type="text" autocomplete="off" value="{{old('email_address')}}">                
                @error('email_address')
                <label class="text-danger">{{ $message }}</label>
                @enderror
              </div>
            </li>
            <li class="row">
              <div class="col-sm-6">
                <label for="password" class="required">Password</label>
                <input class="input form-control" name="password" id="password" type="password" autocomplete="off" value="{{old('password')}}">
                @error('password')
                <label class="text-danger">{{ $message }}</label>
                @enderror
              </div>
              <div class="col-sm-6">
                <label for="confirm" class="required">Confirm Password</label>
                <input class="input form-control" name="confirm" id="confirm" type="password" value="{{old('confirm')}}">
                <label id="cpass" class="text-danger" style="display: none;">Password don't matchs.</label>
                @error('confirm')
                <label class="text-danger">{{ $message }}</label>
                @enderror
              </div>
            </li>
            <li class="row">
              <div class="col-xs-12">
                <label for="address" class="required">Address</label>
                <input class="input form-control" name="address" id="address" type="text" value="{{old('address')}}">
                @error('address')
                <label class="text-danger">{{ $message }}</label>
                @enderror
              </div>
            </li>
            <li class="row">
              <div class="col-sm-6">
                <label for="city" class="required">City</label>
                <input class="input form-control" name="city" id="city" type="text" value="{{old('city')}}">
                @error('address')
                <label class="text-danger">{{ $message }}</label>
                @enderror
              </div>
              <div class="col-sm-6">
                <label class="required">State/Province</label>
                <input type="text" class="input form-control" name="state" id="state" value="{{old('state')}}">
                @error('state')
                <label class="text-danger">{{ $message }}</label>
                @enderror  
              </div>
            </li>
            <li class="row">
              <div class="col-sm-6">
                <label for="postal_code" class="required">Zip/Postal Code</label>
                <input class="input form-control" name="postal_code" id="postal_code" type="text" value="{{old('postal_code')}}">
                @error('postal_code')
                <label class="text-danger">{{ $message }}</label>
                @enderror
              </div>
              <div class="col-sm-6">
                <label class="required">Country</label>
                <input type="text" class="input form-control" name="country" id="country" value="{{old('country')}}">
                @error('country')
                <label class="text-danger">{{ $message }}</label>
                @enderror
              </div>
            </li>
            <li class="row">
              <div class="col-sm-6">
                <label for="Mobile Number" class="required">Mobile Number</label>
                <input class="input form-control" name="mobile_no" id="mobile_no" type="text" value="{{old('mobile_no')}}">
                @error('mobile_no')
                <label class="text-danger">{{ $message }}</label>
                @enderror
              </div>
              
            </li>
            
          </ul>
        </div>
        <h3 class="checkout-sep">2. Shipping Information </h3>
        <label><input type="checkbox" id="sb" class="" style="visibility: visible;"> Same as Billing Information</label>
        <div class="box-border">
          <ul>
            <li class="row">
              <div class="col-sm-6">
                <label for="first_name_1" class="required">First Name</label>
                <input class="input form-control" name="first_name_s" id="first_name_s" type="text" value="{{old('first_name_s')}}">
                @error('first_name_s')
                <label class="text-danger">{{ $message }}</label>
                @enderror
              </div>
              <div class="col-sm-6">
                <label for="last_name_1" class="required">Last Name</label>
                <input class="input form-control" name="last_name_s" id="last_name_s" type="text" value="{{old('last_name_s')}}">
                @error('last_name_s')
                <label class="text-danger">{{ $message }}</label>
                @enderror
              </div>
            </li>
            <li class="row">
              
              <div class="col-sm-6">
                <label for="email_address_1" class="required">Email Address</label>
                <input class="input form-control" name="email_address_s" id="email_address_s" type="text" value="{{old('email_address_s')}}">
                @error('email_address_s')
                <label class="text-danger">{{ $message }}</label>
                @enderror
              </div>
            </li>
            <li class="row">
              <div class="col-xs-12">
                <label for="address_1" class="required">Address</label>
                <input class="input form-control" name="address_s" id="address_s" type="text" value="{{old('address_s')}}">
                @error('address_s')
                <label class="text-danger">{{ $message }}</label>
                @enderror
              </div>
            </li>
            <li class="row">
              <div class="col-sm-6">
                <label for="city_1" class="required">City</label>
                <input class="input form-control" name="city_s" id="city_s" type="text" value="{{old('city_s')}}">
                @error('city_s')
                <label class="text-danger">{{ $message }}</label>
                @enderror
              </div>
              <div class="col-sm-6">
                <label class="required">State/Province</label>
                <div class="custom_select">
                  <input type="text" class="input form-control" id="state_s" name="state_s" value="{{old('state_s')}}">
                  @error('state_s')
                  <label class="text-danger">{{ $message }}</label>
                  @enderror 
                </div>
              </div>
            </li>
            <li class="row">
              <div class="col-sm-6">
                <label for="postal_code_1" class="required">Zip/Postal Code</label>
                <input class="input form-control" name="postal_code_s" id="postal_code_s" type="text" value="{{old('postal_code_s')}}">
                @error('postal_code_s')
                <label class="text-danger">{{ $message }}</label>
                @enderror
              </div>
              <div class="col-sm-6">
                <label class="required">Country</label>
                <div class="custom_select">
                  <input type="text" class="input form-control" name="country_s" id="country_s" value="{{old('country_s')}}">
                  @error('country_s')
                <label class="text-danger">{{ $message }}</label>
                @enderror  
                </div>
              </div>
            </li>
            <li class="row">
              <div class="col-sm-6">
                <label for="Mobile Number" class="required">Mobile Number</label>
                <input class="input form-control" name="mobile_no_s" id="mobile_no_s" type="text" value="{{old('mobile_no_s')}}">
                @error('mobile_no_s')
                <label class="text-danger">{{ $message }}</label>
                @enderror 
              </div>
              
            </li>
          </ul>
          <button type="submit" class="button">Continue</button>
        </div>
        </form>  
      </div>
    </div>
  </main>
  <!-- end MAIN --> 
  {{view('front/footer')}}
  <script type="text/javascript">
    $('#sb').click(function(){
      if($('#sb').prop("checked")){
        $('#first_name_s').val($('#first_name').val());
        $('#last_name_s').val($('#last_name').val());
        $('#email_address_s').val($('#email_address').val());
        $('#address_s').val($('#address').val());
        $('#city_s').val($('#city').val());
        $('#state_s').val($('#state').val());
        $('#postal_code_s').val($('#postal_code').val());
        $('#country_s').val($('#country').val());
        $('#mobile_no_s').val($('#mobile_no').val());
        
      }
      else{
        $('#first_name_s').val('');
        $('#last_name_s').val('');
        $('#email_address_s').val('');
        $('#address_s').val('');
        $('#city_s').val('');
        $('#state_s').val('');
        $('#postal_code_s').val('');
        $('#country_s').val('');
        $('#mobile_no_s').val('');
      }
    });
    $('#confirm').on('keyup',function(){
      let pass=$('#password').val();
      let cpass=$('#confirm').val();
      if(pass==cpass){
        $('#cpass').hide();
      }else{
        $('#cpass').show();
      }
    })
  </script>