{{view('front/headmeta')}}
<body class="index-opt-1 catalog-product-view catalog-view_op1 page-order">
{{view('front/header')->with('catemenu',$catemenu)}}

		<!-- MAIN -->
  <main class="site-main">
    <div class="columns container-fluid"> 
      <!-- Block  Breadcrumb-->
      
      <ol class="breadcrumb no-hide">
        <li><a href="#">Home </a></li>
        <li class="active"> Checkout</li>
      </ol>
      <!-- Block  Breadcrumb-->
      
      <h2 class="page-heading"> <span class="page-heading-title2"> Checkout</span> </h2>
      <h3 class="checkout-sep">* Shipping Information</h3>
        <div class="box-border">
          <ul>
            <li class="row">
              <div class="col-sm-6">
                {{$cust_data[0]->first_name_s}} {{$cust_data[0]->last_name_s}}
                <br>{{$cust_data[0]->address_s}}<br>
                City: {{$cust_data[0]->city_s}}<br>
                State: {{$cust_data[0]->state_s}}<br>
                Pin Code: {{$cust_data[0]->postal_code_s}}<br>
                Country: {{$cust_data[0]->country_s}}<br>
                Mob. No {{$cust_data[0]->mobile_no_s}}<br>
                {{$cust_data[0]->email_address_s}}
              </div>
            </li>
            
          </ul>
          
        </div>
        
        
        <h3 class="checkout-sep">* Order Review</h3>
        <div class="order-detail-content">
          <div class="table-responsive">
              <table class="table table-bordered  cart_summary">
                  <thead>
                      <tr>
                          <th class="cart-description">Image</th>
                          <th class="cart-product-name">Product Name</th>
                          <th class="cart-price">Price</th>
                          <th class="cart-qty">Quantity</th>
                          <th class="cart-total">Grandtotal</th>
                          <th class="cart-romove">Stock</th>
                      </tr>
                  </thead>
                  <tbody>
                      @php $total=0; @endphp
                      @foreach ($cart as $data)
                      <tr class="cartpage">
                          <td class="cart-image">
                              <a class="entry-thumbnail" href="javascript:void(0)">
                                  <img src="{{ asset('product_image/'.$data['item_image']) }}" width="70px" alt="">
                              </a>
                          </td>
                          <td class="cart-product-name-info">
                              <h4 class='cart-product-description'>
                                  <a href="javascript:void(0)">{{ $data['item_name'] }}</a>
                              </h4>
                          </td>
                          <td class="cart-product-sub-total">
                              <span class="cart-sub-total-price">{{ number_format($data['item_price'], 2) }}</span>
                          </td>
                          <td class="cart-product-quantity">
                              <input type="hidden" class="product_id" value="{{ $data['item_id'] }}">
                              <label>{{$data['item_quantity']}}</label>
                               <!-- <input type="text" minlength="1" maxlength="12" name="qty0" id="qty0" value="{{$data['item_quantity']}}" class="form-control input-sm"> -->
                              <!--<span  data-field="qty0" data-type="minus" class=" decrement-btn"><i class="fa fa-caret-up "></i></span>
                              <span  data-field="qty0" data-type="plus" class=" increment-btn"><i class="fa fa-caret-down"></i></span> -->
                              <!-- <input type="number" class="changeQuantity qty-input form-control" value="{{ $data['item_quantity'] }}" min="1" max="100"/> -->                                                     
                          </td>
                          <td class="cart-product-grand-total">
                              <span class="cart-grand-total-price">{{ number_format($data['item_quantity'] * $data['item_price'], 2) }}</span>
                          </td>
                          <td style="font-size: 20px;">
                              In Stock
                          </td>
                          @php $total = $total + ($data["item_quantity"] * $data["item_price"]) @endphp
                      </tr>
                      @endforeach
                  </tbody>
                  <tfoot>
                      <tr>
                          <td rowspan="2" colspan="2"></td>
                          <td colspan="3">Total products (tax incl.)</td>
                          <td colspan="2">{{$total}}</td>
                      </tr>
                      <tr>
                          <td colspan="3"><strong>Total</strong></td>
                          <td colspan="2"><strong>{{$total}}</strong></td>
                      </tr>
                  </tfoot>    
              </table>
          </div>
          <form method="post" action="{{URL('/shop/create_order')}}">
            @csrf
        
              <input class="input form-control" name="first_name_s" id="first_name_s" type="hidden" value="{{$cust_data[0]->first_name_s}}">
              <input class="input form-control" name="last_name_s" id="last_name_s" type="hidden" value="{{$cust_data[0]->last_name_s}}">
              <input class="input form-control" name="email_address_s" id="email_address_s" type="hidden" value="{{$cust_data[0]->email_address_s}}">
              <input class="input form-control" name="address_s" id="address_s" type="hidden" value="{{$cust_data[0]->address_s}}">
              <input class="input form-control" name="city_s" id="city_s" type="hidden" value="{{$cust_data[0]->city_s}}">
                  <input type="hidden" class="input form-control" id="state_s" name="state_s" value="{{$cust_data[0]->state_s}}">
                
              <input class="input form-control" name="postal_code_s" id="postal_code_s" type="hidden" value="{{$cust_data[0]->postal_code_s}}">
                  <input type="hidden" class="input form-control" name="country_s" id="country_s" value="{{$cust_data[0]->country_s}}">
                
              <input class="input form-control" name="mobile_no_s" id="mobile_no_s" type="hidden" value="{{$cust_data[0]->mobile_no_s}}">
               
            </ul>
            
          </div>
          <h3 class="checkout-sep">* Payment Information</h3>
          <div class="box-border">
            <ul>
              <li>
                <label for="radio_button_5">
                  <input checked="" name="pay_method" id="radio_button_5" type="radio" value="cod">
                  Cash on Delevery</label>
              </li>
              <li>
                <label for="radio_button_6">
                  <input name="pay_method" id="radio_button_6" type="radio" value="online">
                  Credit card / Debit card</label>
              </li>
            </ul>
            
          </div>
          <div class="cart_navigation">
              <a href="#" class="prev-btn">Continue shopping</a>
              <button type="submit"class="btn btn-success">Place Order</button>
          </div>
          </form>
          
        </div>
        
      </div>
    </div>
  </main>
  <form style="display: none;">
    <h3 class="checkout-sep">* Shipping Information</h3>
        <div class="box-border">
          <ul>
            <li class="row">
              <div class="col-sm-6">
                <label for="first_name_1" class="required">First Name</label>
                <input class="input form-control" name="first_name_s" id="first_name_s" type="text" value="{{$cust_data[0]->first_name_s}}">
                @error('first_name_s')
                <label class="text-danger">{{ $message }}</label>
                @enderror
              </div>
              <div class="col-sm-6">
                <label for="last_name_1" class="required">Last Name</label>
                <input class="input form-control" name="last_name_s" id="last_name_s" type="text" value="{{$cust_data[0]->last_name_s}}">
                @error('last_name_s')
                <label class="text-danger">{{ $message }}</label>
                @enderror
              </div>
            </li>
            <li class="row">
              
              <div class="col-sm-6">
                <label for="email_address_1" class="required">Email Address</label>
                <input class="input form-control" name="email_address_s" id="email_address_s" type="text" value="{{$cust_data[0]->email_address_s}}">
                @error('email_address_s')
                <label class="text-danger">{{ $message }}</label>
                @enderror
              </div>
            </li>
            <li class="row">
              <div class="col-xs-12">
                <label for="address_1" class="required">Address</label>
                <input class="input form-control" name="address_s" id="address_s" type="text" value="{{$cust_data[0]->address_s}}">
                @error('address_s')
                <label class="text-danger">{{ $message }}</label>
                @enderror
              </div>
            </li>
            <li class="row">
              <div class="col-sm-6">
                <label for="city_1" class="required">City</label>
                <input class="input form-control" name="city_s" id="city_s" type="text" value="{{$cust_data[0]->city_s}}">
                @error('city_s')
                <label class="text-danger">{{ $message }}</label>
                @enderror
              </div>
              <div class="col-sm-6">
                <label class="required">State/Province</label>
                <div class="custom_select">
                  <input type="text" class="input form-control" id="state_s" name="state_s" value="{{$cust_data[0]->state_s}}">
                  @error('state_s')
                  <label class="text-danger">{{ $message }}</label>
                  @enderror 
                </div>
              </div>
            </li>
            <li class="row">
              <div class="col-sm-6">
                <label for="postal_code_1" class="required">Zip/Postal Code</label>
                <input class="input form-control" name="postal_code_s" id="postal_code_s" type="text" value="{{$cust_data[0]->postal_code_s}}">
                @error('postal_code_s')
                <label class="text-danger">{{ $message }}</label>
                @enderror
              </div>
              <div class="col-sm-6">
                <label class="required">Country</label>
                <div class="custom_select">
                  <input type="text" class="input form-control" name="country_s" id="country_s" value="{{$cust_data[0]->country_s}}">
                  @error('country_s')
                <label class="text-danger">{{ $message }}</label>
                @enderror  
                </div>
              </div>
            </li>
            <li class="row">
              <div class="col-sm-6">
                <label for="Mobile Number" class="required">Mobile Number</label>
                <input class="input form-control" name="mobile_no_s" id="mobile_no_s" type="text" value="{{$cust_data[0]->mobile_no_s}}">
                @error('mobile_no_s')
                <label class="text-danger">{{ $message }}</label>
                @enderror 
              </div>
              
            </li>
          </ul>
          <!-- <button type="submit" class="button">Update Shipping Deatails</button> -->
        </div>
  </form>
  <!-- end MAIN --> 
  {{view('front/footer')}}