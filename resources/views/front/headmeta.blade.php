<!DOCTYPE html>
<html lang="en">
<head>
<?php if(isset($page)){ ?>	
<title>{{$page}}</title>
<?php }else{ ?>
	<title>Market-Place</title>
<?php } ?>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="csrf-token" content="{{ csrf_token() }}" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name=description content=" Afremarkete is India's largest online marketplace that help manufacturers, suppliers & exporters to trade with each other at a common, reliable & transparent platform. Largest free online business directory & yellow page with listing of 50,000 Indian & International companies. ">
<meta name=keywords content=" Business directory, business directory in india, business e-commerce, business listings, business website, business marketplace, electronic trade & commerce, electronic trade and commerce, exporter importer directory, exporters business directory ">
<!-- Style CSS -->
<link rel="stylesheet" type="text/css" href="{{URL::asset('front-html/css/style.css') }}">
<link rel="stylesheet" href="{{URL::asset('front-html/alertifyjs/css/alertify.min.css') }}" />
<link rel="stylesheet" href="{{URL::asset('front-html/alertifyjs/themes/css/bootstrap.css') }}" />
</head>