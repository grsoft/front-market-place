<?php $p=$product;?>
{{view('front/headmeta')}}
<body class="index-opt-1 catalog-product-view catalog-view_op1 page-order">
{{view('front/header')->with('catemenu',$catemenu)}}

		<!-- MAIN -->
  <main class="site-main">
    <div class="columns container-fluid"> 
      <!-- Block  Breadcrumb-->
      
      <ol class="breadcrumb no-hide">
        <li><a href="#">Home </a></li>
        <li><a href="#">Search </a></li>
        <li class="active">{{$_GET['title']}} </li> 
      </ol>
      <!-- Block  Breadcrumb-->
      
      <div class="row"> 
        
        <!-- Main Content -->
        <div class="col-md-12   col-main"> 
          
          <!-- List Products -->
          <div class="products  products-grid">
            <ol class="product-items row" id="productitems">
              
            </ol>
            <!-- list product --> 
          </div>
          <!-- List Products --> 
          
          <!-- Toolbar -->
          <div class=" toolbar-products toolbar-bottom">
            
            <ul class="pagination" id="pagination">
              
            </ul> 
          </div>
          <!-- Toolbar --> 
          
        </div>
        <!-- Main Content --> 
        
        
      </div>
    </div>
  </main>
  <!-- end MAIN --> 
  
  <!-- jQuery --> 
<script type="text/javascript" src="{{ URL::asset('front-html/js/jquery.min.js') }}"></script> 
  <script type="text/javascript" src="{{url::asset('jquery.twbsPagination.min.js')}}"></script>
  <script type="text/javascript">

    var result=[];
    result=<?php echo json_encode($p) ?>;
    var $pagination = $('#pagination'),
    totalRecords = 0,
    records = [],
    displayRecords = [],
    recPerPage = 18,
    page = 1,
    totalPages = 0;
    records = result;
    totalRecords = records.length;
    totalPages = Math.ceil(totalRecords / recPerPage);
    if(records.length==0)
    { 
       $('#productitems').html('<li class="col-sm-12 center " ><h3>Search not found</h3></li>');
    }
    apply_pagination();
    function generate_product() { 
          var product;var disc="";
          $('#productitems').html('');
          
          for (var i = 0; i < displayRecords.length; i++) {
                if(displayRecords[i].discount>0){
                disc=`<span class="product-item-label label-price">${displayRecords[i].discount}% <span>off</span></span>
                      `;
                }
                var product=`<li class="col-sm-3 product-item " >
                <div class="product-item-opt-1">
                  <div class="product-item-info">
                    <div class="product-item-photo"> <a href="{{URL('/shop/detail/')}}/${displayRecords[i].id}" class="product-item-img"><img src="{{URL::asset('product_image')}}/${displayRecords[i].image}" alt="product name"></a>
                      <div class="product-item-actions"> <a href="" class="btn btn-wishlist"><span>wishlist</span></a> <a href="" class="btn btn-compare"><span>compare</span></a> <a href="" class="btn btn-quickview"><span>quickview</span></a> </div>
                      <button class="btn btn-cart" data-pid="${displayRecords[i].id}" data-qty="1" type="button"><span>Add to Cart</span></button>
                       ${disc} 
                      </div>
                    <div class="product-item-detail"> <strong class="product-item-name"><a href="{{URL('/shop/detail/')}}/${displayRecords[i].id}">${displayRecords[i].title}</a></strong>
                      <div class="clearfix">
                        <div class="product-item-price"> <span class="price">${displayRecords[i].sell_price}</span> <span class="old-price">${displayRecords[i].mrp}</span> </div>
                        <div class="product-reviews-summary">
                          <div class="rating-summary">
                            <div class="rating-result" title="80%"> <span style="width:80%"> <span><span>80</span>% of <span>100</span></span> </span> </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </li>`;
                $('#productitems').append(product);
          }
    }
    function apply_pagination() {
      $pagination.twbsPagination({
            totalPages: totalPages,
            visiblePages: 5,
            onPageClick: function (event, page) {
                  displayRecordsIndex = Math.max(page - 1, 0) * recPerPage;
                  endRec = (displayRecordsIndex) + recPerPage;
                 
                  displayRecords = records.slice(displayRecordsIndex, endRec);
                  generate_product();
            }
      });
    }
</script>
{{view('front/footer')}}