<!-- categori -->
          <div class="block-nav-categori">
          <div class="block-title"> <span>Categories</span> </div>
          <div class="block-content">
            <div class="clearfix"><span data-action="close-cat" class="close-cate"><span>Categories</span></span></div>
            <ul class="ui-categori">
              <?php $i=0; ?>
              @foreach($catemenu[0] as $k => $value)
              @if(count($catemenu[1][$value->title])>0)
              
              
              <li class="parent"> <a href="{{url('/shop/category')}}/{{$value->title}}/{{$value->id}}"> <span class="icon"><img src="{{URL::asset('front-html')}}/images/icon/index1/nav-cat1.png" alt="nav-cat"></span>{{$value->title}} </a> <span class="toggle-submenu"></span>
               
                <div class="submenu" style="background-image: url(images/media/index1/bgmenu.jpg);">
                  
                  <ul class="categori-list clearfix">
                    <?php $oldcat=""; ?>
                    @foreach($catemenu[1][$value->title] as $svalue)
                    <?php $i++; ?>
                    <?php if($oldcat<>$svalue->title1){ $oldcat=$svalue->title1; ?>
                    <li class="col-sm-3"> <strong class="title"><a href="#">{{$svalue->title1}}</a></strong>
                      <ul>
                        @foreach($catemenu[1][$value->title] as $sivalue)
                        @if($sivalue->title1 === $svalue->title1)
                        <li><a href="{{url('/shop/category')}}/{{$value->title}}/{{$sivalue->title2}}/{{$sivalue->id}}">{{$sivalue->title2}} </a></li>
                        @endif
                        @endforeach

                      </ul>
                    </li>
                  <?php } ?>
                    @endforeach
                    
                  </ul>
                  
                </div>
                
              </li>
              @else
              <li> <a href="#"> <span class="icon"><img src="{{URL::asset('front-html')}}/images/icon/index1/nav-cat4.png" alt="nav-cat"></span> {{$value->title}} </a> </li>
              @endif
              @endforeach
              <!-- <li class="parent"> <a href="#"> <span class="icon"><img src="{{URL::asset('front-html')}}/images/icon/index1/nav-cat2.png" alt="nav-cat"></span> Sports & Outdoors </a> <span class="toggle-submenu"></span>
                <div class="submenu">
                  <div class="categori-img"> <a href="#"><img src="{{URL::asset('front-html')}}/images/media/index1/categori-img1.jpg" alt="categori-img"></a> </div>
                  <ul class="categori-list">
                    <li class="col-sm-3"> <strong class="title"><a href="#">Smartphone</a></strong>
                      <ul>
                        <li><a href="#">Skirts </a></li>
                        <li><a href="#">Jackets</a></li>
                        <li><a href="#">Jumpusuits</a></li>
                        <li><a href="#">Scarvest</a></li>
                        <li><a href="#">T-Shirts</a></li>
                      </ul>
                    </li>
                    <li class="col-sm-3"> <strong class="title"><a href="#">TElevision</a></strong>
                      <ul>
                        <li><a href="#">Skirts </a></li>
                        <li><a href="#">Jackets</a></li>
                        <li><a href="#">Jumpusuits</a></li>
                        <li><a href="#">Scarvest</a></li>
                        <li><a href="#">T-Shirts</a></li>
                      </ul>
                    </li>
                    <li class="col-sm-3"> <strong class="title"><a href="#">Camera</a></strong>
                      <ul>
                        <li><a href="#">Skirts </a></li>
                        <li><a href="#">Jackets</a></li>
                        <li><a href="#">Jumpusuits</a></li>
                        <li><a href="#">Scarvest</a></li>
                        <li><a href="#">T-Shirts</a></li>
                      </ul>
                    </li>
                    <li class="col-sm-3"> <strong class="title"><a href="#">washing machine</a></strong>
                      <ul>
                        <li><a href="#">Skirts </a></li>
                        <li><a href="#">Jackets</a></li>
                        <li><a href="#">Jumpusuits</a></li>
                        <li><a href="#">Scarvest</a></li>
                        <li><a href="#">T-Shirts</a></li>
                      </ul>
                    </li>
                  </ul>
                </div>
              </li>
              <li class="parent"> <a href="#"> <span class="icon"><img src="{{URL::asset('front-html')}}/images/icon/index1/nav-cat3.png" alt="nav-cat"></span> Smartphone & Tablets </a> <span class="toggle-submenu"></span>
                <div class="submenu"> <strong class="subtitle"><span>special products</span></strong>
                  <div class="owl-carousel" 
                                                data-nav="true" 
                                                data-dots="false" 
                                                data-margin="30" 
                                                data-autoplayTimeout="300" 
                                                data-autoplay="true" 
                                                data-loop="true"
                                                data-responsive='{
                                                "0":{"items":1},
                                                "380":{"items":1},
                                                "480":{"items":1},
                                                "600":{"items":1},
                                                "992":{"items":4}
                                                }'>
                    <div class="product-item product-item-opt-1">
                      <div class="product-item-info">
                        <div class="product-item-photo"> <a class="product-item-img" href="detail.html"><img alt="product name" src="{{URL::asset('front-html')}}/images/media/index1/product-menu1.jpg"></a> </div>
                        <div class="product-item-detail home-pro"> <strong class="product-item-name"><a href="#">Asus Ispiron 20</a></strong>
                          <div class="product-item-price"> <span class="price">$45.00</span> </div>
                        </div>
                      </div>
                    </div>
                    <div class="product-item product-item-opt-1">
                      <div class="product-item-info">
                        <div class="product-item-photo"> <a class="product-item-img" href="detail.html"><img alt="product name" src="{{URL::asset('front-html')}}/images/media/index1/product-menu2.jpg"></a> </div>
                        <div class="product-item-detail home-pro"> <strong class="product-item-name"><a href="#">Electronics Ispiron 20 </a></strong>
                          <div class="product-item-price"> <span class="price">$45.00</span> </div>
                        </div>
                      </div>
                    </div>
                    <div class="product-item product-item-opt-1">
                      <div class="product-item-info">
                        <div class="product-item-photo"> <a class="product-item-img" href="detail.html"><img alt="product name" src="{{URL::asset('front-html')}}/images/media/index1/product-menu3.jpg"></a> </div>
                        <div class="product-item-detail home-pro"> <strong class="product-item-name"><a href="#">Samsung Ispiron 20 </a></strong>
                          <div class="product-item-price"> <span class="price">$45.00</span> </div>
                        </div>
                      </div>
                    </div>
                    <div class="product-item product-item-opt-1">
                      <div class="product-item-info">
                        <div class="product-item-photo"> <a class="product-item-img" href="detail.html"><img alt="product name" src="{{URL::asset('front-html')}}/images/media/index1/product-menu4.jpg"></a> </div>
                        <div class="product-item-detail home-pro"> <strong class="product-item-name"><a href="#">Electronics Ispiron 20 </a></strong>
                          <div class="product-item-price"> <span class="price">$45.00</span> </div>
                        </div>
                      </div>
                    </div>
                    <div class="product-item product-item-opt-1">
                      <div class="product-item-info">
                        <div class="product-item-photo"> <a class="product-item-img" href="detail.html"><img alt="product name" src="{{URL::asset('front-html')}}/images/media/index1/product-menu4.jpg"></a> </div>
                        <div class="product-item-detail home-pro"> <strong class="product-item-name"><a href="#">Samsung Ispiron 20 </a></strong>
                          <div class="product-item-price"> <span class="price">$45.00</span> </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </li> -->
              
            </ul>
            <div class="view-all-categori"> <a  class="open-cate btn-view-all">All Categories</a> </div>
          </div>
        </div>
          <!-- categori --> 