{{view('front/headmeta')}}
<body class="index-opt-1 catalog-product-view catalog-view_op1 page-order">
{{view('front/header')->with('catemenu',$catemenu)}}

		<!-- MAIN -->
  <main class="site-main">
    <div class="columns container-fluid"> 
      <!-- Block  Breadcrumb-->
      
      <ol class="breadcrumb no-hide">
        <li><a href="#">Home </a></li>
        <li class="active">Seller</li>
      </ol>
      <!-- Block  Breadcrumb-->
      
      <?php /*?><h2 class="page-heading"> <span class="page-heading-title2">Authentication</span> </h2><?php */?>
      <div class="page-content">
        <div class="container-fluid">
          @if (session('success'))
              <div class="alert alert-success">
                  {{ session('success') }}
              </div>
          @endif
          @if (session('error'))
              <div class="alert alert-success">
                  {{ session('error') }}
              </div>
          @endif
          <div class="row">
            <div class="col-sm-6">
              <div class="box-authentication">
                <h3>Create an account</h3>
                <p>Please enter your email address to create an account.</p>
                <form action="{{URL('/seller/registration')}}" autocomplete="off" method="post">
                  @csrf
                <ul>
              <div class="row">
              <div class="col-sm-6">
                <label for="first_name" class="required">First Name</label>
                <input class="input form-control" name="first_name" id="first_name" type="text" required>
              </div>
              <div class="col-sm-6">
                <label for="last_name" class="required">Last Name</label>
                <input name="last_name" class="input form-control" id="last_name" type="text" required>
              </div>
            
              <div class="col-sm-6">
                <label for="company_name">Shop Name</label>
                <input name="shop_name" class="input form-control" id="shop_name" type="text" required>
              </div>
              <div class="col-sm-6">
                <label for="email_address" autofill="false" >Email Address</label>
                <input class="input form-control" name="email_address" id="email_address" type="email" required>
              </div>
            
              <div class="col-xs-12">
                <label for="address" class="required">Address</label>
                <input class="input form-control" name="address" id="address" type="text" required>
              </div>
            
              <div class="col-sm-6">
                <label for="city" class="required">City</label>
                <input class="input form-control" name="city" id="city" type="text" required>
              </div>
              <div class="col-sm-6">
                <label class="required">State/Province</label>
                <input type="text" class="input form-control" name="state" required>
                  
              </div>
            
              <div class="col-sm-6">
                <label for="postal_code" class="required">Zip/Postal Code</label>
                <input class="input form-control" name="postal_code" id="postal_code" type="text" required>
              </div>
              <div class="col-sm-6">
                <label class="required">Country</label>
                <input type="text" class=" form-control" name="country" required>
                  
              </div>
            
              <div class="col-sm-6">
                <label for="telephone" class="required">Mobile Number</label>
                <input class="input form-control" name="mobile_number" id="mobile_number" type="text" required>
              </div>
              
            
              <div class="col-sm-6">
                <label for="password" class="required">Password</label>
                <input class="input form-control" name="password" id="password" type="password" required>
              </div>
              
              </div>
            <li>
              <button type="submit" class="button">Continue</button>
            </li>
          </ul>
            </form>    
                
              </div>
            </div>
            <div class="col-sm-6">
              <div class="box-authentication">
                <h3>Already registered?</h3>
                <form action="{{url('/seller/login')}}" method="post">
                  @csrf
                <label for="emmail_login">Email address</label>
                <input type="text" name="email_address" class="form-control" id="emmail_login">
                <label for="password_login">Password</label>
                <input type="password" name="password" class="form-control" id="password_login">
                <p class="forgot-pass"><a href="#">Forgot your password?</a></p>
                <button class="button"><i class="fa fa-lock"></i> Sign in</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </main>
  <!-- end MAIN --> 
  {{view('front/footer')}}