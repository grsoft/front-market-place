{{view('front/headmeta')}}
<body class="index-opt-1 catalog-product-view catalog-view_op1">
{{view('front/header')->with('catemenu',$catemenu)}}

		<!-- MAIN -->
  <main class="site-main">
    <div class="columns container-fluid"> 
      <!-- Block  Breadcrumb-->
      
      <ol class="breadcrumb no-hide">
        <li><a href="#">Home </a></li>
        <?php 
        $click=$hometree[count($hometree)-1];
        for($i=0;$i<count($hometree);$i++){ ?>
        <li><a href="#">{{$hometree[$i]}} </a></li>
        <?php } ?>
        <?php $revdata=DB::table('rating')->join('customer','rating.customer_id','customer.id')->where('rating.product_id',$data[0]->id)->get();
        ?>
      </ol>
      <!-- Block  Breadcrumb-->
      
      <div class="row"> 
        
        <!-- Main Content -->
        <div class="col-md-12  col-main">
          <div class="row">
            <div class="col-sm-6 col-md-5 col-lg-5">
              <div class="product-media media-horizontal">
                <div class="image_preview_container images-large"> <img id="img_zoom" data-zoom-image="{{URL::asset('product_image')}}/{{$data[0]->image_zoom}}" src="{{URL::asset('product_image')}}/{{$data[0]->image_zoom}}" alt="">
                  <button class="btn-zoom open_qv"><span>zoom</span></button>
                </div>
                <div class="product_preview images-small">
                  <div class="owl-carousel thumbnails_carousel" id="thumbnails"  data-nav="true" data-dots="false" data-margin="10" data-responsive='{"0":{"items":3},"480":{"items":4},"600":{"items":5},"768":{"items":3}}'> 
                    @foreach($thumb as $value)
                    <a href="#" data-image="{{URL::asset('thumb_image')}}/{{$value->image_zoom}}" data-zoom-image="{{URL::asset('thumb_image')}}/{{$value->image_zoom}}"> <img src="{{URL::asset('thumb_image')}}/{{$value->image}}" data-large-image="{{URL::asset('thumb_image')}}/{{$value->image_zoom}}" alt=""> </a> 
                    @endforeach
                  </div>
                  <!--/ .owl-carousel--> 
                  
                </div>
                <!--/ .product_preview--> 
                
              </div>
              <!-- image product --> 
            </div>
            <div class="col-sm-6 col-md-7 col-lg-7">
              <div class="product-info-main">
                <h1 class="page-title"> {{$data[0]->title}} </h1>
                <div class="product-reviews-summary">
                  <div class="rating-summary">
                    <div class="rating-result" title="70%"> <span style="width:70%"> <span><span>70</span>% of <span>100</span></span> </span> </div>
                  </div>
                  <div class="reviews-actions"> <a href="#" class="action view">Based  on <?php echo $revdata->count(); ?> ratings</a> 
                  @if(Session::has('cid')) <a href="#" data-toggle="modal" data-target="#rating" class="action add"><img alt="img" src="{{URL::asset('front-html')}}/images/icon/write.png">&#160;&#160;write a review</a>@else <a href="{{URL('/shop/shop-login')}}"  class="action add"><img alt="img" src="{{URL::asset('front-html')}}/images/icon/write.png">&#160;&#160;write a review</a> @endif </div>
                </div>
                <div class="product-info-price">
                  <div class="price-box"> <span class="price">{{$data[0]->sell_price}} </span> <span class="old-price">{{$data[0]->mrp}}</span> <span class="label-sale">-{{$data[0]->discount}}%</span> </div>
                </div>
                <div class="product-code"> Item Code: {{$data[0]->model}} </div>
                <div class="product-info-stock">
                  <div class="stock available"> <span class="label">Availability: </span>@if($data[0]->availability==1) <b>In Stock</b> @else <b>Out Of Stock</b> @endif</div>
                </div>
                <!-- <div class="product-condition"> Condition: New </div> -->
                <div class="product-overview">
                  <div class="overview-content"> <?php echo $data[0]->short_info; ?> </div>
                </div>
                @if(!empty($data[0]->json_data))
                <?php $json=json_decode($data[0]->json_data); ?>
                <div class="product-overview">
                  <ul>
                  @foreach($json as $key => $value)
                    <li>{{ ucfirst(str_replace('_',' ',$key)) }} : {{$value}}</li>
                    
                  @endforeach
                  </ul>
                </div>
                @endif
                <div class="product-add-form">
                
                  <p>Available Options:</p>
                  <form>
                    <div class="product-options-wrapper">
                     <div class="row">
                     <div class="col-md-3">
                      <div class="form-qty">
                        <label class="label">Qty: </label>
                        <div class="control">
                          <input type="text" class="form-control input-qty" value='1' id="qty1" name="qty1"  maxlength="12"  minlength="1">
                          <button class="btn-number  qtyminus" data-type="minus" data-field="qty1"><span>-</span></button>
                          <button class="btn-number  qtyplus" data-type="plus" data-field="qty1"><span>+</span></button>
                        </div>
                      </div>
                      </div>
                      <div class="col-md-3">
                      <div class="form-configurable">
                        <label for="forSize" class="label">Size: </label>
                        <div class="control">
                          <select  id="forSize" class="form-control attribute-select">
                            <option value="1">XXXL</option>
                            <option value="4">X</option>
                            <option value="5">L</option>
                          </select>
                        </div>
                        <a href="#" class="size-chart">Size chart</a> </div>
                      </div>
                      <div class="col-md-12">
                     
                      <div class="">
            <!-- <label class="btn btn-info">
                <input type="radio" class="form-switch radio_1set" name="colorCheckbox" value="Latifa Towers Branch" gl1="1" data-id="a">Buy 1 At $35.98/pc</label>
            <label class="btn btn-info">

                <input type="radio" class="form-switch radio_1set" name="colorCheckbox" value="Al Barsha Branch" gl1="2" data-id="b"> Buy 2 At $32.99/pc (Top Selling)</label>
            <label class="btn btn-info">
                <input type="radio" class="form-switch radio_1set" name="colorCheckbox" value="Vision Tower Branch" gl1="3" data-id="c"> Buy 3 At $29.99/pc (Best Deal)</label> -->
        </div>
                     </div>
                     
                     
                     
                     </div>
                    </div>
                    <div class="product-options-bottom clearfix">
                      <div class="actions">
                        <button type="submit" title="Add to Cart" class="action btn-cart" data-pid="{{$data[0]->id}}" data-qty="getinput"> <span>Add to Cart</span> </button>
                         <button type="submit" title="Add to Cart" class="action btn-cart1"> <span>Contact Supplier </span> </button>
                          <button type="submit" title="Add to Cart" class="action btn-cart"> <span>Buy Now</span> </button>
                        <div class="product-addto-links"> <a href="wishList.html" class="action btn-wishlist" title="Wish List"> <span>Wishlist</span> </a> <a href="compare.html" class="action btn-compare" title="Compare"> <span>Compare</span> </a> </div>
                      </div>
                    </div>
                  </form>
                </div>
                <div class="product-addto-links-second"> <a href="#" class="action action-print">Print</a> <a href="#" class="action action-friend">Send to a friend</a> </div>
                <div class="share"> <img src="{{URL::asset('front-html')}}/images/media/index1/share.png" alt="share"> </div>
              </div>
              <!-- detail- product --> 
              
            </div>
            <!-- Main detail --> 
            
          </div>
          
          <!-- product tab info -->
          
          <div class="product-info-detailed "> 
            
            <!-- Nav tabs -->
            <ul class="nav nav-pills" role="tablist">
              <li role="presentation" class="active"><a href="#description"  role="tab" data-toggle="tab">Product Details </a></li>
              <li role="presentation"><a href="#tags"  role="tab" data-toggle="tab">information </a></li>
              <li role="presentation"><a href="#reviews"  role="tab" data-toggle="tab">reviews</a></li>
              <!-- <li role="presentation"><a href="#additional"  role="tab" data-toggle="tab">Extra Tabs</a></li>
              <li role="presentation"><a href="#tab-cust"  role="tab" data-toggle="tab">Guarantees</a></li> -->
            </ul>
            
            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="description">
                <div class="block-title">Product Details</div>
                <div class="block-content" style="line-spacing:1;">
                  <?php echo $data[0]->about; ?>
                </div>
              </div>
              <div role="tabpanel" class="tab-pane" id="tags">
                <div class="block-title">information</div>
                <div class="block-content">
                <?php echo $data[0]->short_info; ?>
                </div>
              </div>
              <div role="tabpanel" class="tab-pane" id="reviews">
                <div class="block-title">reviews</div>
                <div class="block-content">
                  <?php
                  
                  for($j=0;$j<$revdata->count();$j++){
                  ?>
                  <h3>{{$revdata[$j]->first_name}} {{$revdata[0]->last_name}}</h3>
                  <h5>{{$revdata[$j]->comment}} </h5>
                  <h4><?php for($i=1;$i<=$revdata[$j]->rating;$i++){
                    ?><span class="rating-color"><i class = "fa fa-star" aria-hidden = "true" ></i></span><?php 
                  } ?>
                  <?php for($i=1;$i<=(5-$revdata[$j]->rating);$i++){
                    ?><span class="rating-no-color"><i class = "fa fa-star" aria-hidden = "true" ></i></span><?php 
                  } ?> </h4>
                  <?php } ?>
                </div>
              </div>
              <!-- <div role="tabpanel" class="tab-pane" id="additional">
                <div class="block-title">Extra Tabs</div>
                <div class="block-content">
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>
                </div>
              </div>
              <div role="tabpanel" class="tab-pane" id="tab-cust">
                <div class="block-title">Guarantees</div>
                <div class="block-content">
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also Aldus PageMaker including versions of Lorem Ipsum</p>
                </div> -->
              </div>
            </div>
          </div>
          <!-- product tab info --> 
          
          <!-- block-related product -->
          <div class="block-related ">
            <div class="block-title"> <strong class="title">RELATED products</strong> </div>
            <div class="block-content ">
              <ol class="product-items owl-carousel " data-nav="true" data-dots="false" data-margin="30" data-responsive='{"0":{"items":1},"480":{"items":2},"600":{"items":3},"992":{"items":3},"1200":{"items":4}}'>
                @foreach($related as $key=> $prod)
              <li class="product-item product-item-opt-2 " id="li{{$key}}">
                <div class="product-item-opt-1">
                  <div class="product-item-info">
                    <div class="product-item-photo"> <a href="{{URL('/shop/detail/')}}/{{$prod->id}}" class="product-item-img"><img src="{{URL::asset('product_image')}}/{{$prod->image_zoom}}" alt="product name"></a>
                      <div class="product-item-actions"> <a href="" class="btn btn-wishlist"><span>wishlist</span></a> <a href="" class="btn btn-compare"><span>compare</span></a> <a href="" class="btn btn-quickview"><span>quickview</span></a> </div>
                      <button class="btn btn-cart" data-pid="{{$prod->id}}" data-qty="1" type="button"><span>Add to Cart</span></button>
                      @if($prod->discount>0)
                      <span class="product-item-label label-price">{{$prod->discount}}% <span>off</span></span>
                      @endif
                      </div>
                    <div class="product-item-detail"> <strong class="product-item-name"><a href="{{URL('/shop/detail/')}}/{{$prod->id}}">{{$prod->title}}</a></strong>
                      <div class="clearfix">
                        <div class="product-item-price"> <span class="price">${{$prod->sell_price}}</span> <span class="old-price">${{$prod->mrp}}</span> </div>
                        <div class="product-reviews-summary">
                          <div class="rating-summary">
                            <div class="rating-result" title="80%"> <span style="width:80%"> <span><span>80</span>% of <span>100</span></span> </span> </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </li>
              @endforeach
                
                
              </ol>
            </div>
          </div>
          <!-- block-related product --> 
          
          <!-- block-Upsell Products -->
          <div class="block-upsell ">
            <div class="block-title"> <strong class="title">You might also like</strong> </div>
            <div class="block-content ">
              <ol class="product-items owl-carousel " data-nav="true" data-dots="false" data-margin="30" data-responsive='{"0":{"items":1},"480":{"items":2},"600":{"items":3},"992":{"items":3},"1200":{"items":4}}'>
                <li class="product-item product-item-opt-2">
                  <div class="product-item-info">
                    <div class="product-item-photo"> <a href="#" class="product-item-img"><img src="{{URL::asset('front-html')}}/images/media/detail/Upsell2-1.jpg" alt="product name"></a>
                      <div class="product-item-actions"> <a href="#" class="btn btn-wishlist"><span>wishlist</span></a> <a href="#" class="btn btn-compare"><span>compare</span></a> <a href="#" class="btn btn-quickview"><span>quickview</span></a> </div>
                      <button class="btn btn-cart" type="button"><span>Add to Cart</span></button>
                    </div>
                    <div class="product-item-detail"> <strong class="product-item-name"><a href="#">Leather Swiss Watch</a></strong>
                      <div class="clearfix">
                        <div class="product-item-price"> <span class="price">$45.00</span> <span class="old-price">$52.00</span> </div>
                        <div class="product-reviews-summary">
                          <div class="rating-summary">
                            <div class="rating-result" title="70%"> <span style="width:70%"> <span><span>70</span>% of <span>100</span></span> </span> </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </li>
                <li class="product-item product-item-opt-2">
                  <div class="product-item-info">
                    <div class="product-item-photo"> <a href="#" class="product-item-img"><img src="{{URL::asset('front-html')}}/images/media/detail/Upsell2-2.jpg" alt="product name"></a>
                      <div class="product-item-actions"> <a href="#" class="btn btn-wishlist"><span>wishlist</span></a> <a href="#" class="btn btn-compare"><span>compare</span></a> <a href="#" class="btn btn-quickview"><span>quickview</span></a> </div>
                      <button class="btn btn-cart" type="button"><span>Add to Cart</span></button>
                    </div>
                    <div class="product-item-detail"> <strong class="product-item-name"><a href="#">Sport T-Shirt For Men</a></strong>
                      <div class="clearfix">
                        <div class="product-item-price"> <span class="price">$45.00</span> <span class="old-price">$52.00</span> </div>
                        <div class="product-reviews-summary">
                          <div class="rating-summary">
                            <div class="rating-result" title="70%"> <span style="width:70%"> <span><span>70</span>% of <span>100</span></span> </span> </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </li>
                <li class="product-item product-item-opt-2">
                  <div class="product-item-info">
                    <div class="product-item-photo"> <a href="#" class="product-item-img"><img src="{{URL::asset('front-html')}}/images/media/detail/Upsell2-3.jpg" alt="product name"></a>
                      <div class="product-item-actions"> <a href="#" class="btn btn-wishlist"><span>wishlist</span></a> <a href="#" class="btn btn-compare"><span>compare</span></a> <a href="#" class="btn btn-quickview"><span>quickview</span></a> </div>
                      <button class="btn btn-cart" type="button"><span>Add to Cart</span></button>
                    </div>
                    <div class="product-item-detail"> <strong class="product-item-name"><a href="#">Fashion Leather Handbag</a></strong>
                      <div class="clearfix">
                        <div class="product-item-price"> <span class="price">$45.00</span> <span class="old-price">$52.00</span> </div>
                        <div class="product-reviews-summary">
                          <div class="rating-summary">
                            <div class="rating-result" title="70%"> <span style="width:70%"> <span><span>70</span>% of <span>100</span></span> </span> </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </li>
                <li class="product-item product-item-opt-2">
                  <div class="product-item-info">
                    <div class="product-item-photo"> <a href="#" class="product-item-img"><img src="{{URL::asset('front-html')}}/images/media/detail/Upsell2-3.jpg" alt="product name"></a>
                      <div class="product-item-actions"> <a href="#" class="btn btn-wishlist"><span>wishlist</span></a> <a href="#" class="btn btn-compare"><span>compare</span></a> <a href="#" class="btn btn-quickview"><span>quickview</span></a> </div>
                      <button class="btn btn-cart" type="button"><span>Add to Cart</span></button>
                    </div>
                    <div class="product-item-detail"> <strong class="product-item-name"><a href="#">Fashion Leather Handbag</a></strong>
                      <div class="clearfix">
                        <div class="product-item-price"> <span class="price">$45.00</span> <span class="old-price">$52.00</span> </div>
                        <div class="product-reviews-summary">
                          <div class="rating-summary">
                            <div class="rating-result" title="70%"> <span style="width:70%"> <span><span>70</span>% of <span>100</span></span> </span> </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </li>
              </ol>
            </div>
          </div>
          <!-- block-Upsell Products --> 
          
        </div>
        <!-- Main Content --> 
        
      </div>
    </div>
  </main>
  <!-- end MAIN --> 
  <div id="rating" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">write a review</h4>
      </div>
      <div class="modal-body">

      <div class="product-rating">
        <input type="text" placeholder="Write Comment" class="form-control" id="comment">
      <i class = "fa fa-star" aria-hidden = "true" id = "st1"></i>  
       <i class = "fa fa-star" aria-hidden = "true" id = "st2"></i>  
       <i class = "fa fa-star" aria-hidden = "true" id = "st3"></i>  
       <i class = "fa fa-star" aria-hidden = "true" id = "st4"></i>  
       <i class = "fa fa-star" aria-hidden = "true" id = "st5"></i> 
      </div> 
      <p id="rating-msg" class="text-center"> 0 Star</p>
      <input type="hidden" id="prod_id" value="{{$prod->id}}">
      <p class="submit-event text-center"><button id="rating_submit" type="button" class="btn btn-danger">Submit</button></p>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<style>
  .product-rating{
    text-align:center;
  }
  .product-rating .fa-star {
     
        font-size : 50px;  
        align-content: center;  
        color:#b3afaf;
    
}
.rating-color{
  font-size : 20px;  
  color:#e48505;
}
.rating-no-color{
  font-size : 20px;  
  color:#b3afaf;
}
</style>

  {{view('front/footer')}}
  <script>  
         
         $("#st1").click(function() {  
             $(".fa-star").css("color", "#b3afaf");  
             $("#st1").css("color", "#e48505");  
            $("#rating-msg").text("1 Star");
            $("#rating_submit").val(1);
         });  
         $("#st2").click(function() {  
             $(".fa-star").css("color", "#b3afaf");  
             $("#st1, #st2").css("color", "#e48505");  
             $("#rating-msg").text("2 Star");
             $("#rating_submit").val(2);
         });  
         $("#st3").click(function() {  
             $(".fa-star").css("color", "#b3afaf")  
             $("#st1, #st2, #st3").css("color", "#e48505");  
             $("#rating-msg").text("3 Star");
             $("#rating_submit").val(3);
         });  
         $("#st4").click(function() {  
             $(".fa-star").css("color", "#b3afaf");  
             $("#st1, #st2, #st3, #st4").css("color", "#e48505");  
             $("#rating-msg").text("4 Star");
             $("#rating_submit").val(4);
         });  
         $("#st5").click(function() {  
             $(".fa-star").css("color", "#b3afaf");  
             $("#st1, #st2, #st3, #st4, #st5").css("color", "#e48505");  
             $("#rating-msg").text("5 Star");
             $("#rating_submit").val(5);
         });  
         $('#rating_submit').click(function (e) {
              e.preventDefault();
              if($('#rating_submit').val()=="")
              {
                
                $(".product-rating").fadeOut('slow');
                $(".product-rating").fadeIn('slow');
              }
              else
              {
                var data = {
                    '_token': $('input[name=_token]').val(),
                    'rating':$('#rating_submit').val(),
                    'prod_id':$('#prod_id').val(),
                    'comment':$('#comment').val()
                };
                $.ajax({
                    url: "{{URL('/rating-add')}}",
                    type: 'POST',
                    data: data,
                    success: function (response) {
                      $('#rating_submit').hide();
                      $('.submit-event').text('Your review submited.').css({'color':'green','font-size':'22px'});
                      setTimeout(function () {
                        $("#rating").modal('hide');
                    }, 5000);
                    }
                });
              }
          });
   </script>  
  