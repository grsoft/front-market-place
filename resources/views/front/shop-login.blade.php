{{view('front/headmeta')->with('page',$page)}}
<body class="index-opt-1 catalog-product-view catalog-view_op1 page-order">
{{view('front/header')->with('catemenu',$catemenu)}}

		<!-- MAIN -->
  <main class="site-main">
    <div class="columns container-fluid"> 
      <!-- Block  Breadcrumb-->
      
      <ol class="breadcrumb no-hide">
        <li><a href="#">Home </a></li>
        <li class="active"> Checkout</li>
      </ol>
      <!-- Block  Breadcrumb-->
      
      <div class="page-content checkout-page">
        <h3 class="checkout-sep">Login & Register</h3>
        <div class="box-border">
          <div class="row">
            <div class="col-sm-6">
              <h4>Checkout as a Register</h4>
              <p>Register with us for future convenience:</p>
              <ul>
                
                <li>
                  <label>
                    <input name="radio1" checked="true" type="radio">
                    Register</label>
                </li>
              </ul>
              <br>
              <h4>Register and save time!</h4>
              <p>Register with us for future convenience:</p>
              <p><i class="fa fa-check-circle text-primary"></i> Fast and easy check out</p>
              <p><i class="fa fa-check-circle text-primary"></i> Easy access to your order history and status</p>
              <a href="{{URL('/shop/shop-register')}}" class="btn btn-success">Continue</a>
            </div>
            <div class="col-sm-6">
              <h4>Login</h4>
              <p>Already registered? Please log in below:</p>
              <form method="post" action="{{URL('shop/shop-customer-login')}}">
                @csrf
                <label>Email address</label>
                <input class="form-control input" name="email_address" type="email">
                <label>Password</label>
                <input class="form-control input" name="password" type="password">
                <p><a href="#">Forgot your password?</a></p>
                <button type="submit" class="button">Login</button>
              </form>
            </div>
          </div>
        </div>
        
      </div>
    </div>
  </main>
  <!-- end MAIN --> 
  {{view('front/footer')}}