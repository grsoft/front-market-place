{{view('front/headmeta')}}
<body class="index-opt-1 catalog-product-view catalog-view_op1 page-order">
{{view('front/header')->with('catemenu',$catemenu)}}

		<!-- MAIN -->
  <main class="site-main">

            <div class="columns container-fluid">
                <!-- Block  Breadcrumb-->
                        
                <ol class="breadcrumb no-hide">
                    <li><a href="#">Home    </a></li>
                    <li class="active">Your Orders</li>
                </ol><!-- Block  Breadcrumb-->

                <h2 class="page-heading">
                    <span class="page-heading-title2">Order</span>
                </h2>
                @if (session('success'))
                      <div class="alert alert-success">
                          {{ session('success') }}
                      </div>
                @endif
                <div class="page-content page-order">
                    
                    <?php $total=0; ?>
                    <div class="order-detail-content">
                        <div class="table-responsive">
                            @foreach ($cdata as $data)
                            <div class="row ">
                                <div class="col-md-4 " style="background: red; color: white;">
                                    <div style="padding: 5px;" >
                                    Order Number/Date Time 
                                    {{$data->order_unique_no}} / {{$data->created_at}}
                                    </div>
                                </div>
                                <div class="col-md-4 " style="background: skyblue; color: white;">
                                    <div style="padding: 5px;" >
                                    Order Status : {{$data->order_status}}
                                    </div>
                                </div>
                                <div class="col-md-4 " style="background: skyblue; color: white;">
                                    <div style="padding: 5px;" >
                                    Order Remarks : {{$data->order_status_resion}}
                                    </div>
                                </div>
                                
                            </div>
                            <table class="table table-bordered  cart_summary">
                                <thead>
                                    <tr>
                                        <th class="cart-description">Image</th>
                                        <th class="cart-product-name">Product Name</th>
                                        <th class="cart-price">Price</th>
                                        <th class="cart-qty">Quantity</th>
                                        <th class="cart-total">Grandtotal</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                    
                                    <tr class="cartpage">
                                        <td class="cart-image">
                                            <a class="entry-thumbnail" href="javascript:void(0)">
                                                <img src="{{ asset('product_image/'.$data->image) }}" width="70px" alt="">
                                            </a>
                                        </td>
                                        <td class="cart-product-name-info">
                                            <h4 class='cart-product-description'>
                                                <a href="javascript:void(0)">{{ $data->title }}</a>
                                            </h4>
                                        </td>
                                        <td class="cart-product-sub-total">
                                            <span class="cart-sub-total-price">{{ number_format($data->amount, 2) }}</span>
                                        </td>
                                        <td class="cart-product-quantity">
                                            <input type="hidden" class="product_id" value="{{ $data->product_id }}">
                                            
                                            <input type="number" readonly class="changeQuantity qty-input form-control" value="{{ $data->quantity }}" min="1" max="100"/>                                                     
                                        </td>
                                        <td class="cart-product-grand-total">
                                            <span class="cart-grand-total-price">{{ number_format($data->quantity * $data->amount, 2) }}</span>
                                        </td>
                                        
                                        @php $total = ($data->quantity * $data->amount) @endphp
                                    </tr>
                                    
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td rowspan="1" colspan="1"></td>
                                        <td colspan="3">Total products (tax incl.)</td>
                                        <td colspan="1">{{$total}}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="4"><strong>Total</strong></td>
                                        <td colspan="1"><strong>{{$total}}</strong></td>
                                    </tr>
                                </tfoot>    
                            </table>
                            @endforeach
                        </div>
                        <div class="cart_navigation">
                            <a href="#" class="prev-btn">Continue shopping</a>
                            
                        </div>
                    </div>
                </div>
                <br>
            </div>


    </main>
  <!-- end MAIN --> 
  {{view('front/footer')}}