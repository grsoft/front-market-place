<!-- FOOTER -->
  <footer class="site-footer footer-opt-1">
    <div class="container-fluid">
      <div class="footer-column">
        <div class="row">
          <div class="col-md-3 col-lg-3 col-xs-6 col"> <strong class="logo-footer"> <a href="#"><img src="{{URL::asset('front-html')}}/images//logo-footer.png" width="200" alt="logo"></a> </strong>
            <table class="address">
              <tr>
                <td><b>Address: </b></td>
                <td>Example Street 68, Mahattan,New York, USA</td>
              </tr>
              <tr>
                <td><b>Phone: </b></td>
                <td>+ 65 123 456 789</td>
              </tr>
              <tr>
                <td><b>Email:</b></td>
                <td>Support@ovicsoft.com</td>
              </tr>
            </table>
          </div>
          <div class="col-md-2 col-lg-2 col-xs-6 col">
            <div class="links">
              <h3 class="title">Company</h3>
              <ul>
                <li><a href="about.html">About Us</a></li>
                <li><a href="#">Testimonials</a></li>
                <li><a href="#">Affiliate Program</a></li>
                <li><a href="#">Terms & Conditions</a></li>
              </ul>
            </div>
          </div>
          <div class="col-md-2 col-lg-2 col-xs-6 col">
            <div class="links">
              <h3 class="title">My Account</h3>
              <ul>
                <li><a href="#">My Order</a></li>
                <li><a href="#">My Wishlist</a></li>
                <li><a href="#">My Credit Slip</a></li>
                <li><a href="#">My Addresses</a></li>
                <li><a href="#">My Account</a></li>
              </ul>
            </div>
          </div>
          <div class="col-md-2 col-lg-2 col-xs-6 col">
            <div class="links">
              <h3 class="title">Support</h3>
              <ul>
                <li><a href="#">New User Guide</a></li>
                <li><a href="#">Help Center</a></li>
                <li><a href="#">Refund Policy</a></li>
                <li><a href="#">Report Spam</a></li>
                <li><a href="#">FAQ</a></li>
              </ul>
            </div>
          </div>
          <div class="col-md-3 col-lg-3 col-xs-6 col">
            <div class="block-newletter">
              <div class="block-title">NEWSLETTER</div>
              <div class="block-content">
                <form>
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Your Email Address">
                    <span class="input-group-btn">
                    <button class="btn btn-subcribe" type="button"><span>ok</span></button>
                    </span> </div>
                </form>
              </div>
            </div>
            <div class="block-social">
              <div class="block-title">Let’s Socialize </div>
              <div class="block-content"> <a href="#" class="sh-facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a> <a href="#" class="sh-pinterest"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a> <a href="#" class="sh-vk"><i class="fa fa-vk" aria-hidden="true"></i></a> <a href="#" class="sh-twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a> <a href="#" class="sh-google"><i class="fa fa-google-plus" aria-hidden="true"></i></a> </div>
            </div>
          </div>
        </div>
      </div>
      <div class="payment-methods">
        <div class="block-title"> Accepted Payment Methods </div>
        <div class="block-content"> <img alt="payment" src="{{URL::asset('front-html')}}/images//media/blog/payment1.png"> <img alt="payment" src="{{URL::asset('front-html')}}/images//media/blog/payment2.png"> <img alt="payment" src="{{URL::asset('front-html')}}/images//media/blog/payment3.png"> <img alt="payment" src="{{URL::asset('front-html')}}/images//media/blog/payment4.png"> <img alt="payment" src="{{URL::asset('front-html')}}/images//media/blog/payment5.png"> <img alt="payment" src="{{URL::asset('front-html')}}/images//media/blog/payment6.png"> <img alt="payment" src="{{URL::asset('front-html')}}/images//media/blog/payment7.png"> <img alt="payment" src="{{URL::asset('front-html')}}/images//media/blog/payment8.png"> <img alt="payment" src="{{URL::asset('front-html')}}/images//media/blog/payment9.png"> <img alt="payment" src="{{URL::asset('front-html')}}/images//media/blog/payment10.png"> </div>
      </div>
      <div class="footer-links">
        <ul class="links">
          <li><strong class="title">HOT SEARCHED KEYWORDS:</strong></li>
          <li><a href="#">Xiaomi Mi3 </a></li>
          <li><a href="#">Digiflip Pro XT 712 Tablet</a></li>
          <li><a href="#">Mi 3 Phones </a></li>
          <li><a href="#">Iphone 6 Plus </a></li>
          <li><a href="#">Women's Messenger Bags</a></li>
          <li><a href="#">Wallets </a></li>
          <li><a href="#">Women's Clutches </a></li>
          <li><a href="#">Backpacks Totes</a></li>
        </ul>
        <ul class="links">
          <li><strong class="title">tvs: </strong></li>
          <li><a href="#">Sony TV </a></li>
          <li><a href="#"> Samsung TV </a></li>
          <li><a href="#">LG TV </a></li>
          <li><a href="#">Panasonic TV </a></li>
          <li><a href="#"> Onida TV </a></li>
          <li><a href="#"> Toshiba TV </a></li>
          <li><a href="#"> Philips TV</a></li>
          <li><a href="#">Micromax TV</a></li>
          <li><a href="#">LED TV </a></li>
          <li><a href="#"> LCD TV </a></li>
          <li><a href="#">Plasma TV </a></li>
          <li><a href="#">3D TV </a></li>
          <li><a href="#">Smart TV </a></li>
        </ul>
        <ul  class="links">
          <li><strong class="title">MOBILES: </strong></li>
          <li><a href="#">Moto E </a></li>
          <li><a href="#"> Samsung Mobile </a></li>
          <li><a href="#">Micromax Mobile</a></li>
          <li><a href="#">Nokia Mobile </a></li>
          <li><a href="#"> HTC Mobile </a></li>
          <li><a href="#">Sony Mobile </a></li>
          <li><a href="#"> Apple Mobile </a></li>
          <li><a href="#"> LG Mobile </a></li>
          <li><a href="#">Karbonn Mobile</a></li>
        </ul>
        <ul class="links">
          <li><strong class="title">LAPTOPS:</strong></li>
          <li><a href="#">Apple Laptop </a></li>
          <li><a href="#">Acer Laptop </a></li>
          <li><a href="#">Samsung Laptop</a></li>
          <li><a href="#">Lenovo Laptop </a></li>
          <li><a href="#">Sony Laptop</a></li>
          <li><a href="#">Dell Laptop</a></li>
          <li><a href="#">Asus Laptop </a></li>
          <li><a href="#"> Toshiba Laptop</a></li>
          <li><a href="#">LG Laptop </a></li>
          <li><a href="#">HP Laptop</a></li>
          <li><a href="#">Notebook</a></li>
        </ul>
        <ul class="links">
          <li><strong class="title">WATCHES:</strong></li>
          <li><a href="#">FCUK Watches</a></li>
          <li><a href="#">Titan Watches </a></li>
          <li><a href="#"> Casio Watches </a></li>
          <li><a href="#"> Fastrack Watches </a></li>
          <li><a href="#">Timex Watches </a></li>
          <li><a href="#">Fossil Watches</a></li>
          <li><a href="#">Diesel Watches </a></li>
          <li><a href="#"> Luxury Watches</a></li>
        </ul>
        <ul class="links">
          <li><strong class="title">FOOTWEAR: </strong></li>
          <li><a href="#">Shoes </a></li>
          <li><a href="#">Casual Shoes </a></li>
          <li><a href="#"> Sports Shoes </a></li>
          <li><a href="#">Formal Shoes </a></li>
          <li><a href="#"> Adidas Shoes </a></li>
          <li><a href="#">Gas Shoes</a></li>
          <li><a href="#"> Puma Shoes</a></li>
          <li><a href="#">Reebok Shoes </a></li>
          <li><a href="#">Woodland Shoes </a></li>
          <li><a href="#">Red tape Shoes</a></li>
          <li><a href="#">Nike Shoes</a></li>
        </ul>
      </div>
      <div class="footer-bottom">
        <div class="links">
          <ul>
            <li><a href="#"> Company Info – Partnerships </a></li>
          </ul>
          <ul>
            <li><a href="#">Online Shopping </a></li>
            <li><a href="#">Promotions </a></li>
            <li><a href="#">My Orders </a></li>
            <li><a href="#">Help </a></li>
            <li><a href="#">Site Map </a></li>
            <li><a href="#">Customer Service </a></li>
            <li><a href="#">Seller of Afremarkete</a></li>
          </ul>
          <ul>
            <li><a href="#">Most Populars </a></li>
            <li><a href="#">Best Sellers </a></li>
            <li><a href="#">New Arrivals </a></li>
            <li><a href="#">Special Products </a></li>
            <li><a href="#"> Manufacturers </a></li>
            <li><a href="#">Our Stores </a></li>
            <li><a href="#">Shipping </a></li>
            <li><a href="#">Payments </a></li>
            <li><a href="#">Payments </a></li>
            <li><a href="#">Refunds </a></li>
          </ul>
          <ul>
            <li><a href="#">Terms & Conditions </a></li>
            <li><a href="#">Policy </a></li>
            <li><a href="#">Policy </a></li>
            <li><a href="#"> Shipping </a></li>
            <li><a href="#">Payments </a></li>
            <li><a href="#">Returns </a></li>
            <li><a href="#">Refunds </a></li>
            <li><a href="#">Warrantee </a></li>
            <li><a href="#">FAQ </a></li>
            <li><a href="contact.html">Contact </a></li>
          </ul>
        </div>
      </div>
      <div class="copyright"> Copyright © Afremarkete. All Rights Reserved. Designed by <a href="https://www.digitalprisma.com/" target="_blank">DigitalPrisma.com</a> </div>
    </div>
  </footer>
  <!-- end FOOTER --> 
  
  <!--back-to-top  --> 
  <a href="#" class="back-to-top"> <i aria-hidden="true" class="fa fa-angle-up"></i> </a> </div>

<!-- jQuery --> 
<script type="text/javascript" src="{{ URL::asset('front-html/js/jquery.min.js') }}"></script> 

<!-- sticky --> 
<script type="text/javascript" src="{{ URL::asset('front-html/js/jquery.sticky.js') }}"></script> 

<!-- OWL CAROUSEL Slider --> 
<script type="text/javascript" src="{{ URL::asset('front-html/js/owl.carousel.min.js') }}"></script> 

<!-- Boostrap --> 
<script type="text/javascript" src="{{ URL::asset('front-html/js/bootstrap.min.js') }}"></script> 

<!-- Countdown --> 
<script type="text/javascript" src="{{ URL::asset('front-html/js/jquery.countdown.min.js') }}"></script> 

<!--jquery Bxslider  --> 
<script type="text/javascript" src="{{ URL::asset('front-html/js/jquery.bxslider.min.js') }}"></script> 

<!-- actual --> 
<script type="text/javascript" src="{{ URL::asset('front-html/js/jquery.actual.min.js') }}"></script> 
<!-- jQuery UI --> 
<script type="text/javascript" src="{{ URL::asset('front-html/js/jquery-ui.min.js') }}"></script> 
<!-- Chosen jquery--> 
<script type="text/javascript" src="{{ URL::asset('front-html/js/chosen.jquery.min.js') }}"></script> 

<!-- parallax jquery--> 
<script type="text/javascript" src="{{ URL::asset('front-html/js/jquery.parallax-1.1.3.js') }}"></script> 

<!-- elevatezoom --> 
<script type="text/javascript" src="{{ URL::asset('front-html/js/jquery.elevateZoom.min.js') }}"></script> 

<!-- fancybox --> 
<script src="{{ URL::asset('front-html/js/fancybox/source/jquery.fancybox.pack.js') }}"></script> 
<script src="{{ URL::asset('front-html/js/fancybox/source/helpers/jquery.fancybox-media.js') }}"></script> 
<script src="{{ URL::asset('front-html/js/fancybox/source/helpers/jquery.fancybox-thumbs.js') }}"></script> 
<!-- alertifyjs -->
<script src="{{ URL::asset('front-html/alertifyjs/alertify.js') }}"></script> 
<!-- alertifyjs -->
<!-- arcticmodal --> 
<script src="{{ URL::asset('front-html/js/arcticmodal/jquery.arcticmodal.js') }}"></script> 

<!-- Main --> 
<script type="text/javascript" src="{{ URL::asset('front-html/js/main.js') }}"></script>
<script>

        (function($) {

            "use strict";

            $(document).ready(function() {

                /*  [ Filter by price ]

                - - - - - - - - - - - - - - - - - - - - */

                $('#slider-range').slider({

                    range: true,

                    min: 0,

                    max: 500,

                    values: [0, 300],

                    slide: function (event, ui) {

                        $('#amount-left').text(ui.values[0] );
                        $('#amount-right').text(ui.values[1] );

                    }

                });

                $('#amount-left').text($('#slider-range').slider('values', 0));

                $('#amount-right').text($('#slider-range').slider('values', 1));
            }); 

        })(jQuery);
      /*  [ input number ]
        - - - - - - - - - - - - - - - - - - - - */
        $('.btn-cart').on( 'click', function(e) {
       
            e.preventDefault();
            
            var prod_id = $(this).attr('data-pid');
            var qty      = $(this).attr('data-qty');
            var input;
            if(qty=='getinput')
            {
              input = $("input[name=qty1]").val();
            }
            else
            {
              input=qty;
            }
            var currentVal = parseInt(input);
            if (!isNaN(currentVal)) {
              $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
              });

              var product_id = prod_id;
              var quantity = input;

              $.ajax({
                  url: "{{URL('/add-to-cart')}}",
                  method: "POST",
                  data: {
                      'quantity': quantity,
                      'product_id': product_id,
                  },
                  success: function (response) {
                      alertify.set('notifier','position','bottom-right');
                      alertify.success(response.status);
                      cartload();
                  },
                  error:function(x,y,z)
                  {
                    alert('MY');
                  }
              });
            } 
        });
        cartload();
        function cartload()
        {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url: "{{URL('/load-cart-data')}}",
                method: "GET",
                success: function (response) {
                    $('.counter-number').html('');
                    var parsed = jQuery.parseJSON(response)
                    var value = parsed; //Single Data Viewing
                    $('.counter-number').html(value['totalcart']);
                    $('.counter qty').html($('<span class="counter-number">'+ value['totalcart'] +'</span>'));
                    if(value['cart_item'] != "")
                    {
                      
                      $('#cartitem').html(value['cart_item']);
                      $('#cart_total').html(value['total']);
                      $('#totalitem').html(value['totalcart']);
                      $('.cartdiv').css('display','block');

                      $('#cartitems').html(value['cart_item']);
                      $('#cart_totals').html(value['total']);
                      $('#totalitems').html(value['totalcart']);
                      $('.cartdivs').css('display','block');
                    }
                    else{
                      $('.cartdiv').css('display','none');
                      $('.cartdivs').css('display','none');
                    }
                }
            });
        }
        

          $('.increment-btn').click(function (e) {
              e.preventDefault();
              var incre_value = $(this).parents('.quantity').find('.qty-input').val();
              var value = parseInt(incre_value, 10);
              value = isNaN(value) ? 0 : value;
              if(value<10){
                  value++;
                  $(this).parents('.quantity').find('.qty-input').val(value);
              }
          });

          $('.decrement-btn').click(function (e) {
              e.preventDefault();
              var decre_value = $(this).parents('.quantity').find('.qty-input').val();
              var value = parseInt(decre_value, 10);
              value = isNaN(value) ? 0 : value;
              if(value>1){
                  value--;
                  $(this).parents('.quantity').find('.qty-input').val(value);
              }
          });
          $('.changeQuantity').on('change',function (e) {
              e.preventDefault();

              var quantity = $(this).closest(".cartpage").find('.qty-input').val();
              var product_id = $(this).closest(".cartpage").find('.product_id').val();

              var data = {
                  '_token': $('input[name=_token]').val(),
                  'quantity':quantity,
                  'product_id':product_id,
              };

              $.ajax({
                  url: "{{URL('/update-to-cart')}}",
                  type: 'POST',
                  data: data,
                  success: function (response) { 
                      window.location.reload();
                      alertify.set('notifier','position','buttom-right');
                      alertify.success(response.status);
                  }
              });
          });
          $('.delete_cart_data').click(function (e) { 
              e.preventDefault();

              var product_id = $(this).closest(".cartpage").find('.product_id').val();

              var data = {
                  '_token': $('input[name=_token]').val(),
                  "product_id": product_id,
              };

              // $(this).closest(".cartpage").remove();

              $.ajax({
                  url: "{{URL('/delete-from-cart')}}",
                  type: 'POST',
                  data: data,
                  success: function (response) {
                      window.location.reload();
                      alertify.set('notifier','position','buttom-right');
                      alertify.success(response.status);
                  },
                  error:function(x,y,z)
                  {
                    alert('MY');
                  }
              });
          });
          
          $('.clear_cart').click(function (e) {
              e.preventDefault();

              $.ajax({
                  url: "{{URL('/clear-cart')}}",
                  type: 'GET',
                  success: function (response) {
                      window.location.reload();
                      alertify.set('notifier','position','top-right');
                      alertify.success(response.status);
                  }
              });

          });
      
    </script>
    
    
</body>

</html>