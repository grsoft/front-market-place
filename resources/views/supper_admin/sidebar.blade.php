 <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="{{ URL::asset('admins/dist/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
              <p>Super Admin</p>

              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MENU</li>
            <li class="active"><a href="{{ URL('supper_admin/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            
           
            <li class="treeview">
              <a href="#">
                <i class="fa fa-edit"></i> <span>Admin</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{ URL('supper_admin/admin/form') }}"><i class="fa fa-circle-o"></i> Create Admin</a></li>
                <li><a href="{{ URL('supper_admin/admin/list') }}"><i class="fa fa-circle-o"></i> Admin List</a></li>
                
              </ul>
            </li> 
            <li><a href="{{ URL('supper_admin/customer/list') }}"><i class="fa fa-edit"></i> Customer</a></li>
            <li><a href="{{ URL('supper_admin/seller/list') }}"><i class="fa fa-edit"></i> Seller</a></li>
            <!-- <li><a href="report_excel.php"><i class="fa fa-edit"></i> Excel Report</a></li> -->
            <li class="treeview">
              <a href="#">
                <i class="fa fa-edit"></i> <span>Category</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{ URL('supper_admin/category/form') }}"><i class="fa fa-circle-o"></i> Add Category</a></li>
                <li><a href="{{ URL('supper_admin/category/list') }}"><i class="fa fa-circle-o"></i> Category List</a></li>
                
              </ul>
            </li> 
            <li class="treeview">
              <a href="#">
                <i class="fa fa-edit"></i> <span>Sub Category</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{ URL('supper_admin/category/sub-form') }}"><i class="fa fa-circle-o"></i> Add Sub Category</a></li>
                <li><a href="{{ URL('supper_admin/category/sub-list') }}"><i class="fa fa-circle-o"></i> Sub Category List</a></li>
                
              </ul>
            </li> 
            <li class="treeview">
              <a href="#">
                <i class="fa fa-edit"></i> <span>Product</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{ URL('supper_admin/product/form') }}"><i class="fa fa-circle-o"></i> Add Product</a></li>
                <li><a href="{{ URL('supper_admin/product/list') }}"><i class="fa fa-circle-o"></i> Product List</a></li>
                
              </ul>
            </li> 
            <li class="treeview">
              <a href="#">
                <i class="fa fa-edit"></i> <span>Product Approvel</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{ URL('supper_admin/product/pending') }}"><i class="fa fa-circle-o"></i> Pending</a></li>
                <li><a href="{{ URL('supper_admin/product/approved') }}"><i class="fa fa-circle-o"></i> Approved</a></li>
                
              </ul>
            </li> 
            <li class="treeview">
              <a href="#">
                <i class="fa fa-edit"></i> <span>Banner</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{ URL('supper_admin/banner/home-banner-list') }}"><i class="fa fa-circle-o"></i> Home Banner</a></li>
                <li><a href="{{ URL('supper_admin/banner/category-banner-form') }}"><i class="fa fa-circle-o"></i> Category Banner</a></li>
                
              </ul>
            </li> 
           
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>