@extends('supper_admin/master')
@section('content')
      <!-- Right side column. Contains the navbar and content of the page -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            
            <small></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Banner</a></li>
            <li class="active">Form</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          @if (session('success'))
              <div class="alert alert-success">
                  {{ session('success') }}
              </div>
          @endif
          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Add Banner Image</h3> <a href="{{ URL('supper_admin/banner/home-banner-form') }}" title="Add Banner Image" class="btn btn-info">Add New Image</a>
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <form method="post" enctype='multipart/form-data' action="{{ URL('/supper_admin/banner/home-banner-save')}}">
              @csrf
              <div class="box-body">
                <div class="row">
                  <div class="col-md-12">
                    <table class="table table-responsive">
                        <thead>
                            <th>Title/Url</th>
                            <th>Image</th>
                            <th>Action</th>
                        </thead>
                        @foreach($data as $key => $value)
                        <tr>
                            <td>Title: {{$value->title}}<br>URL: {{$value->url}}</td>
                            <td><img src="{{ asset('/')}}home_banner/{{$value->image}}" width="200px" height="100px"></td>
                            <td>
                                <a href="{{ URL('supper_admin/banner/home-banner-edit')}}/{{$value->id}}" title="Edit" class="btn btn-warning"><i class="fa fa-edit"></i></a>
                                <a href="{{ URL('supper_admin/banner/home-banner-delete')}}/{{$value->id }}" onclick="return confirm('Are you sure to delete image ?') " title="Delete" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                  </div>
                </div>
              </div><!-- /.box-body -->
              
            </form>
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
@endsection    
