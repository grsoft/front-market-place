@extends('supper_admin/master')
@section('content')
      <!-- Right side column. Contains the navbar and content of the page -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            
            <small></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Seller</a></li>
            <li class="active">List</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          @if (session('success'))
              <div class="alert alert-success">
                  {{ session('success') }}
              </div>
          @endif
          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">{{$title}}</h3>
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <form method="post" enctype='multipart/form-data' >
              @csrf
              <div class="box-body table-responsive">
                
                <div class="row">
                  <div class="col-md-12">
                    <table id="example2" class="table">
                      <thead>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Shop Name</th>
                        <th>State</th>
                        <th>city</th>
                        <th>mobile_no</th>
                        <th>Address</th>
                        <th>action pending</th>
                      </thead>
                      <tbody>
                        @foreach($data as $row)
                        <tr>
                          <td>{{$row->first_name}} {{$row->last_name}}</td>
                          <td>{{$row->email_address}}</td>
                          <td>{{$row->shop_name}}</td>
                          <td>{{$row->state}}</td>
                          <td>{{$row->city}}</td>
                          <td>{{$row->mobile_number}}</td>
                          <td>{{$row->address}}</td>
                          <td>
                            <a href="{{URL('supper_admin/product/edit')}}/{{$row->id}}" class="btn btn-warning" title="Edit"><i class="fa fa-edit"></i></a>
                            <a href="{{URL('supper_admin/seller/view')}}/{{$row->id}}" class="btn btn-success" title="Edit"><i class="fa fa-eye"></i></a>
                            <a onclick="return confirm('Are you sure to delete category')" href="{{URL('supper_admin/category/delete')}}/{{$row->id}}" class="btn btn-danger" title="Delete"><i class="fa fa-trash"></i></a>
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div><!-- /.box-body -->
              <div class="box-footer">
                
              </div><!-- /.box-footer-->
            </form>
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
@endsection    
