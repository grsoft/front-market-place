@extends('supper_admin/master')
@section('content')
      <style type="text/css">
        @media screen and (min-width: 768px) {
          .text-sm {
            width:150px;
          }
          .text-large {
            width:450px;
          }
        }
        ul.tul {
            display: inline-flex;
            list-style: decimal;
        }
        .tul li{
          margin-right: 20px;
        }
      </style>
      <!-- Right side column. Contains the navbar and content of the page -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            
            <small></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Seller</a></li>
            <li class="active">View</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          @if (session('success'))
              <div class="alert alert-success">
                  {{ session('success') }}
              </div>
          @endif
          <div class="row">
            
            <form class="formtag " id="brand" method="post" enctype='multipart/form-data' >
              @csrf 
              
            <div class="col-md-12">
               
              
                <div class="box box-primary">
                  <div class="box-header with-border">
                    <h3 class="box-title">Seller details of {{$data[0]->first_name}} {{$data[0]->last_name}}</h3>
                    
                  </div><!-- /.box-header -->
                  <div class="box-body no-padding">
                    
                    <div class="table-responsive mailbox-messages">
                      
                      <table class="table table-hover table-striped">
                        <tbody>
                          
                          <tr>
                            <td>Created On</td>
                            <td>{{$data[0]->created_at}}</td>
                            <td>Name</td>
                            <td>{{$data[0]->first_name}} {{$data[0]->last_name}}</td>
                          </tr>
                          <tr>
                            <td>Shop Name</td>
                            <td>{{$data[0]->shop_name}}
                            <td>Email Address</td>
                            <td>{{$data[0]->email_address}} </td>
                          </tr>
                          <tr>
                            <td>Address</td>
                            <td>{{$data[0]->address}} </td>
                            <td>State</td>
                            <td>{{$data[0]->state}} </td>
                          </tr>
                          <tr>
                            <td>City</td>
                            <td>{{$data[0]->city}} </td>
                            <td>Country</td>
                            <td>{{$data[0]->country}} </td>
                          </tr>
                          <tr>
                            <td>Postal Code</td>
                            <td>{{$data[0]->postal_code}} </td>
                            <td>Mobile Number</td>
                            <td>{{$data[0]->mobile_number}} </td>
                          </tr>
                        </tbody>
                      </table><!-- /.table -->
                    </div><!-- /.mail-box-messages -->
                  </div><!-- /.box-body -->
                  
                </div><!-- /. box -->
              
                
                <div class="box box-primary">
                  <div class="box-header with-border">
                    <h3 class="box-title">Post Item List</h3>
                    
                  </div><!-- /.box-header -->
                  <div class="box-body no-padding">
                    
                    <div class="table-responsive mailbox-messages">
                    <table id="example2" class="table">
                      <thead>
                        <th>Brand</th>
                        <th>Title</th>
                        <th>Mrp</th>
                        <th>Disc. %</th>
                        <th>Disc. Amt</th>
                        <th>Sell Price</th>
                        <th>Action</th>
                      </thead>
                      <tbody>
                        @foreach($seller_item as $row)
                        <tr>
                          <td>{{$row->brand}}</td>
                          <td>{{$row->title}}</td>
                          <td>{{$row->mrp}}</td>
                          <td>{{$row->discount}}</td>
                          <td>{{$row->discount_amt}}</td>
                          <td>{{$row->sell_price}}</td>
                          <td>
                            <a href="{{URL('supper_admin/product/view-approved')}}/{{$row->id}}" class="btn btn-success" title="View"><i class="fa fa-eye"></i></a>
                            
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                      </table><!-- /.table -->
                    </div><!-- /.mail-box-messages -->
                  </div><!-- /.box-body -->
                  
                </div><!-- /. box -->
              
            </div><!-- /.col -->
            </form>
          </div><!-- /.row -->
        </section>
        
      </div><!-- /.content-wrapper -->
      <script src="{{URL::asset('admins/plugins/iCheck/icheck.min.js') }}" type="text/javascript"></script>
      
@endsection    
