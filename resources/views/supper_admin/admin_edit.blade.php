@extends('supper_admin/master')
@section('content')
      <!-- Right side column. Contains the navbar and content of the page -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            
            <small></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">admin</a></li>
            <li class="active">Edit</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          @if (session('success'))
              <div class="alert alert-success">
                  {{ session('success') }}
              </div>
          @endif
          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Admin</h3>
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <form method="post" enctype='multipart/form-data' action="{{ URL('/supper_admin/admin/admin-update')}}">
              @csrf
              <input type="hidden" name="aid" value="{{$data->id}}" required>
              <div class="box-body">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="">Email</label>
                      <input type="text" name="email" class="form-control" value="{{$data->email}}" required>
                      @error('email')
                      <label class="text-danger">{{ $message }}</label>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="">Password</label>
                      <input type="text" name="password" class="form-control" value="{{$data->password}}" required>
                    </div>
                    @error('password')
                    <label class="text-danger">{{ $message }}</label>
                    @enderror
                    <div class="form-group">
                      <label for="">Full Name</label>
                      <input type="text" name="name" class="form-control" value="{{$data->name}}" required>
                    </div>
                    @error('name')
                    <label class="text-danger">{{ $message }}</label>
                    @enderror
                    <div class="form-group">
                      <label for="">Image</label>
                      <input type="hidden" name="oldimage" value="{{$data->image}}">
                      <input type="file" name="a_image" class="form-control">
                      @if($data->image!="")
                      <img src="{{ asset('/')}}admin_image/{{$data->image}}" width="100px" height="100px">
                      @else
                      <img src="{{ asset('/')}}img/default.png" width="100px" height="100px">
                      @endif
                    </div>
                  </div>
                </div>
              </div><!-- /.box-body -->
              <div class="box-footer">
                
                <button type="submit" class="btn btn-success">Save</button>
              </div><!-- /.box-footer-->
            </form>
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
@endsection    
