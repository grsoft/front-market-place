<?php
    $formdata = DB::table('form_table')->get();//select("SELECT * FROM `form_table` ");
    //$max= $datamrp[0]->maxmrp;
    $form_option='<option value="">Select Form</option>';
     for($i=0;$i<count($formdata);$i++){ 
        $form_option.='<option title="'.$formdata[$i]->name_for_product.'" value="'.$formdata[$i]->id.'">'.$formdata[$i]->form_name.'</option>';
     } ?>
    
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Supper Admin | {{$title}}</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="{{URL::asset('admins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />    
    <!-- FontAwesome 4.3.0 -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons 2.0.0 -->
    <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />  
    <link href="{{URL::asset('admins/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" type="text/css" />  
    <!-- Theme style -->
    <link href="{{URL::asset('admins/dist/css/AdminLTE.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins 
         folder instead of downloading all of them to reduce the load. -->
    <link href="{{URL::asset('admins/dist/css/skins/_all-skins.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- iCheck -->
    <link href="{{URL::asset('admins/plugins/iCheck/flat/blue.css') }}" rel="stylesheet" type="text/css" />
    <!-- Morris chart -->
    <link href="{{URL::asset('admins/plugins/morris/morris.css') }}" rel="stylesheet" type="text/css" />
    <!-- jvectormap -->
    <link href="{{URL::asset('admins/plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}" rel="stylesheet" type="text/css" />
    <!-- Date Picker -->
    <link href="{{URL::asset('admins/plugins/datepicker/datepicker3.css') }}" rel="stylesheet" type="text/css" />
    <!-- Daterange picker -->
    <link href="{{URL::asset('admins/plugins/daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet" type="text/css" />
    <!-- bootstrap wysihtml5 - text editor -->
    <link href="{{URL::asset('admins/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet" type="text/css" />

    <link href="{{URL::asset('admins/plugins/iCheck/square/blue.css') }}" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  	<body class="skin-blue">
    <div class="wrapper">
		{{View::make('supper_admin/header')}}
		{{View::make('supper_admin/sidebar')}}
		@yield('content')
		{{View::make('supper_admin/footer')}}
	</div><!-- ./wrapper -->

    <!-- jQuery 2.1.3 -->
    <script src="{{URL::asset('admins/plugins/jQuery/jQuery-2.1.3.min.js') }}"></script>
    
    <!-- Bootstrap 3.3.2 JS -->
    <script src="{{URL::asset('admins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>    
    <!-- Morris.js charts -->
    <script src="{{URL::asset('admins/plugins/datatables/jquery.dataTables.js') }}" type="text/javascript"></script>
    <script src="{{URL::asset('admins/plugins/datatables/dataTables.bootstrap.js') }}" type="text/javascript"></script>
    
    <script src="{{URL::asset('admins/plugins/morris/morris.min.js') }}" type="text/javascript"></script>
    <!-- Sparkline -->
    <script src="{{URL::asset('admins/plugins/sparkline/jquery.sparkline.min.js') }}" type="text/javascript"></script>
    <!-- jvectormap -->
    <script src="{{URL::asset('admins/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}" type="text/javascript"></script>
    <script src="{{URL::asset('admins/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}" type="text/javascript"></script>
    <!-- jQuery Knob Chart -->
    <script src="{{URL::asset('admins/plugins/knob/jquery.knob.js') }}" type="text/javascript"></script>
    <!-- daterangepicker -->
    <script src="{{URL::asset('admins/plugins/daterangepicker/daterangepicker.js') }}" type="text/javascript"></script>
    <!-- datepicker -->
    <script src="{{URL::asset('admins/plugins/datepicker/bootstrap-datepicker.js') }}" type="text/javascript"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="{{URL::asset('admins/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}" type="text/javascript"></script>
    <script src="{{URL::asset('admins/plugins/ckeditor/ckeditor.js') }}"  type="text/javascript" /></script>
    <!-- iCheck -->
    <script src="{{URL::asset('admins/plugins/iCheck/icheck.min.js') }}" type="text/javascript"></script>
    <!-- Slimscroll -->
    <script src="{{URL::asset('admins/plugins/slimScroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
    <!-- FastClick -->
    <script src="{{URL::asset('admins/plugins/fastclick/fastclick.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{URL::asset('admins/dist/js/app.min.js ') }}" type="text/javascript"></script>

    
    <!-- AdminLTE for demo purposes -->
    <script src="{{URL::asset('dist/js/demo.js') }}" type="text/javascript"></script>
    <!-- my text validation -->
    <script src="{{URL::asset('admins/textvalidation.js ') }}" type="text/javascript"></script>
    <script>
    $('.alert').delay(10000).fadeOut();
        
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
          checkboxClass: 'icheckbox_minimal-blue',
          radioClass: 'iradio_minimal-blue'
        });
    </script>
    <script type="text/javascript">
      $(function () {
        $("#example1").dataTable();
        $('#example2').dataTable({
          "bPaginate": true,
          "bLengthChange": false,
          "bFilter": true,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": false
        });
      });
      CKEDITOR.replace( 'text' );
	  $(".numericOnly").keypress(function (e) {
			if (String.fromCharCode(e.keyCode).match(/[^0-9]/g)) return false;
		});
		var _URL = window.URL;
		$("#file").change(function (e) {
			var file, img;
			if ((file = this.files[0])) {
				img = new Image();
				img.onload = function () {
					if(this.width>160 || this.height>160)
					{
						alert("Width:" + this.width + "   Height: " + this.height + " Upload width:160, height:160");
						$("#file").val(null);
					}
					
				};
				img.src = _URL.createObjectURL(file);
			}
		});
    </script>
    <script type="text/javascript">
    var nested_cat_id=[];
    var divid=$('#dividval').val();
    function removediv(afterdiv)
    {

        iv=parseInt(afterdiv+1);
        
        //console.log(iv);
        var nid=$('#nested_id').val();
        while(divid>afterdiv) {
            $("#div"+divid).remove();
            delete nested_cat_id[divid];
            divid--;  

        }

    }
      $( document ).ready(function() {
        $('.category').change(function(){
            let indexvalue=$(this).children('option:selected').index();
            let cval=$(this).find('option:selected').val();

            removediv(0);
            
            if(indexvalue>0)
            {

                nested_cat_id[divid]=cval;
                
                $.ajax({

                    url: "{{URL('/supper_admin/category/ajax-subcate-by-cat-id')}}/"+cval,
                    data: {},
                    type: "get",
                    success: function(result){ 
                        divid++;
                        $('#cat').append('<div id="div'+divid+'" class="col-md-12 subcat"><div class="form-group"><label for="">Sub Category</label><select data-div="'+divid+'" id="sub'+divid+'" class="sub-category form-control" onchange="changesub(this)" required><option  value="">Select One</option><option value="">Add new...</option></select></div></div>');
                        
                        for (let i = 0; i < result.length; i++) {
                        $('#sub'+divid).append(`<option data-div="${divid}" value="${result[i]['id']}">${result[i]['title']}</option>`);
                        }
                    }, 
                    error: function(x,y,z){
                          alert(y);
                     }
                });
                
            }
        });
        
      });
      function changesub(e){ 
            let indexvalue=$(e).children('option:selected').index();
            let cval=$(e).find('option:selected').val();
            let datadiv=$(e).data('div');
            removediv(datadiv);
            if(indexvalue==1)
            {
                var tree= nested_cat_id.join(',');
                $('#treecategory_idhidden').val(tree);
                let cateid=$('#category_id').val();
                $('#category_idhidden').val(cateid);
                $('#childecategory_idhidden').val(nested_cat_id[nested_cat_id.length - 1]);
                $('#addscategory').modal({
                  backdrop: false
                }); 
            }
            else if(indexvalue>1)
            {
                nested_cat_id[divid]=cval;
                
                $.ajax({

                    url: "{{URL('/supper_admin/category/ajax-childe-by-subcat-id')}}/"+cval,
                    data: {},
                    type: "get",
                    success: function(result){ 
                        divid++;
                        $('#cat').append('<div data-div="'+divid+'" id="div'+divid+'" class="col-md-12 subcat"><div class="form-group"><label for="">Sub Category</label><select data-div="'+divid+'" id="sub'+divid+'" class="sub-category form-control" onchange="changesub(this)" required><option value="">Select One</option><option value="">Add new...</option></select></div></div>');
                        
                        for (let i = 0; i < result.length; i++) {
                        $('#sub'+divid).append(`<option value="${result[i]['id']}">${result[i]['title']}</option>`);
                        }
                    }, 
                    error: function(x,y,z){
                          alert(y);
                     }
                });
            }
        };
       
        function modal_text_box(e){ 
          var formoption='<?= $form_option ?>';
          
            $('#model-body-html').append('<tr><td width="50%"><div id="deldiv" class="form-group"><input type="text" name="title[]" class="form-control" ></div></td><td width="50%"><div id="deldiv" class="form-group"><select name="form[]" class="form-control">'+formoption+'</select></div></td><td><button  class="btn btn-danger" id="delbtnmod" type="button">X</button></td></tr>');
        }
        //onclick="removeModeldiv(this)" 
        function removeModeldiv(e)
        {
            $(e).remove();
        }
        $("#model-body-html").on("click", "#delbtnmod", function() {
           $(this).closest("tr").remove();
        });
    </script>
    <!--form script -->
    <script type="text/javascript">
        $( document ).ready(function() {
        $('.fcategory').change(function(){
            let indexvalue=$(this).children('option:selected').index();
            let cval=$(this).find('option:selected').val();

            removediv(0);
            
            if(indexvalue>0)
            {

                nested_cat_id[divid]=cval;
                
                $.ajax({

                    url: "{{URL('/supper_admin/category/ajax-subcate-by-cat-id')}}/"+cval,
                    data: {},
                    type: "get",
                    success: function(result){ 
                        divid++;
                        $('#cat').append('<div id="div'+divid+'" class="col-md-3 subcat"><div class="form-group"><label for="">Sub Category</label><select data-div="'+divid+'" id="sub'+divid+'" name="sub_cate[]" class="sub-category form-control catbox" size="15" multiple onchange="fchangesub(this)" required><option  value="">Select One</option></select></div></div>');
                        
                        for (let i = 0; i < result.length; i++) {
                        $('#sub'+divid).append(`<option data-div="${divid}" value="${result[i]['id']}">${result[i]['title']}</option>`);
                        }
                    }, 
                    error: function(x,y,z){
                          alert(y);
                     }
                });
                
            }
        });
        
      });
      function fchangesub(e){ 
            let indexvalue=$(e).children('option:selected').index();
            let cval=$(e).find('option:selected').val();
            let datadiv=$(e).data('div');
            removediv(datadiv);
            if(indexvalue>0)
            {
                nested_cat_id[divid]=cval;
                
                $.ajax({

                    url: "{{URL('/supper_admin/category/ajax-childe-by-subcat-id')}}/"+cval,
                    data: {},
                    type: "get",
                    success: function(result){ 
                        if(result.length>0){
                        divid++;
                        $('#cat').append('<div data-div="'+divid+'" id="div'+divid+'" class="col-md-3 subcat"><div class="form-group"><label for="">Sub Category</label><select data-div="'+divid+'" id="sub'+divid+'" name="sub_cate[]" class="sub-category form-control catbox" size="15" multiple onchange="fchangesub(this)" required><option value="">Select One</option></select></div></div>');
                        
                        for (let i = 0; i < result.length; i++) {
                        $('#sub'+divid).append(`<option value="${result[i]['id']}">${result[i]['title']}</option>`);
                        }
                        }
                        else{
                            $('#gobutton').show();
                        }
                    }, 
                    error: function(x,y,z){
                          alert(y);
                     }
                });
            }
        };
        $('.form').click(function(){
            var formid=$(this).data('link');
            $('.formtag').addClass('hidden');
            $('#'+formid).removeClass('hidden');
            //tab side
            $('.tab').removeClass('active');
            $('.'+formid).addClass('active');
        });
        </script>
        <!-- end form script -->
    </script>
    <script type="text/javascript">
        $('.editsub').on('click',function(){
            $('#id').val($(this).data('id'));
            $('#title').val($(this).data('title'));
            $('#formname').val($(this).data('form'));
            $('#editcategory').modal({
              backdrop: false
            });
        });
        $('.categoryform').change(function(){
            let indexvalue=$(this).children('option:selected').index();
            let cval=$(this).find('option:selected').val();
            $.ajax({

                url: "{{URL('/supper_admin/product/ajax-subcate-by-cat-id')}}/"+cval,
                data: {},
                type: "get",
                success: function(result){ 
                    $('#chckcategory').empty();
                    //$('#chckcategory').append(`<option value="">Select One</option>`);
                    for (let i = 0; i < result.length; i++) {
                    $('#chckcategory').append(`<div class="checkbox icheck">
                              <label>
                                <input type="checkbox" name="sub[]" value="${result[i]['id']}"> ${result[i]['title']}
                              </label>
                            </div>`);
                    $('input').iCheck({
                      checkboxClass: 'icheckbox_square-blue',
                      radioClass: 'iradio_square-blue',
                      increaseArea: '20%' // optional
                    });
                    }
                }, 
                error: function(x,y,z){
                      alert(y);
                 }
            });
           
        });
      
    </script>
    <script type="text/javascript">
       var HomeBannerloadFileSingle = function(event) {
            var output = document.getElementById('hbimage');
            output.src = URL.createObjectURL(event.target.files[0]);
        }
        var loadFileSingle = function(event) {
            var output = document.getElementById('image');
            output.src = URL.createObjectURL(event.target.files[0]);
        }
      var loadFile = function(event) {
        var fi = document.getElementById('fileToUpload');
        var length=fi.files.length;
        // VALIDATE OR CHECK IF ANY FILE IS SELECTED.
        if (fi.files.length > 0) {
          // THE TOTAL FILE COUNT.
          var dtime=$.now();
          for (var i = 0; i <= length ; i++) {
            //element.innerHTML(para1);
            $('#dropContainer').append(`<div id='id${dtime}${i}' class='text-center' style='display:inline-block;width:110px;height:180px;margin:5px;margin-top:-80px;'><div style='padding:5px;border:solid 2px;'><img class='loading' width='70px' height='70px' src='${URL.createObjectURL(fi.files[i])}'> </div><button data-id='id${dtime}${i}' value='${fi.files.item(i).name}' class=' ridrop' onclick='removeimage(this)' type='button'>Remove</button></div>`);
            
            $('.ridrop').addClass("btn btn-block btn-danger");
            
            
            }
           
        }
      };
      dropContainer.ondragover = dropContainer.ondragenter = function(evt) {
          evt.preventDefault();
          $('#dropContainer').css('border','dotted green');
        };
        $('#browsfile').click(function(){
            $('#fileToUpload').trigger('click');
        });
        function dragEnd()
        {
            $('#dropContainer').css('border','solid 1px');
        }
         
        dropContainer.ondrop = function(evt) {
          // pretty simple -- but not for IE :(
          $('#dropContainer').css('border','solid 1px');
          var fi = document.getElementById('fileToUpload');
          var preno=fi.files.length;
          var newno=evt.dataTransfer.files.length;
          const dT = new DataTransfer();
          
          for (let i = 0; i < preno; i++) {
            
            dT.items.add(fi.files[i]); // here you exclude the file. thus removing it.
          }
          console.log(evt.dataTransfer.files[0])
          for(var x=0;x<newno;x++)
          {
            
            dT.items.add(evt.dataTransfer.files[x]) ;
          }
          fileToUpload.files = dT.files;
          //fileToUpload.files = evt.dataTransfer.files;
          
           evt.stopPropagation();
          evt.preventDefault();
          
          //var fi = document.getElementById('fileToUpload');
        // VALIDATE OR CHECK IF ANY FILE IS SELECTED.
        if (newno > 0) { var dtime=$.now();
          // THE TOTAL FILE COUNT.
          //document.getElementById('new').innerHTML = 'Total Files: <b>' + fi.files.length + '</b></br >';
          // RUN A LOOP TO CHECK EACH SELECTED FILE.
          for (var i = preno; i <= fi.files.length ; i++) {
            var fname = fi.files.item(i).name;      // THE NAME OF THE FILE.
            var fsize = fi.files.item(i).size;      // THE SIZE OF THE FILE.
            
            //element.innerHTML(para1);
            $('#dropContainer').append(`<div id='id${dtime}${i}' class='text-center' style='display:inline-block;width:110px;height:180px;margin:5px;margin-top:-80px;'><div style='padding:5px;border:solid 2px;'><img class='loading' width='70px' height='70px' src='${URL.createObjectURL(fi.files[i])}'> </div><button data-id='id${dtime}${i}' value='${fi.files.item(i).name}' class=' ridrop' onclick='removeimage(this)' type='button'>Remove</button></div>`);
            
            $('.ridrop').addClass("btn btn-block btn-danger");
          }
        }
        };
      function removeimage(evt){
        var fi = document.getElementById('fileToUpload');
          var preno=fi.files.length;
          removediv=$(evt).data("id");
          imagename=$(evt).val();
          const dT = new DataTransfer();
          console.log(removediv+'dd'+imagename);
        var itemedel=1;
        for (var i = 0; i < preno; i++) {
            if (fi.files.item(i).name === imagename && itemedel==1) {
              itemedel=0;
            }
            else
            {
                dT.items.add(fi.files[i]);
            }
            
        }
        fileToUpload.files = dT.files;
        $('#'+removediv).remove();
      } 
      function removeimage1(evt){
        var fi = document.getElementById('fileToUpload1');
          var preno=fi.files.length;
          removediv=$(evt).data("id");
          imagename=$(evt).val();
          const dT = new DataTransfer();
          console.log(removediv+'dd'+imagename);
        var itemedel=1;
        for (var i = 0; i < preno; i++) {
            if (fi.files.item(i).name === imagename && itemedel==1) {
              itemedel=0;
            }
            else
            {
                dT.items.add(fi.files[i]);
            }
            
        }
        fileToUpload1.files = dT.files;
        $('#'+removediv).remove();
      } 
      // color image
      var loadFile1 = function(event) {
        var fi = document.getElementById('fileToUpload1');
        var length=fi.files.length;
        // VALIDATE OR CHECK IF ANY FILE IS SELECTED.
        if (fi.files.length > 0) {
          // THE TOTAL FILE COUNT.
          var dtime=$.now();
          for (var i = 0; i <= length ; i++) {
            //element.innerHTML(para1);
            $('#dropContainer1').append(`<div id='id${dtime}${i}' class='text-center' style='display:inline-block;width:110px;height:180px;margin:5px;margin-top:-80px;'><div style='padding:5px;border:solid 2px;'><img class='loading' width='70px' height='70px' src='${URL.createObjectURL(fi.files[i])}'> </div><button data-id='id${dtime}${i}' value='${fi.files.item(i).name}' class=' ridrop' onclick='removeimage(this)' type='button'>Remove</button></div>`);
            
            $('.ridrop').addClass("btn btn-block btn-danger");
            
            
            }
           
        }
      };
      dropContainer1.ondragover = dropContainer1.ondragenter = function(evt) {
          evt.preventDefault();
          $('#dropContainer1').css('border','dotted green');
        };
        $('#browsfile1').click(function(){
            $('#fileToUpload1').trigger('click');
        });
        function dragEnd1()
        {
            $('#dropContainer1').css('border','solid 1px');
        }
         
        dropContainer1.ondrop = function(evt) {
          // pretty simple -- but not for IE :(
          $('#dropContainer1').css('border','solid 1px');
          var fi = document.getElementById('fileToUpload1');
          var preno=fi.files.length;
          var newno=evt.dataTransfer.files.length;
          const dT = new DataTransfer();
          
          for (let i = 0; i < preno; i++) {
            
            dT.items.add(fi.files[i]); // here you exclude the file. thus removing it.
          }
          console.log(evt.dataTransfer.files[0])
          for(var x=0;x<newno;x++)
          {
            
            dT.items.add(evt.dataTransfer.files[x]) ;
          }
          fileToUpload1.files = dT.files;
          //fileToUpload.files = evt.dataTransfer.files;
          
           evt.stopPropagation();
          evt.preventDefault();
          
          //var fi = document.getElementById('fileToUpload');
        // VALIDATE OR CHECK IF ANY FILE IS SELECTED.
        if (newno > 0) { var dtime=$.now();
          // THE TOTAL FILE COUNT.
          //document.getElementById('new').innerHTML = 'Total Files: <b>' + fi.files.length + '</b></br >';
          // RUN A LOOP TO CHECK EACH SELECTED FILE.
          for (var i = preno; i <= fi.files.length ; i++) {
            var fname = fi.files.item(i).name;      // THE NAME OF THE FILE.
            var fsize = fi.files.item(i).size;      // THE SIZE OF THE FILE.
            
            //element.innerHTML(para1);
            $('#dropContainer1').append(`<div id='id${dtime}${i}' class='text-center' style='display:inline-block;width:110px;height:180px;margin:5px;margin-top:-80px;'><div style='padding:5px;border:solid 2px;'><img class='loading' width='70px' height='70px' src='${URL.createObjectURL(fi.files[i])}'> </div><button data-id='id${dtime}${i}' value='${fi.files.item(i).name}' class=' ridrop' onclick='removeimage1(this)' type='button'>Remove</button></div>`);
            
            $('.ridrop').addClass("btn btn-block btn-danger");
          }
        }
        };
      function centerImage(center_code)
      {
        $.ajax({
          url:"centerData.php",
          type:"POST",
          dataType:"JSON",
          data:{center_code:center_code},
          success:function(result){
            console.log(result['center_name']);
            document.getElementById('center_image').src="center_photo/"+result['center_photo'];
          }
        });
      }
      

      
      
    </script>

  	</body>
</html>
