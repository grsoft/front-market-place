@extends('supper_admin/master')
@section('content')
      <!-- Right side column. Contains the navbar and content of the page -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            
            <small></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Banner</a></li>
            <li class="active">Form</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          @if (session('success'))
              <div class="alert alert-success">
                  {{ session('success') }}
              </div>
          @endif
          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Add Banner Image</h3>
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <form method="post" enctype='multipart/form-data' action="{{ URL('/supper_admin/banner/home-banner-save')}}">
              @csrf
              <div class="box-body">
                <div class="row">
                  <div class="col-md-9">
                    <div class="form-group">
                      <label for="">Title</label>
                      <input type="text" name="title" class="form-control" value="{{old('title')}}" required>
                      @error('title')
                      <label class="text-danger">{{ $message }}</label>
                      @enderror
                    </div>
                    
                    <div class="form-group">
                      <label for="">URL</label>
                      <input type="url" name="url" class="form-control" value="{{old('url')}}" required>
                    </div>
                    @error('url')
                    <label class="text-danger">{{ $message }}</label>
                    @enderror
                    <div class="form-group">
                      <label for="">Image</label>
                      <input type="file" name="a_image"  onchange="HomeBannerloadFileSingle(event)" multiple="false" class="form-control">
                      <img src="" id="hbimage" width="990px" height="400px">
                    </div>
                  </div>
                </div>
              </div><!-- /.box-body -->
              <div class="box-footer">
                
                <button type="submit" class="btn btn-success">Save</button>
              </div><!-- /.box-footer-->
            </form>
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
@endsection    
