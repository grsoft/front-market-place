@extends('supper_admin/master')
@section('content')
      <!-- Right side column. Contains the navbar and content of the page -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            
            <small></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Category</a></li>
            <li class="active">Form</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          @if (session('success'))
              <div class="alert alert-success">
                  {{ session('success') }}<a href="{{URL('supper_admin/category/list')}}" class="btn btn-warning">Category List</a>
              </div>
          @endif
          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Category Form</h3>
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <input type="hidden" id="dividval" value="0">
            <form method="post" enctype='multipart/form-data' >
              @csrf
              <div class="box-body">
                <div class="row">
                  <div class="col-md-6">
                    <div class="row" id="cat">
                      <div class="col-md-12" >
                        <div class="form-group">
                          <label for="">Product Category</label>
                          <select id="category_id" class=" category form-control" required>
                            <option value="">Select One</option>
                            @foreach($category as $row)
                            <option value="{{$row->id}}">{{$row->title}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>  
                      
                      
                    </div>
                  </div>
                  
                </div>
              </div><!-- /.box-body -->
              <div class="box-footer">
                <!-- <a href="{{URL('supper_admin/category/list')}}" class="btn btn-danger" title="Cancel">Cancel</a>
                <button type="submit" class="btn btn-success">Save</button> -->
              </div><!-- /.box-footer-->
            </form>
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
       <div class="modal fade" id="addscategory">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
              <h4 class="modal-title">Sub Category Form</h4>
            </div>
            <form method="post" action="{{ URL('/supper_admin/sub-category-save')}}">
              @csrf
            <div class="modal-body" id="">
              <input type="hidden" id="category_idhidden" name="category_id" class="form-control" >
                <input type="hidden" id="childecategory_idhidden" name="childe_category_id" class="form-control" >
                <input type="hidden" id="treecategory_idhidden" name="tree_category_id" class="form-control" >
                <table class="table" id="model-body-html">
                  <tr>
                    <td width="50%">
                    <div class="form-group">
                      <label for="">Title</label>
                      <input type="text" name="title[]" class="form-control" >
                    </div>
                    </td>
                    <td width="50%">
                    <div class="form-group">
                      <label for="">Form</label>
                      <?php
                      $formdata = DB::table('form_table')->get();//select("SELECT * FROM `form_table` ");
                      //$max= $datamrp[0]->maxmrp;
                      ?>
                      <select name="form[]" id="formname" class="form-control">
                        <option value="">Select Form</option>
                        <?php for($i=0;$i<count($formdata);$i++){ ?>
                          <option title="{{$formdata[$i]->name_for_product}}" value="{{$formdata[$i]->id}}">{{$formdata[$i]->form_name}}</option>
                        <?php } ?>
                      </select>
                      
                    </div>
                    </td>
                  </tr>
                </table>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-warning" onclick="modal_text_box()">Add New Box</button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Save</button>
            </div>
            </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
@endsection    


