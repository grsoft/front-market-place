@extends('supper_admin/master')
@section('content')
      <!-- Right side column. Contains the navbar and content of the page -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            
            <small></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Sub Category</a></li>
            <li class="active">View</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          @if (session('success'))
              <div class="alert alert-success">
                  {{ session('success') }}
              </div>
          @endif
          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">{{$title}} </h3>
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <form method="post" enctype='multipart/form-data' action="{{ URL('/supper_admin/category-save')}}">
              @csrf
              <div class="box-body ">
                
                <div class="row">
                  <div class="col-md-12 table-responsive">
                    <table id="example2" class="table ">
                      <thead>
                        <th>Category</th>
                        <th>Title</th>
                        <th>Form</th>
                        <th>Action</th>
                      </thead>
                      <tbody>
                        @foreach($data as $row)
                        <tr>
                          <td>{{$row->parrent}}</td>
                          <td>{{$row->category}}</td>
                          <?php
                            $formdata = DB::table('form_table')->where('id',$row->form_id)->first();//select("SELECT * FROM `form_table` ");
                            //$max= $datamrp[0]->maxmrp;
                            //echo "<pre>";print_r($formdata);exit;
                          ?>
                          <td>{{!empty($formdata) ? $formdata->form_name :""}}</td>
                          <td>
                            <a href="{{URL('supper_admin/category/view-sub')}}/{{$row->scid}}" class="btn btn-warning" title="View"><i class="fa fa-eye"></i></a>
                            <button type="button" data-id="{{$row->scid}}" data-title="{{$row->category}}" data-form="{{!empty($formdata) ? $formdata->id :0}}" class="editsub btn btn-success" title="Edit"><i class="fa fa-edit"></i></button>   
                            <a href="{{URL('supper_admin/category/del-sub')}}/{{$row->scid}}" onclick="return confirm('Are you sure to delete category')" class="btn btn-danger" title="Delete"><i class="fa fa-trash"></i></a> 
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div><!-- /.box-body -->
              <div class="box-footer">
                
              </div><!-- /.box-footer-->
            </form>
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <div class="modal fade" id="editcategory">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
              <h4 class="modal-title">Edit Category</h4>
            </div>
            <form method="post" action="{{ URL('/supper_admin/sub-category-update')}}">
              @csrf
            <div class="modal-body" id="">
              <input type="hidden" id="id" name="id" class="form-control" >
                <div class="form-group">
                  <label>Title</label>
                  <input type="text" id="title" name="title" class="form-control">
                </div>
                <div class="form-group">
                  <label>Set Form</label>
                  <select name="form_id" id="formname" class="form-control">
                    <option value="">Select Form</option>
                    <?php $formdata = DB::table('form_table')->get(); ?>
                    <?php for($i=0;$i<count($formdata);$i++){ ?>
                      <option title="{{$formdata[$i]->name_for_product}}" value="{{$formdata[$i]->id}}">{{$formdata[$i]->form_name}}</option>
                    <?php } ?>
                  </select>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
              
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Update</button>
            </div>
            </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
@endsection    
