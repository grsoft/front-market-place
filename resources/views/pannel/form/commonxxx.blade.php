@extends('supper_admin/master')
@section('content')
      <!-- Right side column. Contains the navbar and content of the page -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            
            <small></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Product</a></li>
            <li class="active">Form</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          @if (session('success'))
              <div class="alert alert-success">
                  {{ session('success') }}
              </div>
          @endif
          <div class="row">
            <div class="col-md-3">
              
              <div class="box box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">Createarea</h3>
                </div>
                <div class="box-body no-padding">
                  <ul class="nav nav-pills nav-stacked">
                    <li class="<?php if($page=='brand'){echo 'active';} ?> tab brand"><a class="form" data-link="brand"> Brand</a></li>
                    <li class="<?php if($page=='item'){echo 'active';} ?> tab item"><a class="form" data-link="item"> Item <span class="label label-primary pull-right">0</span></a></li>
                    <li class="<?php if($page=='imageform'){echo 'active';} ?> tab imageform"><a class="form" data-link="imageform">Image</a></li>
                    <li class="<?php if($page=='aboutform'){echo 'active';} ?> tab aboutform"><a class="form" data-link="aboutform">About Product</a></li>
                    <li class="<?php if($page=='colorform'){echo 'active';} ?> tab colorform"><a class="form" data-link="colorform">Color Variant <span class="label label-waring pull-right">65</span></a></li>
                    <li class="<?php if($page=='seoform'){echo 'active';} ?> tab seoform"><a class="form" data-link="seoform">SEO</a></li>
                  </ul>
                </div><!-- /.box-body -->
              </div><!-- /. box -->
              
            </div><!-- /.col -->
            
            <div class="col-md-9">
              <form class="formtag <?php if($page!='brand'){echo 'hidden';} ?>" id="brand" method="post" enctype='multipart/form-data' action="{{ URL('/supper_admin/process-brand')}}">
              @csrf 
                <div class="box box-primary">
                  <div class="box-header with-border">
                    <h3 class="box-title">Brand</h3>
                    
                  </div><!-- /.box-header -->
                  <div class="box-body no-padding">
                    
                    <div class="table-responsive mailbox-messages">
                      <input type="hidden" name="id" value="{{$id}}">
                      <table class="table table-hover table-striped">
                        <tr>
                          <td>Brand Name <span>*</span></td>
                          <td><input type="text" name="brand" class="form-control" required></td>
                        </tr>
                      </table>
                    </div>
                    
                  </div>
                  <div class="box-footer no-padding">
                    <div class="mailbox-controls">
                      <button type="submit" class="btn btn-info">Next</button>
                      
                    </div>
                  </div>
                </div>
              </form>
              <form class="formtag <?php if($page!='item'){echo 'hidden';} ?>" id="item" method="post" enctype='multipart/form-data' action="{{ URL('/supper_admin/process-brand')}}">
              @csrf
              
                <div class="box box-primary">
                  <div class="box-header with-border">
                    <h3 class="box-title">Item</h3>
                    
                  </div><!-- /.box-header -->
                  <div class="box-body no-padding">
                    
                    <div class="table-responsive mailbox-messages">
                      <input type="hidden" name="id" value="{{$id}}">
                      <table class="table table-hover table-striped">
                        <tbody>
                          <tr>
                            <td>Status</td>
                            <td>
                              <select class="form-control" name="status">
                                <option>Select Status</option>
                                <option value="1">Active</option>
                                <option value="0">Inctive</option>
                              </select>
                            </td>
                          </tr>
                          <tr>
                            <td>Product Title</td>
                            <td><input type="text" name="title" class="form-control"></td>
                          </tr>
                          <tr>
                            <td>Price (MRP)</td>
                            <td><input type="number" name="mrp" class="form-control"></td>
                          </tr>
                          <tr>
                            <td>Discount</td>
                            <td><input type="number" name="discount" class="form-control"> </td>
                          </tr>
                          <tr>
                            <td>Sell Price</td>
                            <td><input type="number" name="sell_price" class="form-control">  </td>
                          </tr>
                        </tbody>
                      </table><!-- /.table -->
                    </div><!-- /.mail-box-messages -->
                  </div><!-- /.box-body -->
                  <div class="box-footer no-padding">
                    <div class="mailbox-controls">
                      <a class="form btn btn-warning" data-link="brand"> Back </a>
                      <button class="btn btn-success" type="submit"> Next </button>
                    </div>
                  </div>
                </div><!-- /. box -->
              </form>
              <form class="formtag <?php if($page!='imageform'){echo 'hidden';} ?>" id="imageform" method="post" enctype='multipart/form-data' action="{{ URL('/supper_admin/process-brand')}}">
              @csrf 
              
                <div class="box box-primary">
                  <div class="box-header with-border">
                    <h3 class="box-title">Image</h3>
                    
                  </div><!-- /.box-header -->
                  <div class="box-body no-padding">
                    
                    <div class="table-responsive mailbox-messages">
                      <input type="hidden" name="id" value="{{$id}}">
                      <div class="row" style="padding: 15px;">
                        <div class="col-md-12">
                          <div class="form-group">
                            <label>Product Image</label>
                            <input type="file" name="photo" id="fileToUploadsingle" ondrop="loadFileSingle(event)"  onchange="loadFileSingle(event)" multiple="false"  class="form-control" >
                          </div>
                          <img id="image" width="100px" height="100px" src="">
                        </div>
                        <div class="col-md-12">
                          <label>Upload Thumbnale Image</label>
                          
                            <div style="display: none;"><input type="file" name="thumb[]" id="fileToUpload" ondrop="loadFile(event)"  onchange="loadFile(event)" multiple="true"  class="form-control" >
                            </div>

                          
                          <div id="dropContainer" onmouseout="dragEnd()" class="form-group"style="border:1px solid black;height:145px; padding: 2px;overflow: auto;">
                            <div style="text-align: center;"><br>
                              <a id="browsfile" class="btn btn-success">Browse File to Stystem</a><br>
                              <span style="text-align: center; font-size: 30px; color:skyblue; ">Drag & Drop Here</span>
                            </div>
                              
                          </div>
                
                        </div>
                      </div>
                    </div>
                    
                  </div>
                  <div class="box-footer no-padding">
                    <div class="mailbox-controls">
                      <a class="form btn btn-warning" data-link="item"> Back </a>
                      <a class="form btn btn-success" data-link="aboutform"> Next </a>
                    </div>
                  </div>
                </div>
              </form>
              <form class="formtag <?php if($page!='aboutform'){echo 'hidden';} ?>" id="aboutform" method="post" enctype='multipart/form-data' action="{{ URL('/supper_admin/process-brand')}}">
              @csrf
              
                <div class="box box-primary">
                  <div class="box-header with-border">
                    <h3 class="box-title">About Product</h3>
                    
                  </div><!-- /.box-header -->
                  <div class="box-body no-padding">
                    <input type="hidden" name="id" value="{{$id}}">
                    <div class=" mailbox-messages">
                      <div class="row" style="padding: 15px;">
                        <div class="col-md-12">
                          <div class="form-group">
                            <label for="">Product Details</label>
                            <textarea id="text" rows="12" class="form-control"></textarea>
                          </div>
                        </div>
                  
                      </div>
                    </div>
                    
                  </div>
                  <div class="box-footer no-padding">
                    <div class="mailbox-controls">
                      <a class="form btn btn-warning" data-link="imageform"> Back </a>
                      <a class="form btn btn-success" data-link="colorform"> Next </a>
                    </div>
                  </div>
                </div>
              </form>
              <form class="formtag <?php if($page!='colorform'){echo 'hidden';} ?>" id="colorform" method="post" enctype='multipart/form-data' action="{{ URL('/supper_admin/process-brand')}}">
              @csrf
              
                <div class="box box-primary">
                  <div class="box-header with-border">
                    <h3 class="box-title">Color Varients</h3>
                    
                  </div><!-- /.box-header -->
                  <div class="box-body no-padding">
                    <input type="hidden" name="id" value="{{$id}}">
                    <div class="table-responsive mailbox-messages">
                      <div class="row" style="padding: 15px;">
                        <div class="col-md-12">
                          <div class="form-group">
                            <label>Color Image</label>
                            
                          </div>
                          
                        </div>
                        <div class="col-md-12">
                          
                          
                            <div style="display: none;"><input type="file" name="colorphoto[]" id="fileToUpload1" ondrop="loadFile1(event)"  onchange="loadFile1(event)" multiple="true"  class="form-control" >
                            </div>

                          
                          <div id="dropContainer1"  onmouseout="dragEnd1()" class="form-group"style="border:1px solid black;height:145px; padding: 2px;overflow: auto;">
                            <div style="text-align: center;"><br>
                              <a id="browsfile1" class="btn btn-success">Browse File to Stystem</a><br>
                              <span style="text-align: center; font-size: 30px; color:skyblue; ">Drag & Drop Here</span>
                            </div>
                              
                          </div>
                
                        </div>
                      </div>
                    </div>
                    
                  </div>
                  <div class="box-footer no-padding">
                    <div class="mailbox-controls">
                      <a class="form btn btn-warning" data-link="aboutform"> Back </a>
                      <a class="form btn btn-success" data-link="seoform"> Next </a>
                    </div>
                  </div>
                </div>
              </form>
              <form class="formtag <?php if($page!='seoform'){echo 'hidden';} ?>" id="seoform" method="post" enctype='multipart/form-data' action="{{ URL('/supper_admin/process-brand')}}">
              @csrf
              
                <div class="box box-primary">
                  <div class="box-header with-border">
                    <h3 class="box-title">SEO</h3>
                    
                  </div><!-- /.box-header -->
                  <div class="box-body no-padding">
                    <input type="hidden" name="id" value="{{$id}}">
                    <div class="table-responsive mailbox-messages">
                      <table class="table table-hover table-striped">
                        <tbody>
                          
                          <tr>
                            <td>Meta Title</td>
                            <td><input type="text" name="title" class="form-control"></td>
                          </tr>
                          <tr>
                            <td>Keywords</td>
                            <td><input type="number" name="price" class="form-control"></td>
                          </tr>
                          
                        </tbody>
                      </table><!-- /.table -->
                    </div><!-- /.mail-box-messages -->
                  </div><!-- /.box-body -->
                  <div class="box-footer no-padding">
                    <div class="mailbox-controls">
                      <a class="form btn btn-warning" data-link="colorform"> Back </a>
                      <button type="submit" class="btn btn-default ">Submit</button>
                    </div>
                  </div>
                </div><!-- /. box -->
              </form>
            </div><!-- /.col -->
            </form>
          </div><!-- /.row -->
        </section>
        
      </div><!-- /.content-wrapper -->
      <script src="{{URL::asset('admins/plugins/iCheck/icheck.min.js') }}" type="text/javascript"></script>
@endsection    
