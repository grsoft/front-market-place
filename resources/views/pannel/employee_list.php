<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Admin | Employee List</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />    
    <!-- FontAwesome 4.3.0 -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons 2.0.0 -->
    <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />  
    <link href="plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />  
    <!-- Theme style -->
    <link href="dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins 
         folder instead of downloading all of them to reduce the load. -->
    <link href="dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
    <!-- iCheck -->
    <link href="plugins/iCheck/flat/blue.css" rel="stylesheet" type="text/css" />
    <!-- Morris chart -->
    <link href="plugins/morris/morris.css" rel="stylesheet" type="text/css" />
    <!-- jvectormap -->
    <link href="plugins/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
    <!-- Date Picker -->
    <link href="plugins/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />
    <!-- Daterange picker -->
    <link href="plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap wysihtml5 - text editor -->
    <link href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="skin-blue">
    <div class="wrapper">
      
      <?php include "header.php"; 
			include "sidebar.php";?>
      <!-- Left side column. contains the logo and sidebar -->
     

      <!-- Right side column. Contains the navbar and content of the page -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            
            <small></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Employee</a></li>
            <li class="active">Employee List</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <?php 
            if(isset($_POST['add']))
            {
                $post_date=date('Y-m-d');
                $sector_name=$_POST['sector_name'];
                $sql="INSERT INTO sector (sector_name,post_date) VALUES ('$sector_name','$post_date')";
                $con->connect();
                $excute=$con->insert($sql);
                if($excute)
                {
                    echo "<script>alert('Sector add succesfully.');window.location.assign('sector_add.php');</script>";
                    
                }
            }
            if (isset($_POST['del_sector'])) 
            {
                $id=$_POST['del_sector'];
                $sql="DELETE from employee_details WHERE id='$id'";
                $con->connect();
                $excute=$con->delete($sql);
                if($excute)
                {
                    echo "<script>alert('Employee delete succesfully.');</script>";
                    echo "<script>window.location.assign('employee_list.php');</script>";
                }
            }
            ?>
         
			
            <div class="row">
            <!-- left column -->
                <div class="col-md-12">
                  <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header">
                          <h3 class="box-title">Employee List</h3>
                        </div><!-- /.box-header -->
                        <!-- form start -->
                        <form role="form" method="post">
                          <table id="example1" class="table">
                            <thead>
                                <th>S No.</th>
                                <th>Sector Name</th>
								<th>Designation Name</th>
								<th>Employee ID</th>
								<th>Date of Joining</th>
								<th>Posting Place</th>
								<th>Employee Name</th>
								<th>Mobile Number</th>
								<th>Email</th>
                                <th>Action</th>
                            </thead>
                            <tbody>
                            <?php
                            $sql="SELECT sector.sector_name as sector_names, designation.designation_name as designation_names, employee_details.* FROM employee_details join sector on 
							sector.id=employee_details.sector_name join designation on designation.id=employee_details.designation_name";
                            $con->connect();
                            $fetchData=$con->query($sql);
                            if(!empty($fetchData))
                            {
                                for ($i=0; $i < count($fetchData); $i++) 
                                { 
                                ?>    
                                <tr>
                                  <td><?php echo $i+1; ?></td>
                                  <td><?php echo $fetchData[$i]['sector_names']; ?></td>
								  <td><?php echo $fetchData[$i]['designation_names']; ?></td>
								  <td><?php echo $fetchData[$i]['employee_id']; ?></td>
								  <td><?php echo $fetchData[$i]['date_of_joining']; ?></td>
								  <td><?php echo $fetchData[$i]['posting_place_name']; ?></td>
								  <td><?php echo $fetchData[$i]['employee_name']; ?></td>
								  <td><?php echo $fetchData[$i]['mobile_number']; ?></td>
								  <td><?php echo $fetchData[$i]['email_id']; ?></td>
                                  <td>
                                      <a href="employee_edit.php?id=<?php echo $fetchData[$i]['id']; ?>" class="btn btn-warning"><i class="fa fa-edit"></i></a>
                                      <button name="del_sector" value="<?php echo $fetchData[$i]['id']; ?>" onclick="return confirm('Are you sure to delete ?')" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                                  </td>
                                </tr>
                                <?php
                                }
                            }
                            ?>
                            </tbody>
                          </table>
                        </form>
                    </br>
                    </div>
                </div><!-- /.box -->
            </div>
		</section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <?php include "footer.php"; ?>
    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.3 -->
    <script src="plugins/jQuery/jQuery-2.1.3.min.js"></script>
    
    <!-- Bootstrap 3.3.2 JS -->
    <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>    
    <!-- Morris.js charts -->
    <script src="plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
    <script src="plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
    
    <script src="plugins/morris/morris.min.js" type="text/javascript"></script>
    <!-- Sparkline -->
    <script src="plugins/sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
    <!-- jvectormap -->
    <script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js" type="text/javascript"></script>
    <script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js" type="text/javascript"></script>
    <!-- jQuery Knob Chart -->
    <script src="plugins/knob/jquery.knob.js" type="text/javascript"></script>
    <!-- daterangepicker -->
    <script src="plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
    <!-- datepicker -->
    <script src="plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
    <!-- iCheck -->
    <script src="plugins/iCheck/icheck.min.js" type="text/javascript"></script>
    <!-- Slimscroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src='plugins/fastclick/fastclick.min.js'></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js" type="text/javascript"></script>

    
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js" type="text/javascript"></script>
    <script type="text/javascript">
      $(function () {
        $("#example1").dataTable();
        $('#example2').dataTable({
          "bPaginate": true,
          "bLengthChange": false,
          "bFilter": false,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": false
        });
      });
    </script>
  </body>
</html>