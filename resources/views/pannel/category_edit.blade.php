@extends('supper_admin/master')
@section('content')
      <!-- Right side column. Contains the navbar and content of the page -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            
            <small></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Category</a></li>
            <li class="active">Form</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          @if (session('success'))
              <div class="alert alert-success">
                  {{ session('success') }}<a href="{{URL('supper_admin/category/list')}}" class="btn btn-warning">Category List</a>
              </div>
          @endif
          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Category Form</h3>
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <form method="post" enctype='multipart/form-data' action="{{ URL('/supper_admin/category-edit')}}">
              @csrf
              <input type="hidden" name="id" value="{{$data->id}}">
              <div class="box-body">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="">Status</label>
                      <select name="status" class="form-control" required>
                        <option value="">Select One</option>
                        <option <?php if($data->status==0) echo 'selected'; ?> value="0">Draft</option>
                        <option <?php if($data->status==1) echo 'selected'; ?> value="1">Published</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="">Category Name</label>
                      <input type="text" name="title" class="form-control" value="{{$data->title}}" required>
                    </div>
                    <div class="form-group">
                      <label for="">Image</label><br>
                      <img width="100px" height="100px" src="{{asset('/')}}category_image/{{$data->image}}">
                    </div>
                    <div class="form-group">
                      <label for="">New Image</label>
                      <input type="hidden" name="category_image" value="{{$data->image}}">
                      <input type="file" name="category_image1" class="form-control">
                    </div>
                  </div>
                </div>
              </div><!-- /.box-body -->
              <div class="box-footer">
                <a href="{{URL('supper_admin/category/list')}}" class="btn btn-danger" title="Cancel">Cancel</a>
                <button type="submit" class="btn btn-success">Update</button>
              </div><!-- /.box-footer-->
            </form>
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
@endsection    
