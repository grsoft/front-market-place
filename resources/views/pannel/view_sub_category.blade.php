@extends('supper_admin/master')
@section('content')
      <!-- Right side column. Contains the navbar and content of the page -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            
            <small></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Sub Category</a></li>
            <li class="active">List</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          @if (session('success'))
              <div class="alert alert-success">
                  <h3>{{ session('success') }}</h3>
              </div>
          @endif
          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">{{$title}} - {{$data[0]->title}}</h3>

              <div class="box-tools pull-right">
                <!-- <div class="btn-group">
                      <button type="button" class="btn btn-info btn-flat">Action</button>
                      <button type="button" class="btn btn-info btn-flat dropdown-toggle" data-toggle="dropdown">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                      </button>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="{{URL('supper_admin/category/edit-sub')}}/{{$data[0]->id}}">Edit</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Delete</a></li>
                      </ul>
                    </div> -->
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>

              </div>
            </div>
            <form method="post" enctype='multipart/form-data' action="{{ URL('/supper_admin/category-save')}}">
              @csrf
              <div class="box-body ">
                
                <div class="row table-responsive">
                  <div class="col-md-6 ">
                    <table class="table ">
                      
                      <tbody>
                        <tr>
                          <td>Title</td>
                          <td>{{$data[0]->title}}</td>
                          
                        </tr>
                        <tr>
                          <td>Created Date/Time</td>
                          <td>{{$data[0]->created_at}}</td>
                        </tr>
                        <tr>
                          <td>Created By</td>
                          <td>{{$data[0]->created_by}}</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <div class="col-md-6 ">
                        
                        @foreach($subdata as $row)
                        
                        <button class="btn btn-block text-center">
                        {{$row->title}}   
                        </button>
                        <p class="text-center"><img width="50px" height="50px" src="{{URL::asset('admins/dist/img/white-down-arrow-icon.png')}}"></p>
                        @endforeach
                        
                        <button class="btn btn-success btn-block text-center">{{$data[0]->title}}</button> 
                     
                    
                  </div>
                </div>
              </div><!-- /.box-body -->
              <div class="box-footer">
                
              </div><!-- /.box-footer-->
            </form>
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
@endsection    
