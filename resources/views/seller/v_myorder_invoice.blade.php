<!DOCTYPE html>
<html>
<head>
    <title>Invoice</title>
    <style type="text/css">
    	.table{
    		width: 100%;
    		border-collapse: collapse;
    	}
    	.table th{
    		vertical-align: top;
    		padding: 5px;
    		border: 0.5px solid black;
    	}
    	.table tr{
    		border: 0.5px solid black; 
    	}
    	.table td{
    		padding: 5px;
    		vertical-align: top;
    		border: 0.5px solid black;
    	}
    	td{
    		vertical-align: top;
    	}
    </style>
</head>
<body>
    <?php
    $o_data=$odata[0];
    $ship_data=$ship_data[0];
    //$cdata=$cdata[0];
     //echo "<pre>";print_r($data);print_r($ship_data);print_r($cdata); ?>
    <?php $total=0; ?>
    <h2 style="text-align: center; border:solid 1px;">INVOICE</h2>
    <table class="" style="width: 100%">
    	<tr>
    		<td >
    			<h4>Order Number<br> {{$o_data->unique_no}}</h4>
    			<h4>Order Date<br> {{$o_data->order_date}}</h4>
    		</td>
    		<td>
    			<h4>Shiping Address</h4>
                {{$ship_data->first_name_s}} {{$ship_data->last_name_s}}
                <br>{{$ship_data->address_s}}<br>
                City: {{$ship_data->city_s}}<br>
                State: {{$ship_data->state_s}}<br>
                Pin Code: {{$ship_data->postal_code_s}}<br>
                Country: {{$ship_data->country_s}}<br>
                Mob. No {{$ship_data->mobile_no_s}}<br>
                {{$ship_data->email_address_s}}
    		</td>
    		<td>
    			<h4>Billing Address</h4>
                {{$o_data->first_name}} {{$o_data->last_name}}
                <br>{{$o_data->address}}<br>
                City: {{$o_data->city}}<br>
                State: {{$o_data->state}}<br>
                Pin Code: {{$o_data->postal_code}}<br>
                Country: {{$o_data->country}}<br>
                Mob. No {{$o_data->mobile_no}}<br>
                {{$o_data->email_address}}
    		</td>
    	</tr>
    </table><br><br>

    <table class="table">
        
            <tr>
                <th >Image</th>
                <th >Product Name</th>
                <th >Price</th>
                <th >Quantity</th>
                <th >Grandtotal</th>
                
            </tr>
        
        
            
            @foreach ($cdata as $data)
            <tr >
                <td >
                    <img src="{{ public_path('product_image/'.$data->image) }}" width="70px" alt="">
                    
                </td>
                <td >
                    <p>{{ $data->title }}</p>
                    
                </td>
                <td>
                    <p>{{ number_format($data->amount, 2) }}</p>
                </td>
                <td >
                    <p>{{ $data->quantity}}</p>                                                   
                </td>
                <td>
                    <p>{{ number_format($data->quantity * $data->amount, 2) }}</p>
                </td>
                
                @php $total = $total + ($data->quantity * $data->amount) @endphp
            </tr>
            @endforeach
        
        
            <tr>
                <td rowspan="1" colspan="1"></td>
                <td colspan="3">Total products (tax incl.)</td>
                <td colspan="1">{{$total}}</td>
            </tr>
            <tr>
            	<td rowspan="1" colspan="1"></td>
                <td colspan="3"><strong>Total</strong></td>
                <td colspan="1"><strong>{{$total}}</strong></td>
            </tr>
           
    </table>
    
</body>
</html>