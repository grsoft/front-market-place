@extends('seller/master')
@section('content')
      <!-- Right side column. Contains the navbar and content of the page -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            
            <small></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Scanner</a></li>
            <li class="active">QR Code</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          @if (session('success'))
              <div class="alert alert-success">
                  {{ session('success') }}
              </div>
          @endif
          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Scanner</h3>
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <form method="post" enctype='multipart/form-data' action="{{ URL('/supper_admin/category-save')}}">
              @csrf
              <div class="box-body table-responsive">
                <script src="{{ URL::asset('html5-qrcode.min.js') }}"></script>
                  <style>
                    .result{
                      background-color: green;
                      color:#fff;
                      padding:20px;
                    }
                    .row{
                      display:flex;
                    }
                  </style>


                  <div class="row">
                    <div class="col-md-12">
                      <div style="width:300px;" id="reader"></div>
                    </div>
                    <!-- <div class="col" style="padding:30px;">
                      <h4>SCAN RESULT</h4>
                      <div id="result">Result Here</div>
                    </div> -->
                  </div>


                  <script type="text/javascript">
                  function onScanSuccess(qrCodeMessage) {
                      //document.getElementById('result').innerHTML = '<span class="result">'+qrCodeMessage+'</span>';
                  }

                  function onScanError(errorMessage) {
                    //handle scan error
                  }

                  var html5QrcodeScanner = new Html5QrcodeScanner(
                      "reader", { fps: 10, qrbox: 250 });
                  html5QrcodeScanner.render(onScanSuccess, onScanError);

                  </script>
              </div><!-- /.box-body -->
              <div class="box-footer">
                
              </div><!-- /.box-footer-->
            </form>
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
@endsection    
