 <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="{{ URL::asset('admins/dist/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
              <p>{{session('user_name')}}</p>

              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MENU</li>
            <li class="active"><a href="{{ URL('seller/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            
            <li class="treeview">
              <a href="#">
                <i class="fa fa-edit"></i> <span>Product</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{ URL('seller/product/form') }}"><i class="fa fa-circle-o"></i> Add Product</a></li>
                <li><a href="{{ URL('seller/product/list') }}"><i class="fa fa-circle-o"></i> Product List</a></li>
                
              </ul>
            </li> 
            <li><a href="{{ URL('seller/qr-scaner') }}"><i class="fa fa-circle-o"></i> QR Code</a></li>
            <li><a href="{{ URL('seller/myorder') }}"><i class="fa fa-circle-o"></i> My Order</a></li>
            <li><a href="{{ URL('seller/delivered-order') }}"><i class="fa fa-circle-o"></i> Delivered Order</a></li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>