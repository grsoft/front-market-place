
              
<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Item</h3>
    
  </div><!-- /.box-header -->
  <div class="box-body no-padding">
    
    <div class="table-responsive mailbox-messages">
    <input type="hidden" name="json_data[]"  value="display_type">
    <input type="hidden" name="json_data[]"  value="screen_size">
    <input type="hidden" name="json_data[]"  value="operating_system">
    <input type="hidden" name="json_data[]"  value="processor_type">
    <input type="hidden" name="json_data[]"  value="ram_size">
    <input type="hidden" name="json_data[]"  value="hdd">
    <input type="hidden" name="json_data[]"  value="color">
      <table class="table ">
        <tbody>
          
          <tr>
            <td>Display Type</td>
            <td><div class="form-group">
                    <label style="padding-right:10px;">
                      <input type="radio" name="display_type" value="LED" class="minimal" /> LED
                    </label>
                    <label style="padding-right:10px;">
                      <input type="radio" name="display_type" value="LCD" class="minimal"/> LCD
                    </label>
                    
                  </div>
            </td>
          </tr>
          <tr>
            <td>Screen Size</td>
            <td><input type="text" name="screen_size" class="form-control text-large" value="{{old('screen_size')}}" required></td>
          </tr>
          <tr>
            <td>Operating System</td>
            <td><input type="text"  name="operating_system"  class="form-control text-sm" value="{{old('operating_system')}}" required></td>
          </tr>
          <tr>
            <td>Processor Type</td>
            <td><div class="form-group">
                    <label style="padding-right:30px;">
                      <input type="radio" name="processor_type" value='i3' class="minimal" /> i3
                    </label>
                    <label style="padding-right:30px;">
                      <input type="radio" name="processor_type" value='i5' class="minimal"/> i5
                    </label>
                    <label style="padding-right:30px;">
                      <input type="radio" name="processor_type" value='i7' class="minimal"/> i7
                    </label>
                    <label style="padding-right:30px;">
                      <input type="radio" name="processor_type" value='Other' class="minimal"/> Other
                    </label>
                    
                  </div>
                </td>
          </tr>
          <tr>
            <td>RAM Size</td>
            <td><input type="text"  name="ram_size" class="form-control text-large" value="{{old('ram_size')}}" required>  </td>
          </tr>
          <tr>
            <td>Hard Disc Drive (HDD)</td>
            <td><input type="text"  name="hdd" class="form-control text-large" value="{{old('ram_size')}}" required>  </td>
          </tr>
          <tr>
            <td>Color </td>
            <td><input type="text"  name="color" class="form-control text-sm" value="{{old('color')}}" required>  </td>
          </tr>
        </tbody>
      </table><!-- /.table -->
    </div><!-- /.mail-box-messages -->
  </div><!-- /.box-body -->
  
</div><!-- /. box -->
              
