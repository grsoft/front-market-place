@extends('seller/master')
@section('content')
      <style type="text/css">
        .catbox{
          font-size: 20px;
        }
      </style>
      <!-- Right side column. Contains the navbar and content of the page -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            
            <small></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Product</a></li>
            <li class="active">Form</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          @if (session('success'))
              <div class="alert alert-success">
                  {{ session('success') }}
              </div>
          @endif
          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Product</h3>
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <form method="post" enctype='multipart/form-data' action="{{ URL('/seller/product-step')}}">
              @csrf
              
              <div class="box-body">
                <input type="hidden"id="dividval">
                <div class="row" id="cat" >
                  <div class="col-md-3">
                    <div class="form-group">
                      <label for="">Product Category</label>
                      <select multiple="true" size="15"  name="category" class=" fcategory form-control catbox" required>
                        <option value="">Select One</option>
                        @foreach($category as $row)
                        <option value="{{$row->id}}">{{$row->title}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>
                <p style="margin-top: 30px; display: none;" id="gobutton">
                    <button  type="submit" class="btn-lg btn-success">Next</button>
                </p>
              </div>
            </form>
            
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <script src="{{URL::asset('admins/plugins/iCheck/icheck.min.js') }}" type="text/javascript"></script>
@endsection    
