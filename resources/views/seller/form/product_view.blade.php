@extends('seller/master')
@section('content')
      <style type="text/css">
        @media screen and (min-width: 768px) {
          .text-sm {
            width:150px;
          }
          .text-large {
            width:450px;
          }
        }
        ul.tul {
            display: inline-flex;
            list-style: decimal;
        }
        .tul li{
          margin-right: 20px;
        }
      </style>
      <!-- Right side column. Contains the navbar and content of the page -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            
            <small></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Product</a></li>
            <li class="active">View</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          @if (session('success'))
              <div class="alert alert-success">
                  {{ session('success') }}
              </div>
          @endif
          <div class="row">
            
            <form class="formtag " id="brand" method="post" enctype='multipart/form-data' action="{{ URL('/supper_admin/product-update')}}">
              @csrf 
              
            <div class="col-md-12">
              
                <div class="box box-primary">
                  <div class="box-header with-border">
                    <h3 class="box-title">Brand</h3>
                    
                  </div><!-- /.box-header -->
                  <div class="box-body no-padding">
                    
                    <div class="table-responsive mailbox-messages">
                      
                      <table class="table table-hover table-striped">
                        <tr>
                          <td width="20%">Brand Name <span>*</span></td>
                          <td>{{$data[0]->brand}}</td>
                        </tr>
                      </table>
                    </div>
                    
                  </div>
                  
                </div>
              
              
              
                <div class="box box-primary">
                  <div class="box-header with-border">
                    <h3 class="box-title">Item</h3>
                    
                  </div><!-- /.box-header -->
                  <div class="box-body no-padding">
                    
                    <div class="table-responsive mailbox-messages">
                      
                      <table class="table table-hover table-striped">
                        <tbody>
                          <tr>
                            <td width="20%">Status</td>
                            <td>
                              @if ($data[0]->status =="1")
                                {{"Active"}}
                                @else
                                {{"Inctive"}}
                                @endif
                              
                            </td>
                          </tr>
                          <tr>
                            <td>Product Title</td>
                            <td>{{$data[0]->title}}</td>
                          </tr>
                          <tr>
                            <td>Price (MRP)</td>
                            <td>{{$data[0]->mrp}}</td>
                          </tr>
                          <tr>
                            <td>Discount %</td>
                            <td>{{$data[0]->discount}}
                            </td>
                          </tr>
                          <tr>
                            <td>Sell Price</td>
                            <td>{{$data[0]->sell_price}} </td>
                          </tr>
                        </tbody>
                      </table><!-- /.table -->
                    </div><!-- /.mail-box-messages -->
                  </div><!-- /.box-body -->
                  
                </div><!-- /. box -->
              
                <div class="box box-primary">
                  <div class="box-header with-border">
                    <h3 class="box-title">Image</h3>
                    
                  </div><!-- /.box-header -->
                  <div class="box-body no-padding">
                    
                    <div class="mailbox-messages">
                      
                      <div class="row" style="padding: 15px;">
                        <div class="col-md-12">
                          <div class="form-group">
                            <label>Product Image</label>
                            
                          </div>
                          <img id="image" width="100px" height="100px" src="{{URL::asset('product_image')}}/{{$data[0]->image}}">
                        </div>
                        <div class="col-md-12"><br>
                          <label>Thumbnail Image</label><br>
                          <div class="form-group"style="border:1px solid black;height:145px; padding: 2px;overflow: auto;">
                            <ul class="tul">
                              @foreach($thumb as $th)
                              <li style="padding: 10px;">
                              <img id="image" width="100px" height="100px" src="{{URL::asset('thumb_image')}}/{{$th->image}}">
                              
                              </li>
                              @endforeach
                            </ul>
                          </div>
                        </div>
                        
                      </div>
                    </div>
                    
                  </div>
                  
                </div>
                <div class="box box-primary">
                  <div class="box-header with-border">
                    <h3 class="box-title">About Product</h3>
                    
                  </div><!-- /.box-header -->
                  <div class="box-body no-padding">
                    
                    <div class=" mailbox-messages">
                      <div class="row" style="padding: 15px;">
                        <div class="col-md-12">
                          <div class="form-group">
                            <label for="">Product Details</label>
                            <?= $data[0]->about ?>
                          </div>
                        </div>
                  
                      </div>
                    </div>
                    
                  </div>
                  
                </div>
                <div class="box box-primary">
                  <div class="box-header with-border">
                    <h3 class="box-title">Color Varients</h3>
                    
                  </div><!-- /.box-header -->
                  <div class="box-body no-padding">
                    
                    <div class="mailbox-messages">
                      <div class="row" style="padding: 15px;">
                        <div class="col-md-12">
                          <div class="form-group"style="border:1px solid black;height:145px; padding: 2px;overflow: auto;">
                            <ul class="tul">
                              @foreach($color_variente as $th)
                              <li style="padding: 10px;">
                              <img id="image" width="100px" height="100px" src="{{URL::asset('color_image')}}/{{$th->image}}">
                              
                              </li>
                              @endforeach
                            </ul>
                          </div>
                        </div>
                        
                        
                      </div>
                    </div>
                    
                  </div>
                  
                </div>
                <div class="box box-primary">
                  <div class="box-header with-border">
                    <h3 class="box-title">SEO</h3>
                    
                  </div><!-- /.box-header -->
                  <div class="box-body no-padding">
                    
                    <div class="table-responsive mailbox-messages">
                      <table class="table table-hover table-striped">
                        <tbody>
                          
                          <tr>
                            <td width="20%">Meta Title</td>
                            <td>{{$data[0]->meta_title}}</td>
                          </tr>
                          <tr>
                            <td>Keywords</td>
                            <td>{{$data[0]->keywords}}</td>
                          </tr>
                          
                        </tbody>
                      </table><!-- /.table -->
                    </div><!-- /.mail-box-messages -->
                  </div><!-- /.box-body -->
                  <div class="box-footer no-padding">
                    <div class="mailbox-controls">
                      <a href="{{URL('seller/product/edit')}}/{{$data[0]->id}}" class="btn btn-warning" title="Update & Change"><i class="fa fa-edit"></i> Update & Change</a>
                    </div>
                  </div>
                </div><!-- /. box -->
              
            </div><!-- /.col -->
            </form>
          </div><!-- /.row -->
        </section>
        
      </div><!-- /.content-wrapper -->
      <script src="{{URL::asset('admins/plugins/iCheck/icheck.min.js') }}" type="text/javascript"></script>
      
@endsection    
