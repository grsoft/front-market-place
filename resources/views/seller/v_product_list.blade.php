@extends('seller/master')
@section('content')
      <!-- Right side column. Contains the navbar and content of the page -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            
            <small></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Product</a></li>
            <li class="active">List</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          @if (session('success'))
              <div class="alert alert-success">
                  {{ session('success') }}
              </div>
          @endif
          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">{{$title}}</h3>
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <form method="post" enctype='multipart/form-data' action="{{ URL('/supper_admin/category-save')}}">
              @csrf
              <div class="box-body table-responsive">
                
                <div class="row">
                  <div class="col-md-12">
                    <table id="example2" class="table">
                      <thead>
                        <th>Image</th>
                        <th>Brand</th>
                        <th>Title</th>
                        <th>Mrp</th>
                        <th>Disc. %</th>
                        <th>Disc. Amt</th>
                        <th>Sell Price</th>
                        <th>Status</th>
                        <th>Action</th>
                      </thead>
                      <tbody>
                        @foreach($data as $row)
                        <tr>
                          <td><img src="{{URL::asset('/product_image')}}/{{$row->image}}" width="50px" height="50px"></td>
                          <td>{{$row->brand}}</td>
                          <td>{{$row->title}}</td>
                          <td>{{$row->mrp}}</td>
                          <td>{{$row->discount}}</td>
                          <td>{{$row->discount_amt}}</td>
                          <td>{{$row->sell_price}}</td>
                          <td>@if($row->verify_status==0)<p class="text-danger"><b>Pending</b></p>@else <p class="text-success"><b>Approved</b></p> @endif</td>
                          <td>
                            <a href="{{URL('seller/product/edit')}}/{{$row->id}}" class="btn btn-warning" title="Edit"><i class="fa fa-edit"></i></a>
                            <a href="{{URL('seller/product/view')}}/{{$row->id}}" class="btn btn-success" title="Edit"><i class="fa fa-eye"></i></a>
                            <a onclick="return confirm('Are you sure to delete category')" href="{{URL('seller/product/delete')}}/{{$row->id}}" class="btn btn-danger" title="Delete"><i class="fa fa-trash"></i></a>
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div><!-- /.box-body -->
              <div class="box-footer">
                
              </div><!-- /.box-footer-->
            </form>
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
@endsection    
