@extends('seller/master')
@section('content')
      <!-- Right side column. Contains the navbar and content of the page -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            
            <small></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Delivered Order</a></li>
            <li class="active">List</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          @if (session('success'))
              <div class="alert alert-success">
                  {{ session('success') }}
              </div>
          @endif
          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">{{$title}}</h3>
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            
              <div class="box-body table-responsive">
                
                <div class="row">
                  <div class="col-md-12">
                    <table id="example2" class="table">
                      <thead>
                        <th>Order Date/Time</th>
                        <th>Order Number</th>
                        <th>Model No.</th>
                        <th>Title</th>
                        <th>Payment Mode</th>
                        <th>Status</th>
                        <th>Action</th>
                      </thead>
                      <tbody>
                        @foreach($data as $row)
                        <tr>
                          <td>{{$row->order_date}}</td>
                          <td>{{$row->order_unique_no}}</td>
                          <td>{{$row->model}}</td>
                          <td>{{$row->title}}</td>
                          <td>{{$row->choose_payment}}</td>
                          <td><span class="os{{$row->id}}">{{$row->order_status}}</span></td>
                          <td>
                            <a href="{{URL('/seller/myorder/invoice')}}/{{$row->order_unique_no}}" class="btn btn-warning" title="Invoice"><i class="fa fa-edit"></i> Invoice</a>
                            <a href="{{URL('/seller/myorder-view')}}/{{$row->order_unique_no}}" class="btn btn-success" title="View"><i class="fa fa-eye"></i></a>
                            <button data-id="{{$row->id}}" data-order="{{$row->order_unique_no}}" class="actionid btn btn-danger" title="Action">Action</button>
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div><!-- /.box-body -->
              <div class="box-footer">
                
              </div><!-- /.box-footer-->
            
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <div class="modal fade" id="actionbox">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
              <h4 class="modal-title"> </h4>
            </div>
            <form id="actionform" method="post" action="">
              @csrf
            <div class="modal-body" id="">
              <input type="hidden" id="order_idhidden" name="order_id" class="form-control" >
              <label>Select Any</label>
              <select class="form-control" name="order_status" required>
                <option value="">Choose One</option>
                <option value="Active">Active</option>
                <option value="Seller Cancel">Cancel</option>
                <option value="In Progress">In Progress</option>
                <option value="Dispatched">Dispatched</option>
                <option value="Delivered">Delivered</option>
              </select>
              <label>Remarks</label>  
              <textarea name="order_status_reasion" class="form-control"></textarea> 
            </div>
            <div class="modal-footer justify-content-between">
              
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Save</button>
            </div>
            </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
@endsection    

