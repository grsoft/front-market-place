@extends('supper_admin/master')
@section('content')
      <style type="text/css">
        @media screen and (min-width: 768px) {
          .text-sm {
            width:150px;
          }
          .text-large {
            width:450px;
          }
        }
        ul.tul {
            display: inline-flex;
            list-style: decimal;
        }
        .tul li{
          margin-right: 20px;
        }
      </style>
      <!-- Right side column. Contains the navbar and content of the page -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            
            <small></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Product</a></li>
            <li class="active">Edit</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          @if (session('success'))
              <div class="alert alert-success">
                  {{ session('success') }}
              </div>
          @endif
          <div class="row">
            
            <form class="formtag " id="brand" method="post" enctype='multipart/form-data' action="{{ URL('/supper_admin/product-update')}}">
              @csrf 
              <input type="hidden" name="id" value="{{$data[0]->id}}">
            <div class="col-md-12">
              
                <div class="box box-primary">
                  <div class="box-header with-border">
                    <h3 class="box-title">Brand</h3>
                    
                  </div><!-- /.box-header -->
                  <div class="box-body no-padding">
                    
                    <div class="table-responsive mailbox-messages">
                      
                      <table class="table ">
                        <tr>
                          <td>Brand Name <span>*</span></td>
                          <td><input type="text" name="brand" class="form-control text-large" value="{{$data[0]->brand}}" required></td>
                        </tr>
                      </table>
                    </div>
                    
                  </div>
                  
                </div>
              
              
              
                <div class="box box-primary">
                  <div class="box-header with-border">
                    <h3 class="box-title">Item</h3>
                    
                  </div><!-- /.box-header -->
                  <div class="box-body no-padding">
                    
                    <div class="table-responsive mailbox-messages">
                      
                      <table class="table ">
                        <tbody>
                          <tr>
                            <td>Status</td>
                            <td>
                              <select class="form-control text-sm" name="status" required value="">

                                <option value="">Select Status</option>
                                @if ($data[0]->status =="1")
                                <option selected="true" value="1">Active</option>
                                @else
                                <option value="1">Active</option>
                                @endif
                                @if ($data[0]->status =="0")
                                <option selected value="0">Inctive</option>
                                @else
                                <option value="0">Inctive</option>
                                @endif
                              </select>
                            </td>
                          </tr>
                          <tr>
                            <td>Product Title</td>
                            <td><input type="text" name="title" class="form-control text-large" value="{{$data[0]->title}}" required></td>
                          </tr>
                          <tr>
                            <td>Price (MRP)</td>
                            <td><input type="number" onkeyup="setPrice(this.value,'mrp')" id="mrp" name="mrp" step="0.01" class="form-control text-sm" value="{{$data[0]->mrp}}" required></td>
                          </tr>
                          <tr>
                            <td>Discount %</td>
                            <td><input type="number" onkeyup="setPrice(this.value,'disc')" id="discount"  name="discount" step="0.01" min="0" max="99.99" class="form-control text-sm" value="{{$data[0]->discount}}" required> 
                              <input type="hidden" id="discount_amt" name="discount_amt" value="{{$data[0]->discount_amt}}">
                            </td>
                          </tr>
                          <tr>
                            <td>Sell Price</td>
                            <td><input type="number" id="sell_price" name="sell_price" class="form-control text-sm" value="{{$data[0]->sell_price}}" required>  </td>
                          </tr>
                        </tbody>
                      </table><!-- /.table -->
                    </div><!-- /.mail-box-messages -->
                  </div><!-- /.box-body -->
                  
                </div><!-- /. box -->
              
                <div class="box box-primary">
                  <div class="box-header with-border">
                    <h3 class="box-title">Image</h3>
                    
                  </div><!-- /.box-header -->
                  <div class="box-body no-padding">
                    
                    <div class="table-responsive mailbox-messages">
                      
                      <div class="row" style="padding: 15px;">
                        <div class="col-md-12">
                          <div class="form-group">
                            <label>Product Image</label>
                            <input type="file" name="photo" id="fileToUploadsingle" ondrop="loadFileSingle(event)"  onchange="loadFileSingle(event)" multiple="false"  class="form-control" value="{{old('photo')}}" >
                          </div>
                          <img id="image" width="100px" height="100px" src="{{URL::asset('product_image')}}/{{$data[0]->image}}">
                        </div>
                        <div class="col-md-12"><br>
                          <label>Thumbnail Image</label><br>
                          <div class="form-group"style="border:1px solid black;height:145px; padding: 2px;overflow: auto;">
                            <ul class="tul">
                              @foreach($thumb as $th)
                              <li style="padding: 10px;">
                              <img id="image" width="100px" height="100px" src="{{URL::asset('thumb_image')}}/{{$th->image}}">
                              <br>
                              <input type="checkbox" value="{{$th->id}}" name="remove_thumb[]"> Remove
                              </li>
                              @endforeach
                            </ul>
                          </div>
                        </div>
                        <div class="col-md-12">
                          <label>Upload Thumbnail Image</label>
                          
                            <div style="display: block;"><input type="file" name="thumb[]" id="fileToUpload" ondrop="loadFile(event)"  onchange="loadFile(event)" multiple="true"  class="form-control" value="{{old('thumb')}}" >
                            </div>

                          
                          <div id="dropContainer" onmouseout="dragEnd()" class="form-group"style="border:1px solid black;height:145px; padding: 2px;overflow: auto;">
                            <div style="text-align: center;"><br>
                              <a id="browsfile" class="btn btn-success">Browse File to Stystem</a><br>
                              <span style="text-align: center; font-size: 30px; color:skyblue; ">Drag & Drop Here</span>
                            </div>
                              
                          </div>
                
                        </div>
                      </div>
                    </div>
                    
                  </div>
                  
                </div>
                <div class="box box-primary">
                  <div class="box-header with-border">
                    <h3 class="box-title">About Product</h3>
                    
                  </div><!-- /.box-header -->
                  <div class="box-body no-padding">
                    
                    <div class=" mailbox-messages">
                      <div class="row" style="padding: 15px;">
                        <div class="col-md-12">
                          <div class="form-group">
                            <label for="">Product Details</label>
                            <textarea name="about" id="text" rows="12" class="form-control">{{$data[0]->about}}</textarea>
                          </div>
                        </div>
                  
                      </div>
                    </div>
                    
                  </div>
                  
                </div>
                <div class="box box-primary">
                  <div class="box-header with-border">
                    <h3 class="box-title">Color Varients</h3>
                    
                  </div><!-- /.box-header -->
                  <div class="box-body no-padding">
                    
                    <div class="table-responsive mailbox-messages">
                      <div class="row" style="padding: 15px;">
                        <div class="col-md-12">
                          <label>Old Varient Image</label>
                          <div class="form-group"style="border:1px solid black;height:145px; padding: 2px;overflow: auto;">
                            <ul class="tul">
                              @foreach($color_variente as $th)
                              <li style="padding: 10px;">
                              <img id="image" width="100px" height="100px" src="{{URL::asset('color_image')}}/{{$th->image}}">
                              <br>
                              <input type="checkbox" value="{{$th->id}}" name="remove_varient[]"> Remove
                              </li>
                              @endforeach
                            </ul>
                          </div>
                        </div>
                        <div class="col-md-12">
                          <div class="form-group">
                            <label>Upload Varient Image</label>
                            
                          </div>
                          
                        </div>
                        <div class="col-md-12">
                          
                          
                            <div style="display: block;"><input type="file" name="colorphoto[]" id="fileToUpload1" ondrop="loadFile1(event)"  onchange="loadFile1(event)" multiple="true"  class="form-control" value="{{old('colorphoto')}}">
                            </div>

                          
                          <div id="dropContainer1"  onmouseout="dragEnd1()" class="form-group"style="border:1px solid black;height:145px; padding: 2px;overflow: auto;">
                            <div style="text-align: center;"><br>
                              <a id="browsfile1" class="btn btn-success">Browse File to Stystem</a><br>
                              <span style="text-align: center; font-size: 30px; color:skyblue; ">Drag & Drop Here</span>
                            </div>
                              
                          </div>
                
                        </div>
                      </div>
                    </div>
                    
                  </div>
                  
                </div>
                <div class="box box-primary">
                  <div class="box-header with-border">
                    <h3 class="box-title">SEO</h3>
                    
                  </div><!-- /.box-header -->
                  <div class="box-body no-padding">
                    
                    <div class="table-responsive mailbox-messages">
                      <table class="table table-hover table-striped">
                        <tbody>
                          
                          <tr>
                            <td>Meta Title</td>
                            <td><input type="text" name="meta_title" class="form-control" value="{{$data[0]->meta_title}}"></td>
                          </tr>
                          <tr>
                            <td>Keywords</td>
                            <td><input type="text" name="keywords" class="form-control" value="{{$data[0]->keywords}}" ></td>
                          </tr>
                          
                        </tbody>
                      </table><!-- /.table -->
                    </div><!-- /.mail-box-messages -->
                  </div><!-- /.box-body -->
                  <div class="box-footer no-padding">
                    <div class="mailbox-controls">
                      <button type="submit" class="btn btn-default ">Submit</button>
                    </div>
                  </div>
                </div><!-- /. box -->
              
            </div><!-- /.col -->
            </form>
          </div><!-- /.row -->
        </section>
        
      </div><!-- /.content-wrapper -->
      <script src="{{URL::asset('admins/plugins/iCheck/icheck.min.js') }}" type="text/javascript"></script>
      
@endsection    
