<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Session;
class logincheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    { 
        $path=$request->path();
        $patharr=explode('/', $path);
        if($patharr[0]=='supper_admin')
        {
            if($path=='supper_admin/login' && Session::get('id')=='')
            {}
            else if($path !='supper_admin/index' && Session::get('id')=='')
            { 
                return redirect('supper_admin/index');
            }
        }
        elseif($patharr[0]=='admin')
        {
            if($path=='admin/login' && Session::get('aid')=='')
            {}
            else if($path !='admin/index' && Session::get('aid')=='')
            { 
                return redirect('admin/index');
            }
        }
        elseif($patharr[0]=='seller')
        {
            if($path=='seller/login' && Session::get('sid')=='')
            {}
            else if($path !='seller/index' && Session::get('sid')=='')
            { 
                return redirect('seller/index');
            }
        }
        return $next($request);
    }
}
