<?php

namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;
class admin2Login extends Controller
{
    function adminLogin(Request $req)
    {
        //return $req->input();
        $user=DB::table('admin_user')->where(['email'=>$req->username])->first();
        $pass=DB::table('admin_user')->where(['password'=>$req->password])->first();
        if(empty($user))
        {
            return "Username is Wrong.....";
        }
        elseif(empty($pass))
        {
            return "Password is Incurrect.....";
        }
        else
        {
            session()->flush() ;
            session()->put('aid',$user->id);
            session()->put('user_id',$user->id);
            session()->put('user_by','admin');
            session()->put('root','admin');
            return redirect('admin/dashboard');
        }
    }
}
