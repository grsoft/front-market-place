<?php

namespace App\Http\Controllers\seller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use Session;
class v_category extends Controller
{
    function index($action , $id=null)
    {
        if($action=='form')
        { 
            return view('pannel/category_form')->with('title','Category Form');
        }
        if($action=='list')
        { 
            $data=DB::table('master_category')->where('childe_category_id',0)->where('deleted_status',0)->get();
            return view('pannel/category_list')->with('title','Category List')->with('data',$data);
        } 
        if($action=='edit')
        { 
            $data=DB::table('master_category')->where('id',$id)->first();
            return view('pannel/category_edit')->with('title','Category Edit')->with('data',$data);
        }
        if($action=='delete')
        { 
            $deleted_at=date('Y-m-d H:i:s');
            $deleted_id=Session::get('user_id');
            $deleted_by=Session::get('user_by');
            $deleted_status=1;
            $data=DB::select("UPDATE master_category SET deleted_at='$deleted_at', deleted_id='$deleted_id', deleted_by='$deleted_by', deleted_status='$deleted_status' WHERE id='$id'");
            return back()->with('success',"Category deleted successfully.");;
        }
        if($action=='ajax-subcate-by-cat-id')
        { 
            $data=DB::table('master_category')->where('childe_category_id',$id)->where('deleted_status',0)->where('tree_category_id','=',$id)->get();
            return $data;
        }
        if($action=='ajax-childe-by-subcat-id')
        { 
            $data=DB::table('master_category')->where('childe_category_id',$id)->where('deleted_status',0)->where('tree_category_id','!=',$id)->get();
            return $data;
        } 
        if($action=='sub-form')
        { 
            $cdata=DB::table('master_category')->where('childe_category_id',0)->where('deleted_status',0)->get();
            return view('pannel/sub_category_form')->with('title','Sub Category Form')->with('category',$cdata);
        }
        if($action=='sub-list')
        { 
            $data = DB::select("SELECT mca.title as parrent,mcb.title as category,mcb.id as scid  FROM `master_category` as mca,master_category as mcb where mca.id = mcb.childe_category_id AND mcb.deleted_status='0'");
            return view('pannel/sub_category_list')->with('title','Sub Category List')->with('data',$data);
        } 
        if($action=='view-sub')
        { 
            //$data=DB::select("SELECT sca.title as title FROM sub_category AS sca,sub_category AS scb WHERE sca.id=scb.childe_category_id AND sca.id='$id'");
            $datasubcat=array();
            $data = DB::select("SELECT master_category.*  FROM `master_category`  WHERE id='$id' ");
            //print_r($data);
            $arr=explode(',', $data[0]->tree_category_id);
            if(count($arr)>0)
            {
            $str=implode(',', $arr);
            //$arr= array_values($arr);
            $datasubcat = DB::select("SELECT title,id  FROM `master_category` where id IN ($str) ");
            }
            return view('pannel/view_sub_category')->with('title','View Category')->with('data',$data)->with('subdata',$datasubcat);
        } 
        if($action=='del-sub')
        { 
            $deleted_at=date('Y-m-d H:i:s');
            $deleted_id=Session::get('user_id');
            $deleted_by=Session::get('user_by');
            $deleted_status=1;
            $data=DB::select("UPDATE master_category SET deleted_at='$deleted_at', deleted_id='$deleted_id', deleted_by='$deleted_by', deleted_status='$deleted_status' WHERE id='$id'");
            return back()->with('success',"Category deleted successfully.");
        }
        if($action=='edit-sub')
        { 
            //$data=DB::select("SELECT sca.title as title FROM sub_category AS sca,sub_category AS scb WHERE sca.id=scb.childe_category_id AND sca.id='$id'");
            $cdata=DB::table('master_category')->where('childe_category_id',0)->get();
            $datasubcat=array();
            $data = DB::select("SELECT master_category.*  FROM `master_category`  WHERE id='$id' ");
            //print_r($data);
            $arr=explode(',', $data[0]->tree_category_id);
            if(count($arr)>0)
            {
            $str=implode(',', $arr);
            //$arr= array_values($arr);
            $datasubcat = DB::select("SELECT childe_category_id, title,id  FROM `master_category` where id IN ($str) ");
            }
            foreach ($datasubcat as $key => $value) {
                $datasamecat[] = DB::select("SELECT title,id  FROM `master_category` where childe_category_id='$value->id' ");
            }
            return view('pannel/sub_category_edit')->with('title','Edit Category')->with('category',$cdata)->with('data',$data)->with('subdata',$datasubcat)->with('samedata',$datasamecat);
        }
        
    }
    function category_save(Request $req)
    {
            $_POST=$req->all();
            unset($_POST['_token']);
            $_POST['created_at']=date('Y-m-d H:i:s');
            $_POST['created_id']=Session::get('user_id');
            $_POST['created_by']=Session::get('user_by');
            $req->validate([
                'category_image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'title'  => 'required||min:2',
                'status'=> 'required'
            ]);
            if($req->has('category_image'))
            {
                $imageName = time().'.'.$req->category_image->extension();
                $_POST['image']=$imageName;
                
                $req->category_image->move(public_path('category_image'), $imageName);
            }  
            unset($_POST['category_image']);
            
           // $emp->emp_photo=$imageName;
            $insert=DB::table('master_category')->insert([$_POST]);
            if($insert)
            {
                return redirect('supper_admin/category/form')->with('success',"Category created successfully.");
            }
    }
    function sub_category_save(Request $req)
    {
            $_POST=$req->all();
            unset($_POST['_token']);
            $_POST['created_at']=date('Y-m-d H:i:s');
            $_POST['created_id']=Session::get('user_id');
            $_POST['created_by']=Session::get('user_by');
            $title=$_POST['title'];
            $arr=array();
            foreach ($title as $key => $value) { 
                $tval=str_replace(' ','',$value);
                if(strlen($tval) != 0)
                { 
                $arr[$key]['category_id']=$_POST['category_id']; 
                $arr[$key]['childe_category_id']=$_POST['childe_category_id']; 
                $arr[$key]['tree_category_id']=$_POST['tree_category_id'];    
                $arr[$key]['created_at']=$_POST['created_at'];
                $arr[$key]['created_id']=$_POST['created_id'];
                $arr[$key]['created_by']=$_POST['created_by'];
                $arr[$key]['title']=$value;
                }
            }
            //print_r($arr);exit;
            $insert=DB::table('master_category')->insert($arr);
            if($insert)
            {
                return back()->with('success',"Sub category created successfully.");
            }
    }
    function category_edit(Request $req)
    {
            $_POST=$req->all();
            $id=$_POST['id'];
            unset($_POST['_token']);
            unset($_POST['id']);
            $_POST['updated_at']=date('Y-m-d H:i:s');
            $_POST['updated_id']=Session::get('user_id');
            $_POST['updated_by']=Session::get('user_by');
            $req->validate([
                'category_image1' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'title'  => 'required||min:2',
                'status'=> 'required'
            ]);
            if($req->has('category_image1'))
            {
                $imageName = time().'.'.$req->category_image1->extension();
                $_POST['image']=$imageName;
                $req->category_image1->move(public_path('category_image'), $imageName);
                unset($_POST['category_image1']);
                
            }  
            
            unset($_POST['category_image']);
           // $emp->emp_photo=$imageName;
            //$update=category::where('id', $id)
            //  ->update($_POST);
            $update=DB::table('master_category')->where('id',$id)->update($_POST            );
            if($update)
            { 
                return back()->with('success',"Category updated successfully.");
            }
            else{
                return back();
            }
    }
    function sub_category_edit(Request $req)
    {
            $_POST=$req->all();
            $id=$_POST['id'];
            unset($_POST['_token']);
            unset($_POST['id']);
            $_POST['updated_at']=date('Y-m-d H:i:s');
            $_POST['updated_id']=Session::get('user_id');
            $_POST['updated_by']=Session::get('user_by');
            $req->validate([
                'title'  => 'required||min:2',
                
            ]);
            
            $update=DB::table('master_category')->where('id',$id)->update($_POST            );
            if($update)
            { 
                return back()->with('success',"Category updated successfully.");
            }
            else{
                return back();
            }
    }

}
