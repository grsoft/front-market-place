<?php

namespace App\Http\Controllers\seller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PDF;
use Session;
use File;
use Image;
class v_order extends Controller
{
    
    function index($action , $id=null)
    {
        if($action=='form')
        { 
        	$cdata=DB::table('master_category')->where('childe_category_id',0)->where('deleted_status',0)->get();
            return view('seller/form/master_form')->with('title','Product Form1')->with('category',$cdata);
        }
        
        
    }
    function myorder_all(){
        
        $data = DB::table('orders')
            ->join('order_item', 'orders.unique_no', '=', 'order_item.order_unique_no')
            ->join('product', 'product.id', '=', 'order_item.product_id')
            ->select('product.model as model','product.title as title','order_item.*', 'orders.order_status', 'orders.order_date', 'orders.choose_payment')
            ->where(function($query){
                $query->where('orders.order_status','Active');
                $query->orWhere('orders.order_status','In Progress');
                $query->orWhere('orders.order_status','Dispatched');
            })
            ->where('order_item.seller_id','=',session()->get('sid'))
            ->get();
            //return $data;
            return view('seller/v_myorder_list')->with('title','My Order List')->with('data',$data);
    }
    function delivered_order_all(){
        $data = DB::table('orders')
            ->join('order_item', 'orders.unique_no', '=', 'order_item.order_unique_no')
            ->join('product', 'product.id', '=', 'order_item.product_id')
            ->select('product.model as model','product.title as title','order_item.*', 'orders.order_status', 'orders.order_date', 'orders.choose_payment')
            ->where('orders.order_status','Delivered')
            ->where('order_item.seller_id',session()->get('sid'))
            ->get();
            //return $data;
            return view('seller/v_delivered_order_list')->with('title','Delivered Order List')->with('data',$data);
    }
    public function invoiceView($order_number)
    {
        $data=DB::select("SELECT c.* , o.* FROM orders o INNER JOIN customer c on c.id=o.customer_id WHERE o.unique_no='".$order_number."'");
        $ship_data=DB::table('shiping_address')->where('order_number',$order_number)->get();

        $cdata=DB::select("SELECT p.image,p.brand,p.title, oi.* FROM order_item oi INNER JOIN product p on p.id=oi.product_id WHERE oi.order_unique_no='".$order_number."'");
        $data1=array(
            'odata'=>$data,
            'ship_data' => $ship_data,
            'cdata' =>$cdata
        );
        return View('seller/v_myorder_view',$data1)->with('title','Order Summary');
    
    }
    public function invoicePDF($order_number)
    {
        $data=DB::select("SELECT c.* , o.* FROM orders o INNER JOIN customer c on c.id=o.customer_id WHERE o.unique_no='".$order_number."'");
        $ship_data=DB::table('shiping_address')->where('order_number',$order_number)->get();

        $cdata=DB::select("SELECT p.image,p.brand,p.title, oi.* FROM order_item oi INNER JOIN product p on p.id=oi.product_id WHERE oi.order_unique_no='".$order_number."'");
        $data1=array(
            'odata'=>$data,
            'ship_data' => $ship_data,
            'cdata' =>$cdata
        );
        /*$data = [
            'title' => 'Welcome to ItSolutionStuff.com',
            'date' => date('m/d/Y')
        ];*/
        //ini_set('max_execution_time', 120);
        $pdf = PDF::loadView('seller/v_myorder_invoice',$data1);
        $pdf->getDomPDF()->setHttpContext(
            stream_context_create([
                'ssl' => [
                    'allow_self_signed'=> TRUE,
                    'verify_peer' => FALSE,
                    'verify_peer_name' => FALSE,
                ]
            ])
        );
        return $pdf->download($order_number.'-invoice.pdf');
    }
    public function orderActionSave(Request $req)
    {
        $order_id=$req->order_id;
        $order_status=$req->order_status;
        $text=$req->order_status_reasion;
        $update=DB::table('orders')->where('id', $order_id)
              ->update(['order_status' => $order_status,'order_status_resion' => $text]);
        if($update)
        {
            return response()->json(['status'=>'success','id'=>$order_id,'os'=>$order_status]);
            //return redirect('/seller/myorder')->with('success',"Order status is updated");
        }
    }
}
