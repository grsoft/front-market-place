<?php

namespace App\Http\Controllers\seller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;
use File;
use Image;
class v_product extends Controller
{
    
    function index($action , $id=null)
    {
        if($action=='form')
        { 
        	$cdata=DB::table('master_category')->where('childe_category_id',0)->where('deleted_status',0)->get();
            return view('seller/form/master_form')->with('title','Product Form1')->with('category',$cdata);
        }
        if($action=='listing-form')
        { 
            return view('seller/form/second_form')->with('page',session()->get('page'))->with('title',"Listing Form")->with('id',session()->get('user_id'));
        }
        if($action=='list')
        { 
            $data=DB::select("SELECT * FROM product WHERE created_id='".session()->get('user_id')."' AND created_by='seller'");
            return view('seller/v_product_list')->with('title','Product List')->with('data',$data);
        } 
        if($action=='edit')
        { 
            $data=DB::select("SELECT * FROM product WHERE id='".$id."'");
            $thumb=DB::select("SELECT * FROM product_thumbnail WHERE product_id='".$id."'");
            $color_variente=DB::select("SELECT * FROM color_variante WHERE product_id='".$id."'");
            return view('seller/form/second_form_edit')->with('title','Product Edit')->with('data',$data)->with('thumb',$thumb)->with('color_variente',$color_variente);
        }
        if($action=='view')
        { 
            $data=DB::select("SELECT * FROM product WHERE id='".$id."'");
            $thumb=DB::select("SELECT * FROM product_thumbnail WHERE product_id='".$id."'");
            $color_variente=DB::select("SELECT * FROM color_variante WHERE product_id='".$id."'");
            return view('seller/form/product_view')->with('title','Product Edit')->with('data',$data)->with('thumb',$thumb)->with('color_variente',$color_variente);
        }
        if($action=='delete')
        { 
            $deleted_at=date('Y-m-d H:i:s');
            $deleted_id=Session::get('user_id');
            $deleted_by=Session::get('user_by');
            $deleted_status=1;
            $data=DB::select("UPDATE product SET deleted_at='$deleted_at', deleted_id='$deleted_id', deleted_by='$deleted_by', deleted_status='$deleted_status' WHERE id='$id'");
            return back()->with('success',"product deleted successfully.");
        }
        if($action=='ajax-subcate-by-cat-id')
        { 
            $data=DB::table('master_category')->where('category_id',$id)->where('deleted_status',0)->get();
            return $data;
        }
        
    }
    function product_save(Request $req)
    {
        //session()->put('step1',$req->all());
        $req->validate([
            'brand' => 'required',
            'status' => 'required',
            'title' => 'required',
            'mrp' => 'required',
            'discount' => 'required',
            'sell_price' => 'required',
            'photo' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048'

        ]);
        unset($_POST['_token']);
            $_POST["created_at"] = date('Y-m-d H:i:s');
            $_POST['created_id']=Session::get('user_id');
            $_POST['created_by']=Session::get('user_by');
            $_POST['category_id']=Session::get('category_id');
            $_POST['indicate_root']=Session::get('indicate_root');
            $_POST['parent_id']=Session::get('parent_id');
            
        if($req->has('photo'))
        {
            //$imageName = time().'.'.$req->photo->extension();
            //$_POST['image_zoom']=$imageName;
            $image = $req->file('photo');
            $input['imagename'] = time().rand(1,50).'p-thumb.'.$image->extension();
         
            $destinationPath = public_path('/product_image');
            $img = Image::make($image->path());
            $img->resize(173, 211, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath.'/'.$input['imagename']);
            $_POST['image']=$input['imagename'];
            $input['imagename'] = time().rand(1,50).'p-zoom.'.$image->extension();
         
            $destinationPath = public_path('/product_image');
            $img = Image::make($image->path());
            $img->resize(840, 1024, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath.'/'.$input['imagename']);
            $_POST['image_zoom']=$input['imagename'];
            //$req->photo->move(public_path('product_image'), $imageName);
        }  
        unset($_POST['photo']);
        $thumb = [];
        if($req->has('thumb'))
        { 
            foreach($req->file('thumb') as $in=>$file)
            {
                //$name = time().rand(1,50).'.'.$file->extension();
                $image = $file;
                $input['imagename'] = time().rand(1,50).'_thumb.'.$image->extension();
                $destinationPath = public_path('/thumb_image');
                $img = Image::make($image->path());
                $img->resize(173, 211, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath.'/'.$input['imagename']);
                $thumb[$in]['image']=$input['imagename'];

                $input['imagename'] = time().rand(1,50).'_t-zoom.'.$image->extension();
                $destinationPath = public_path('/thumb_image');
                $img = Image::make($image->path());
                $img->resize(840, 1024, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath.'/'.$input['imagename']);
                $thumb[$in]['image_zoom']=$input['imagename'];
                //$file->move(public_path('thumb_image'), $name);  
                //$thumb[$in]['image_zoom'] = $name;  
            }
        }
        unset($_POST['thumb']);
        $color_photo = [];
        if($req->has('colorphoto'))
        { 
            foreach($req->file('colorphoto') as $in=>$file)
            {
                $image = $file;
                $input['imagename'] = time().rand(1,50).'.'.$image->extension();
                $destinationPath = public_path('/color_image');
                $img = Image::make($image->path());
                $img->resize(173, 211, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath.'/'.$input['imagename']);
                $color_photo[$in]['image']=$input['imagename'];
                $input['imagename'] = time().rand(1,50).'c-color.'.$image->extension();
                $destinationPath = public_path('/color_image');
                $img = Image::make($image->path());
                $img->resize(840, 1024, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath.'/'.$input['imagename']);
                $color_photo[$in]['image_zoom']=$input['imagename'];
                //$name = time().rand(1,50).'_thumb.'.$file->extension();
                //$file->move(public_path('color_image'), $name);  
                //$color_photo[$in]['image_zoom'] = $name;  
            }
        }
        unset($_POST['colorphoto']);
        //addition informatin data keep in json
        if(isset($_POST['json_data']))
        {
            $arr_json_field=$_POST['json_data'];
            $new_array=array();
            foreach($arr_json_field as $key => $value){
                $new_array[$value]=$_POST[$value];
                
            }
            $_POST['json_data']=json_encode($new_array);
        }
        $insert=DB::table('product')->insert([$_POST]);
        $id= DB::getPdo()->lastInsertId();
        $data_thumb=[];
        foreach ($thumb as $key => $value) {
            $data_thumb[$key]['product_id']=$id;
            $data_thumb[$key]['image']=$thumb[$key]['image'];
            $data_thumb[$key]['image_zoom']=$thumb[$key]['image_zoom'];
        }
        $data_color_photo=[];
        foreach ($color_photo as $key => $value) {
            $data_color_photo[$key]['product_id']=$id;
            $data_color_photo[$key]['image']=$color_photo[$key]['image'];
            $data_color_photo[$key]['image_zoom']=$color_photo[$key]['image_zoom'];
        }
        

        $insert=DB::table('product_thumbnail')->insert($data_thumb);
        $insert=DB::table('color_variante')->insert($data_color_photo);
        return back()->with('success',"Your product added successfully.");
    }
    function product_update(Request $req)
    {
        //session()->put('step1',$req->all());
        $req->validate([
            'brand' => 'required',
            'status' => 'required',
            'title' => 'required',
            'mrp' => 'required',
            'discount' => 'required',
            'sell_price' => 'required'
            

        ]);
        if(!empty($req->remove_thumb)){
            foreach ($req->remove_thumb as $key => $selected) {
                $getdata=DB::select("SELECT image FROM product_thumbnail WHERE id='".$selected."'");
                if(!empty($getdata))
                {
                    if(File::exists(public_path('thumb_image/'.$getdata[0]->image))){ 
                        File::delete(public_path('thumb_image/'.$getdata[0]->image));
                    }
                    //unlink(public_path('thumb_image/'.$getdata[0]->image));
                    DB::SELECT("DELETE FROM product_thumbnail WHERE id='".$selected."'");
                }
                
            }
        }
        unset($_POST['remove_thumb']);
        if(!empty($req->remove_varient)){
            foreach ($req->remove_varient as $key => $selected) {
                $getdata=DB::select("SELECT image FROM color_variante WHERE id='".$selected."'");
                if(!empty($getdata))
                {
                    if(File::exists(public_path('color_image/'.$getdata[0]->image))){ 
                        File::delete(public_path('color_image/'.$getdata[0]->image));
                    }
                    //unlink('color_image/'.$getdata[0]->image);
                    DB::SELECT("DELETE FROM color_variante WHERE id='".$selected."'");
                }
                
            }
        }
        unset($_POST['remove_varient']);
        unset($_POST['_token']);
        $_POST["updated_at"] = date('Y-m-d H:i:s');
        $_POST['updated_id']=Session::get('user_id');
        $_POST['updated_by']=Session::get('user_by');
            
            
        // if($req->has('photo'))
        // {
        //     $imageName = time().'.'.$req->photo->extension();
        //     $_POST['image']=$imageName;
            
        //     $req->photo->move(public_path('product_image'), $imageName);
        // }  
        // unset($_POST['photo']);
        // $thumb = [];
        // if($req->has('thumb'))
        // { 
        //     foreach($req->file('thumb') as $file)
        //     {
        //         $name = time().rand(1,50).'.'.$file->extension();
        //         $file->move(public_path('thumb_image'), $name);  
        //         $thumb[] = $name;  
        //     }
        // }
        // unset($_POST['thumb']);
        // $color_photo = [];
        // if($req->has('colorphoto'))
        // { 
        //     foreach($req->file('colorphoto') as $file)
        //     {
        //         $name = time().rand(1,50).'.'.$file->extension();
        //         $file->move(public_path('color_image'), $name);  
        //         $color_photo[] = $name;  
        //     }
        // }
        // unset($_POST['colorphoto']);

        if($req->has('photo'))
        {
            //$imageName = time().'.'.$req->photo->extension();
            //$_POST['image_zoom']=$imageName;
            $image = $req->file('photo');
            $input['imagename'] = time().rand(1,50).'p-thumb.'.$image->extension();
         
            $destinationPath = public_path('/product_image');
            $img = Image::make($image->path());
            $img->resize(173, 211, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath.'/'.$input['imagename']);
            $_POST['image']=$input['imagename'];
            $input['imagename'] = time().rand(1,50).'p-zoom.'.$image->extension();
         
            $destinationPath = public_path('/product_image');
            $img = Image::make($image->path());
            $img->resize(840, 1024, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath.'/'.$input['imagename']);
            $_POST['image_zoom']=$input['imagename'];
            //$req->photo->move(public_path('product_image'), $imageName);
        }  
        unset($_POST['photo']);
        $thumb = [];
        if($req->has('thumb'))
        { 
            foreach($req->file('thumb') as $in=>$file)
            {
                //$name = time().rand(1,50).'.'.$file->extension();
                $image = $file;
                $input['imagename'] = time().rand(1,50).'_thumb.'.$image->extension();
                $destinationPath = public_path('/thumb_image');
                $img = Image::make($image->path());
                $img->resize(173, 211, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath.'/'.$input['imagename']);
                $thumb[$in]['image']=$input['imagename'];

                $input['imagename'] = time().rand(1,50).'_t-zoom.'.$image->extension();
                $destinationPath = public_path('/thumb_image');
                $img = Image::make($image->path());
                $img->resize(840, 1024, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath.'/'.$input['imagename']);
                $thumb[$in]['image_zoom']=$input['imagename'];
                //$file->move(public_path('thumb_image'), $name);  
                //$thumb[$in]['image_zoom'] = $name;  
            }
        }
        unset($_POST['thumb']);
        $color_photo = [];
        if($req->has('colorphoto'))
        { 
            foreach($req->file('colorphoto') as $in=>$file)
            {
                $image = $file;
                $input['imagename'] = time().rand(1,50).'.'.$image->extension();
                $destinationPath = public_path('/color_image');
                $img = Image::make($image->path());
                $img->resize(173, 211, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath.'/'.$input['imagename']);
                $color_photo[$in]['image']=$input['imagename'];
                $input['imagename'] = time().rand(1,50).'c-color.'.$image->extension();
                $destinationPath = public_path('/color_image');
                $img = Image::make($image->path());
                $img->resize(840, 1024, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath.'/'.$input['imagename']);
                $color_photo[$in]['image_zoom']=$input['imagename'];
                //$name = time().rand(1,50).'_thumb.'.$file->extension();
                //$file->move(public_path('color_image'), $name);  
                //$color_photo[$in]['image_zoom'] = $name;  
            }
        }
        unset($_POST['colorphoto']);

        
        $insert=DB::table('product')->where('id',$req->id)->update($_POST);
        $id= $req->id;
        // $data_thumb=[];
        // foreach ($thumb as $key => $value) {
        //     $data_thumb[$key]['product_id']=$id;
        //     $data_thumb[$key]['image']=$value;
        // }
        // $data_color_photo=[];
        // foreach ($color_photo as $key => $value) {
        //     $data_color_photo[$key]['product_id']=$id;
        //     $data_color_photo[$key]['image']=$value;
        // }
        $data_thumb=[];
        foreach ($thumb as $key => $value) {
            $data_thumb[$key]['product_id']=$id;
            $data_thumb[$key]['image']=$thumb[$key]['image'];
            $data_thumb[$key]['image_zoom']=$thumb[$key]['image_zoom'];
        }
        $data_color_photo=[];
        foreach ($color_photo as $key => $value) {
            $data_color_photo[$key]['product_id']=$id;
            $data_color_photo[$key]['image']=$color_photo[$key]['image'];
            $data_color_photo[$key]['image_zoom']=$color_photo[$key]['image_zoom'];
        }
        if(!empty($data_thumb)){
            $insert=DB::table('product_thumbnail')->insert($data_thumb);
        }
        if(!empty($data_color_photo)){
            $insert=DB::table('color_variante')->insert($data_color_photo);
        }
        return back()->with('success',"Your product updated successfully.");
    }
    
    function product_step(Request $req)
    {
        //session()->put('step1',$req->all());
        $category_id=$req->category;
        $subcat=$req->sub_cate;
        $form_id=$subcat[count($subcat)-1];
        $form_id_data=DB::table('master_category')->join('form_table', 'form_table.id','=','master_category.form_id')->where('master_category.id',$form_id)->first();
        
        $indicate_root=$category_id.",".implode(',', $req->sub_cate);
        $parent_id=$req->sub_cate[count($req->sub_cate)-1];
        session()->put('category_id',$category_id);
        session()->put('indicate_root',$indicate_root);
        session()->put('parent_id',$parent_id);
        session()->put('include_form',!empty($form_id_data->form_name)?$form_id_data->form_name : "");
        return redirect ('seller/product/listing-form')->with('success',"Your product listing is ready.");
    }
    function product_step1(Request $req)
    {
        //session()->put('step1',$req->all());
        $arr=array(
            "created_at" => date('Y-m-d H:i:s'),
            'created_id'=>Session::get('user_id'),
            'created_by'=>Session::get('user_by')
        );

        $insert=DB::table('product')->insert([$arr]);
        $id= DB::getPdo()->lastInsertId();
        session()->put('title',"Product Listing");
        session()->put('id',$id);
        session()->put('page','brand');
        //session()->put('successed',"Your product listing is ready.");
        return redirect ('admin/product/listing-form')->with('success',"Your product listing is ready.");
    }
    function update_brand(Request $req)
    {
        $req->validate([
            'id' => 'required',
            'brand' => 'required'
        ]);
        $_POST=$req->all();
        unset($_POST['_token']);
        unset($_POST['id']);
        $insert=DB::table('product')->where('id',$req->id)->update($_POST);
        session()->put('title',"Product Listing");
        session()->put('id',$req->id);
        session()->put('page','item');
        //session()->put('successed',"Updated successfully.");
        return redirect ('supper_admin/product/listing-form/step2')->with('success',"Updated successfully.");
    }
    function update_item(Request $req)
    {
        $req->validate([
            'id' => 'required',
            'status' => 'required',
            'title' => 'required',
            'mrp' => 'required',
            'discount' => 'required',
            'sell_price' => 'required'
        ]);
        $_POST=$req->all();
        unset($_POST['_token']);
        unset($_POST['id']);
        session()->put('title',"Product Listing");
        session()->put('id',$req->id);
        session()->put('page','imageform');
        $insert=DB::table('product')->where('id',$req->id)->update($_POST);
        return redirect ('supper_admin/product/listing-form/step3')->with('success',"Updated successfully.");
    }

    function product_step3(Request $req)
    {
        //$step2=$req->all();
        $dd=$this->step1;
        echo "<pre>";print_r(session()->get('step1'));exit;
    }
    function category_save(Request $req)
    {
            $_POST=$req->all();
            unset($_POST['_token']);
            $_POST['created_at']=date('Y-m-d H:i:s');
            $_POST['added_id']=Session::get('user_id');
            $_POST['added_by']=Session::get('user_by');
            $req->validate([
                'category_image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'category_name'  => 'required||min:2',
                'category_status'=> 'required'
            ]);
            if($req->has('category_image'))
            {
                $imageName = time().'.'.$req->category_image->extension();
                $_POST['category_image']=$imageName;
                $req->category_image->move(public_path('category_image'), $imageName);
            }  
            
            
           // $emp->emp_photo=$imageName;
            $insert=DB::table('category')->insert([$_POST]);
            if($insert)
            {
                return redirect('pannel/category/form')->with('success',"Category created successfully.");
            }
    }
    function category_edit(Request $req)
    {
            $_POST=$req->all();
            $id=$_POST['id'];
            unset($_POST['_token']);
            unset($_POST['id']);
            $_POST['updated_at']=date('Y-m-d H:i:s');
            $_POST['updated_by_id']=Session::get('user_id');
            $_POST['updated_by']=Session::get('user_by');
            $req->validate([
                'category_image1' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'category_name'  => 'required||min:2',
                'category_status'=> 'required'
            ]);
            if($req->has('category_image1'))
            {
                $imageName = time().'.'.$req->category_image1->extension();
                $_POST['category_image']=$imageName;
                $req->category_image1->move(public_path('category_image'), $imageName);
                unset($_POST['category_image1']);
            }  
            
            
           // $emp->emp_photo=$imageName;
            $update=category::where('id', $id)
              ->update($_POST);
            //$update=DB::table('category')->where('id',$id)->update($_POST            );
            if($update)
            { 
                return back()->with('success',"Category updated successfully.");
            }
            else{
                return back();
            }
    }
}
