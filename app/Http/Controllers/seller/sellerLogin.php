<?php

namespace App\Http\Controllers\seller;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;
class sellerLogin extends Controller
{
    function sellerLogin(Request $req)
    {
        //return $req->input();
        $user=DB::table('seller_user')->where(['email_address'=>$req->email_address])->first();
        $pass=DB::table('seller_user')->where(['password'=>$req->password])->first();
        if(empty($user))
        {
            return back()->with('error', "Username is Wrong.....");
        }
        elseif(empty($pass))
        {
            return back()->with('error', "Username is Wrong.....");
        }
        else
        {
            session()->flush() ;
            session()->put('sid',$user->id);
            session()->put('user_id',$user->id);
            session()->put('user_by','seller');
            session()->put('root','seller');
            session()->put('user_name',$user->first_name);
            return redirect('seller/dashboard');
        }
    }
    function registration(Request $req)
    {
        

        $arr=$req->all();
        unset($arr['_token']);
        unset($arr['confirm']);
        $insert=DB::table('seller_user')->insert($arr);
        return back()->with('success',"Your registration successfully. Login and sell on market-place");
    }
}
