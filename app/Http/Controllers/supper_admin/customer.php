<?php

namespace App\Http\Controllers\supper_admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;
use File;
class customer extends Controller
{
    
    function index($action , $id=null)
    {
        if($action=='list')
        { 
            $data=DB::select("SELECT * FROM customer");
            return view('supper_admin/su_customer_list')->with('title','Customer List')->with('data',$data);
        } 
        if($action=='edit')
        { 
            $data=DB::select("SELECT * FROM product WHERE id='".$id."'");
            $thumb=DB::select("SELECT * FROM product_thumbnail WHERE product_id='".$id."'");
            $color_variente=DB::select("SELECT * FROM color_variante WHERE product_id='".$id."'");
            return view('pannel/form/second_form_edit')->with('title','Product Edit')->with('data',$data)->with('thumb',$thumb)->with('color_variente',$color_variente);
        }
        if($action=='view')
        { 

            $order_data=DB::select("SELECT order_item.*,orders.master_order_number FROM orders LEFT JOIN order_item ON order_item.master_order_number=orders.master_order_number WHERE orders.customer_id='".$id."'");
            $data=DB::select("SELECT * FROM customer WHERE id='".$id."'");
            return view('supper_admin/su_customer_view')->with('title','View Customer')->with('data',$data)->with('order_data',$order_data);
        }
        if($action=='order_view')
        { 
            $data=DB::select("SELECT c.* , o.* FROM orders o INNER JOIN customer c on c.id=o.customer_id WHERE o.unique_no='".$id."'");
            $ship_data=DB::table('shiping_address')->where('order_number',$id)->get();

            $cdata=DB::select("SELECT p.image,p.brand,p.title, oi.* FROM order_item oi INNER JOIN product p on p.id=oi.product_id WHERE oi.order_unique_no='".$id."'");
            $data1=array(
                'odata'=>$data,
                'ship_data' => $ship_data,
                'cdata' =>$cdata
            );
            return View('supper_admin/su_order_view',$data1)->with('title','Order Summary');
            
        }
        
        
    }

    
    
   
    
    
    
}
