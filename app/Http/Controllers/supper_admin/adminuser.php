<?php

namespace App\Http\Controllers\supper_admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use Storage;
use Session;
class adminuser extends Controller
{
    function index($action , $id=null)
    {
        if($action=='form')
        { 
            
            return view('supper_admin/admin_form')->with('title','Create Admin');
        }
        if($action=='edit')
        { 
            $user=DB::table('admin_user')->where('id',$id)->first();
            return view('supper_admin/admin_edit')->with('title','Edit Admin')->with('data',$user);
        } 
        if($action=='list')
        { 
            $user=DB::table('admin_user')->where('deleted_status',0)->get();
            return view('supper_admin/admin_list')->with('title','Admin List')->with('data',$user);
        } 
        if($action=='delete')
        { 
            $deleted_at=date('Y-m-d H:i:s');
            $deleted_id=Session::get('user_id');
            $deleted_by=Session::get('user_by');
            $deleted_status=1;
            $data=DB::select("UPDATE admin_user SET deleted_at='$deleted_at', deleted_id='$deleted_id', deleted_by='$deleted_by', deleted_status='$deleted_status' WHERE id='$id'");
            return back()->with('success',"admin user deleted successfully.");
        }
        
    }
    function admin_save(Request $req)
    {
            $_POST=$req->all();
            unset($_POST['_token']);
            $_POST['created_at']=date('Y-m-d H:i:s');
            $_POST['created_id']=Session::get('user_id');
            $_POST['created_by']=Session::get('user_by');
            $req->validate([
                'a_image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'email'  => 'required||email||unique:admin_user',
                'name'=> 'required||min:3',
                'password'=> 'required||min:6||max:16'
            ],
            [
                'email.unique' => 'The email address has already taken.'
                
            ]);
            if($req->has('a_image'))
            {
                $imageName = time().'.'.$req->a_image->extension();
                $_POST['image']=$imageName;
                
                $req->a_image->move(public_path('admin_image'), $imageName);
            }  
            unset($_POST['a_image']);
            
           // $emp->emp_photo=$imageName;
            $insert=DB::table('admin_user')->insert([$_POST]);
            if($insert)
            {
                return redirect('supper_admin/admin/form')->with('success',"New admin user created successfully.");
            }
    }
    function admin_update(Request $req)
    {
            $_POST=$req->all();
            unset($_POST['_token']);
            $_POST['updated_at']=date('Y-m-d H:i:s');
            $_POST['updated_id']=Session::get('user_id');
            $_POST['updated_by']=Session::get('user_by');
            $req->validate([
                'a_image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'email'  => 'required||email||unique:admin_user,id,'.$req->aid,
                'name'=> 'required||min:3',
                'password'=> 'required||min:6||max:16'
            ],
            [
                'email.unique' => 'The email address has already taken.'
                
            ]);
            if($req->has('a_image'))
            {
                $imageName = time().'.'.$req->a_image->extension();
                $_POST['image']=$imageName;
                if($req->a_image->move(public_path('admin_image'), $imageName))
                {
                    if(file_exists("admin_image/".$req->oldimage) && !empty($req->oldimage)){
                        unlink("admin_image/".$req->oldimage);
                    }
                }
            }  
            unset($_POST['a_image']);
            unset($_POST['oldimage']);
            unset($_POST['aid']);
           // $emp->emp_photo=$imageName;
            $insert=DB::table('admin_user')->where('id',$req->aid)->update($_POST);
            if($insert)
            {
                return redirect('supper_admin/admin/edit/'.$req->aid)->with('success',"Update admin user successfully.");
            }
    }
    
}
