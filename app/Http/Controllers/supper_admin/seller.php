<?php

namespace App\Http\Controllers\supper_admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;
use File;
class seller extends Controller
{
    
    function index($action , $id=null)
    {
        if($action=='list')
        { 
            $data=DB::select("SELECT * FROM seller_user");
            return view('supper_admin/su_seller_list')->with('title','Seller/Vendor List')->with('data',$data);
        } 
        if($action=='edit')
        { 
            $data=DB::select("SELECT * FROM product WHERE id='".$id."'");
            $thumb=DB::select("SELECT * FROM product_thumbnail WHERE product_id='".$id."'");
            $color_variente=DB::select("SELECT * FROM color_variante WHERE product_id='".$id."'");
            return view('pannel/form/second_form_edit')->with('title','Product Edit')->with('data',$data)->with('thumb',$thumb)->with('color_variente',$color_variente);
        }
        if($action=='view')
        { 

            $data=DB::select("SELECT * FROM seller_user WHERE id='".$id."'");
            $selleritem=DB::select("SELECT * FROM product WHERE created_id='".$id."' AND created_by ='seller'");
            return view('supper_admin/su_seller_view')->with('action',$action)->with('title','View Seller')->with('data',$data)->with('seller_item',$selleritem);
        }
        if($action=='delete')
        { 
            $deleted_at=date('Y-m-d H:i:s');
            $deleted_id=Session::get('user_id');
            $deleted_by=Session::get('user_by');
            $deleted_status=1;
            $data=DB::select("UPDATE product SET deleted_at='$deleted_at', deleted_id='$deleted_id', deleted_by='$deleted_by', deleted_status='$deleted_status' WHERE id='$id'");
            return back()->with('success',"product deleted successfully.");
        }
        
        
    }

    
    
   
    
    
    
}
