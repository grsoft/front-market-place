<?php

namespace App\Http\Controllers\supper_admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\supper_admin\admins;
class adminLogin extends Controller
{
    function adminLogin(Request $req)
    {
        //return $req->input();
        $data=admins::where(['username'=>$req->username,'password'=>$req->password])->first();
        if(empty($data))
        {
            return "Username or Password is Wrong.....";
        }
        else
        {
            $req->session()->put('id',$data->id);
            $req->session()->put('user_id',$data->id);
            $req->session()->put('user_by','supperadmin');
            session()->put('root','supper_admin');
            return redirect('supper_admin/dashboard');
        }
    }
}
