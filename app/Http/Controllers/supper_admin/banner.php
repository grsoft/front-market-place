<?php

namespace App\Http\Controllers\supper_admin;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Session;
class banner extends Controller
{
    function index($action , $id=null)
    {
        if($action=='home-banner-form')
        { 
            
            return view('supper_admin/homebannerform')->with('title','Add Banner Image');
        }
        if($action=='home-banner-edit')
        { 
            $data=DB::table('home_banner')->where('id',$id)->first();
            return view('supper_admin/homebanneredit')->with('title','Edit Banner Image')->with('data',$data);
        } 
        if($action=='home-banner-list')
        { 
            $data=DB::table('home_banner')->where('deleted_status',0)->get();
            return view('supper_admin/homebannerlist')->with('title','Banner List')->with('data',$data);
        } 
        if($action=='home-banner-delete')
        { 
            $deleted_at=date('Y-m-d H:i:s');
            $deleted_id=Session::get('user_id');
            $deleted_by=Session::get('user_by');
            $deleted_status=1;
            $data=DB::select("UPDATE home_banner SET deleted_at='$deleted_at', deleted_id='$deleted_id', deleted_by='$deleted_by', deleted_status='$deleted_status' WHERE id='$id'");
            return back()->with('success',"Image deleted successfully.");
        }
        
        
    }
    function home_banner_save(Request $req)
    {
            $_POST=$req->all();
            unset($_POST['_token']);
            $_POST['created_at']=date('Y-m-d H:i:s');
            $_POST['created_id']=Session::get('user_id');
            $_POST['created_by']=Session::get('user_by');
            $req->validate([
                'a_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'title'  => 'required||min:2',
                'url'=> 'required||url'
            ]);
            if($req->has('a_image'))
            {
                $imageName = time().'.'.$req->a_image->extension();
                $_POST['image']=$imageName;
                
                $req->a_image->move(public_path('home_banner'), $imageName);
            }  
            unset($_POST['a_image']);
            
            // $emp->emp_photo=$imageName;
            $insert=DB::table('home_banner')->insert([$_POST]);
            if($insert)
            {
                return redirect('/supper_admin/banner/home-banner-form')->with('success',"Home banner image add successfully.");
            }
    }
    function home_banner_update(Request $req)
    {
            $_POST=$req->all();
            unset($_POST['_token']);
            unset($_POST['iid']);
            $_POST['updated_at']=date('Y-m-d H:i:s');
            $_POST['updated_id']=Session::get('user_id');
            $_POST['updated_by']=Session::get('user_by');
            $req->validate([
                'a_image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'title'  => 'required||min:2',
                'url'=> 'required||url'
            ]);
            if($req->has('a_image'))
            {
                $imageName = time().'.'.$req->a_image->extension();
                $_POST['image']=$imageName;
                
                $req->a_image->move(public_path('home_banner'), $imageName);
            }  
            unset($_POST['a_image']);
            
            // $emp->emp_photo=$imageName;
            $update=DB::table('home_banner')->where('id',$req->iid)->update($_POST);
            if($update)
            {
                return redirect('/supper_admin/banner/home-banner-edit/'.$req->iid)->with('success',"Home banner image update successfully.");
            }
    }
}
