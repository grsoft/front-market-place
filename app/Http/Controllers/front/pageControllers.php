<?php

namespace App\Http\Controllers\front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\front\cartControllers;
use Session;
use Route;
class pageControllers extends Controller
{
    function index(Request $req)
    {
    	$is_api_request = $req->route()->getPrefix() === 'api';
    	$allcate=$this->getCatMenu();
    	if($is_api_request==1)
    	{
    		return json_encode($allcate);
    	}
    	else
    	{
    		return view('front/index')->with('catemenu',$allcate)->with('title','Home');
    	}
        
    }
    function checkout_page(Request $req)
    {
        $cust_data=DB::table('customer')->where('id',session()->get('cid'))->get();
        $allcate=$this->getCatMenu();
        $cc=new cartControllers();
        $cdata=$cc->index();
        return view('front/checkout')->with('catemenu',$allcate)->with('cust_data',$cust_data)->with('cart',$cdata)->with('page', 'Chekout');
    }
    
    function shop_login_page(Request $req)
    {
        session()->put('cartthru',"process");
        $allcate=$this->getCatMenu();
        return view('front/shop-login')->with('catemenu',$allcate)->with('page', 'Login & Register');
    }
    function filter_left(Request $req)
    {
        $arr=explode("-", $req->mrp_range);
		if(!empty($req->mrp_range) && !empty($req->brand))
		{
			$data=DB::table('product')
			->where('brand',$req->brand)
			->where('category_id',$req->cat_id)
			->where('sell_price','>',$arr[0])
			->where('sell_price','<',$arr[1])
			->where('verify_status',1)
			->where('status',1)
			->get();
			
		}
        else if(!empty($req->mrp_range)){
			$data=DB::table('product')
			->where('category_id',$req->cat_id)
			->where('sell_price','>',$arr[0])
			->where('sell_price','<',$arr[1])
			->where('verify_status',1)
			->where('status',1)
			->get();
			
		}
		else{
			$data=DB::table('product')
			->where('category_id',$req->cat_id)
			->where('brand',$req->brand)
			->where('verify_status',1)
			->where('status',1)
			->get();
			//$data=DB::select("SELECT * FROM product WHERE brand ='".$req->brand."'  AND verify_status='1' AND status='1'");
		}
        //$data=DB::select("SELECT * FROM product WHERE CONTAINS (title, '$req->title' ) AND verify_status='1' AND status='1'");
        return $data;
    }
	function shop_search_page(Request $req)
    {
        $allcate=$this->getCatMenu();
        $arr=explode(" ", $req->title);
        $data=DB::select("SELECT product.* FROM product INNER JOIN master_category ON master_category.childe_category_id=product.category_id WHERE product.title LIKE '%$req->title%' or product.brand LIKE '%$req->title%' or master_category.title LIKE '%$req->title%' AND master_category.childe_category_id = master_category.category_id AND product.verify_status='1' AND product.status='1'");
        //$data=DB::select("SELECT * FROM product WHERE CONTAINS (title, '$req->title' ) AND verify_status='1' AND status='1'");
        return view('front/search')->with('product',$data)->with('catemenu',$allcate)->with('page', 'Search');
    }
    function shop_register_page(Request $req)
    {
        $allcate=$this->getCatMenu();
        return view('front/shop-register')->with('catemenu',$allcate)->with('page', 'New Register');
    }
    function orderSummary($order_number)
    {
        $allcate=$this->getCatMenu();
        $data=DB::select("SELECT c.* , o.* FROM orders o INNER JOIN customer c on c.id=o.customer_id WHERE o.unique_no='".$order_number."'");
        $ship_data=DB::table('shiping_address')->where('order_number',$order_number)->get();

        $cdata=DB::select("SELECT p.image,p.brand,p.title, oi.* FROM order_item oi INNER JOIN product p on p.id=oi.product_id WHERE oi.master_order_number='".$order_number."'");
        return view('front/ordersummary')->with('catemenu',$allcate)->with('o_data',$data)->with('cdata',$cdata)->with('ship_data',$ship_data)->with('page', 'Order Summary');
    }
    function orderList()
    {
        $allcate=$this->getCatMenu();
        $cdata=DB::select("SELECT o.order_status,o.order_status_resion, p.image,p.brand,p.title, oi.* FROM order_item oi INNER JOIN orders o on o.unique_no=oi.order_unique_no INNER JOIN product p on p.id=oi.product_id WHERE oi.customer_id='".session()->get('cid')."'");
        return view('front/order_list_checkout')->with('catemenu',$allcate)->with('cdata',$cdata)->with('page', 'Order List Chekout');
    }
    function shoping_cart(Request $req)
    {
        $allcate=$this->getCatMenu();
        $cc=new cartControllers();
        $cdata=$cc->index();
        return view('front/order')->with('catemenu',$allcate)->with('cart',$cdata)->with('page', 'Order');
    }
    function detailspage(Request $req, $id)
    {
    	$is_api_request = $req->route()->getPrefix() === 'api';
    	$allcate=$this->getCatMenu();
    	$data=DB::select("SELECT su.id as sid, p.* FROM product p INNER JOIN seller_user su on p.created_id=su.id WHERE p.id='$id'");
    	if(empty($data)){return "";}
    	$thumb=DB::select("SELECT * FROM product_thumbnail WHERE product_id='".$data[0]->id."'");
    	$related=DB::select("SELECT * FROM product WHERE parent_id='".$data[0]->parent_id."'");
    	//home root
        
        $hr=DB::select("SELECT indicate_root FROM product WHERE id='$id'");
        if(count($hr)>0)
        {
	        $tree=explode(',',$hr[0]->indicate_root);
	        $hometree=array();
	        foreach ($tree as $key => $value) {
	        	$hr1=DB::select("SELECT title FROM master_category WHERE id='$value'");
	        	if(count($hr1)>0){
	        		$hometree[]=$hr1[0]->title;
	        	}
	        }
	        
	    }
	    else{$hometree[]=$category;}
        //end home root
    	if($is_api_request==1)
    	{
    		return json_encode($allcate);
    	}
    	else
    	{
    		return view('front/detail')->with('catemenu',$allcate)->with('title','Product details')->with('hometree',$hometree)->with('thumb',$thumb)->with('data',$data)->with('related',$related);
    	}
    }
    function seller_page(Request $req)
    {
    	$is_api_request = $req->route()->getPrefix() === 'api';
    	$allcate=$this->getCatMenu();
    	if($is_api_request==1)
    	{
    		return json_encode($allcate);
    	}
    	else
    	{
    		return view('front/seller_account')->with('catemenu',$allcate)->with('title','Seller');
    	}
    }
    function categorypage1(Request $req,$catname,$scatname1, $scatname2 , $cid=null)
    { 	
    	$path= request()->path();
    	$path=explode('/', $path);
    	$is_api_request = $req->route()->getPrefix() === 'api';
    	if(count($path)==4){
    		$data=array(
	    		'api' => $is_api_request,
	    		'catname' => $path[2],
	    		'scatname1' => "",
	    		'scatname2' => "",
	    		'cid' => $path[3]
	    	);
	    	return $this->categorypage($data);
    	}
    	if(count($path)==5){
    		$data=array(
	    		'api' => $is_api_request,
	    		'catname' => $path[2],
	    		'scatname1' => $path[3],
	    		'scatname2' => "",
	    		'cid' => $path[4]
	    	);
	    	return $this->categorypage($data);
    	}
    	if(count($path)==6){
    		$data=array(
	    		'api' => $is_api_request,
	    		'catname' => $path[2],
	    		'scatname1' => $path[3],
	    		'scatname2' => $path[4],
	    		'cid' => $path[5]
	    	);
	    	return $this->categorypage($data);
    	}
    	
    	
    }
    
    function categorypage($data)
    {
    	
    	//$is_api_request = $req->route()->getPrefix() === 'api';
    	$is_api_request=$data['api'];
    	$catname=$data['catname'];
    	$scatname1=$data['scatname1'];
    	$scatname2=$data['scatname2'];
    	$cid=$data['cid'];
    	$allcate=$this->getCatMenu();
    	//left cat data
    	$parent=array();$leftcat=array();
        foreach($allcate[1][$catname] as $ssvalue){
          $parent[]=$ssvalue->title1;
        }
        $parent=array_values(array_unique($parent));
        for($i=count($parent)-1;$i>0 ;$i--){
        	
        	foreach($allcate[1][$catname] as $svalue){$leftcatsub=array();
        		if($catname==$parent[$i]) 
                {
                  $rout="/".$parent[$i]."/".$svalue->title2."/".$svalue->id;
                }
                else{
                  $rout="/".$catname."/".$parent[$i]."/".$svalue->title2."/".$svalue->id;
                }
                if($parent[$i] === $svalue->title1)
                {
                	$leftcatsub[1]=$rout;
                	$leftcatsub[2]=$svalue->title2;
                	$leftcat[$parent[$i]][]=$leftcatsub;
                }
                
        	}
        }
        //end left cat data
        //home root
        
        $hr=DB::select("SELECT title,tree_category_id FROM master_category WHERE id='$cid'");
        if(count($hr)>0)
        {
	        $tree=explode(',',$hr[0]->tree_category_id);
	        $hometree=array();
            $hometree_url=array();
            $mainfirst=true;
	        foreach ($tree as $key => $value) {

	        	$hr1=DB::select("SELECT id, childe_category_id, title FROM master_category WHERE id='$value'");
	        	if(count($hr1)>0){
                    if($key==0)
                    {
                        $hometree_url[]=$hr1[0]->title.'/'.$hr1[0]->id;
                        $hometree[]=$hr1[0]->title;
                    }
                    else{
                        $hometree_url[]=$catname.'/'.$hr1[0]->title.'/'.$hr1[0]->id;
                        $hometree[]=$hr1[0]->title;
                    }
                        
                        
                    
	        		
	        	}
	        }
	        $hometree[]=$hr[0]->title;
            $hometree_url[]=$catname.'/'.$hr[0]->title.'/'.$cid;
	    }
	    else{$hometree[]=$category;}
        //end home root
        //get product
	    $prod=DB::select("SELECT * FROM `product` WHERE FIND_IN_SET('$cid',indicate_root) AND verify_status='1' AND status='1'");
	    //end get product
    	if($is_api_request==1)
    	{
    		return json_encode($allcate);
    	}
    	else
    	{
    		
    		return view('front/category')->with('catemenu',$allcate)->with('title','Category')->with('catname',$catname)->with('leftcat',$leftcat)->with('hometree',$hometree)->with('hometree_url',$hometree_url)->with('product',$prod)->with('catid',$cid);
    	}
    }
    function getCatMenu()
    {
    	$cdata=DB::table('master_category')->where('childe_category_id',0)->where('deleted_status',0)->get();
    	
    	/*foreach ($cdata as $key => $value) {
    		$subdata=DB::select("SELECT * FROM master_category WHERE category_id='".$value->id."' AND deleted_status=0 ");
    		
    		foreach ($subdata as $key => $svalue) {
    			$sid=$svalue->id;
    			$loop=1;
    			while ($loop ==1) {
    				$slast=$this->lastmenu($sid);
    				if(count($slast)>0){$loop=1; $sid=$slast[0]->id;}
    				else{ 
    					$loop=0; 
    					$subdata1=DB::table('master_category')->where('childe_category_id',$sid)->where('deleted_status',0)->get();
    					$arr[]=$subdata1; 
    				}
    			}
	    		
    		}
    		
    	}*/
    	$menu=[];
    	$menu[0]=$cdata;
    	foreach ($cdata as $key => $value) {
    			
    		$subdata=DB::select("SELECT B.id,A.title AS title1, B.title AS title2, A.childe_category_id,A.category_id FROM master_category A, master_category B WHERE A.id = B.childe_category_id AND B.category_id='$value->id' GROUP by B.id ORDER BY B.childe_category_id DESC");
	    	$menu[1][$value->title]=$subdata;
    		
    	}
    	
    	
    	
    	return $menu;
    }
    function lastmenu($id)
    {
    	$subdata1=DB::table('master_category')->where('childe_category_id',$id)->where('deleted_status',0)->get();
    	return $subdata1;
    }
	function addtorating(Request $req)
    {

    	$find=DB::table('rating')->where('customer_id',session()->get('cid'))->where('product_id',$req->prod_id)->get();
		if($find->count()==0)
		{
			DB::table('rating')->insert([
				'customer_id' => session()->get('cid'),
				'product_id' => $req->prod_id,
				'rating' => $req->rating,
				'comment' => $req->comment,
				'created_at' => date('Y-m-d H:i:s')
			]);
			return "add";
		}
		else{
			DB::table('rating')->where('customer_id',session()->get('cid'))->where('product_id',$req->prod_id)->update([
				'rating' => $req->rating,
				'comment' => $req->comment,
				'updated_at' => date('Y-m-d H:i:s')
			]);
			return "update";
		}
    	
    }
}
