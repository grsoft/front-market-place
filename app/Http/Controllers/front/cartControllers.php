<?php

namespace App\Http\Controllers\front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cookie;
use Session;
class cartControllers extends Controller
{
	public function index()
    {
        if(session()->has('cid'))
        {
             $cart_data=DB::table('cart')->where('cust_id',session()->has('cid'))->get(); 
             $cart_data = json_decode($cart_data, true);
             return $cart_data;
        }
        else
        {
            $cookie_data = stripslashes(Cookie::get('shopping_cart'));
            $cart_data = json_decode($cookie_data, true);
            return $cart_data;
        }
        
    }
    public function loginTimeAddToCart()
    {
        $data = stripslashes(Cookie::get('shopping_cart'));
        if(session()->has('cid'))
        {
            if(!empty($data)){
                $data = array_values(array_filter(json_decode($data, true)));
               $item_id_list = array_column($data, 'item_id');
            
            foreach ($item_id_list as $key => $value) {
                $row_data=DB::table('cart')->where('item_id',$value)->where('cust_id',session()->get('cid'))->get();
                if(count($row_data)>0)
                {
                    DB::select("UPDATE cart SET item_quantity='".$data[$key]['item_quantity']."' WHERE id='".$row_data[0]->id."'");
                }
                else
                {
                    DB::select("INSERT INTO cart (seller_id,cust_id,item_id,item_name,item_quantity,item_mrp,item_disc_perc,item_price,item_image) VALUES('".$data[$key]['seller_id']."','".session()->get('cid')."','".$data[$key]['item_id']."','".$data[$key]['item_name']."','".$data[$key]['item_quantity']."','".$data[$key]['item_mrp']."','".$data[$key]['item_disc_perc']."','".$data[$key]['item_price']."','".$data[$key]['item_image']."')");
                }
            }
            }
            Cookie::queue(Cookie::forget('shopping_cart'));
            $data=DB::table('cart')->where('cust_id',session()->get('cid'))->get();
            $minutes = 86400 * 90;
            Cookie::queue(Cookie::make('shopping_cart', $data, $minutes));
        }

        

    }
    public function databaseDeleteCart($prod_id)
    {
        if(session()->has('cid'))
        {
             DB::select("DELETE FROM cart WHERE item_id='".$prod_id."' AND cust_id='".session()->get('cid')."'"); 
        }
    }
    public function databaseDeleteCartAll()
    {
        if(session()->has('cid'))
        {
             DB::select("DELETE FROM cart WHERE cust_id='".session()->get('cid')."'"); 
        }
    }
    public function databaseCart($data)
    {
        $item_id_list = array_column($data, 'item_id');
        if(session()->has('cid'))
        {
            foreach ($item_id_list as $key => $value) {
                $row_data=DB::table('cart')->where('item_id',$value)->where('cust_id',session()->get('cid'))->get();
                if(count($row_data)>0)
                {
                    DB::select("UPDATE cart SET item_quantity='".$data[$key]['item_quantity']."' WHERE id='".$row_data[0]->id."'");
                }
                else
                {
                    DB::select("INSERT INTO cart (seller_id,cust_id,item_id,item_name,item_quantity,item_mrp,item_disc_perc,item_price,item_image) VALUES('".$data[$key]['seller_id']."','".session()->get('cid')."','".$data[$key]['item_id']."','".$data[$key]['item_name']."','".$data[$key]['item_quantity']."','".$data[$key]['item_mrp']."','".$data[$key]['item_disc_perc']."','".$data[$key]['item_price']."','".$data[$key]['item_image']."')");
                }
            }
            

        }
        

    }
    public function addtocart(Request $request)
    {

        $prod_id = $request->input('product_id');
        $quantity = $request->input('quantity');
        if(Cookie::get('shopping_cart'))
        {
            $cookie_data = stripslashes(Cookie::get('shopping_cart'));
            $cart_data = json_decode($cookie_data, true);
        }
        else
        {
            $cart_data = array();
        }

        $item_id_list = array_column($cart_data, 'item_id');
        $prod_id_is_there = $prod_id;

        if(in_array($prod_id_is_there, $item_id_list))
        {
            foreach($cart_data as $keys => $values)
            {
                if($cart_data[$keys]["item_id"] == $prod_id)
                {
                    $cart_data[$keys]["item_quantity"] = $request->input('quantity');
                    $item_data = json_encode($cart_data);
                    $minutes = 86400 * 90;
                    Cookie::queue(Cookie::make('shopping_cart', $item_data, $minutes));
                    $this->databaseCart($cart_data);
                    return response()->json(['status'=>'"'.$cart_data[$keys]["item_name"].'" Already Added to Cart','status2'=>'2']);
                }
            }
        }
        else
        {
            $products = DB::select("SELECT * FROM product WHERE id='".$prod_id."'");
            $seller_id = $products[0]->created_id;
            $prod_name = $products[0]->title;
            $prod_image = $products[0]->image;
            $prod_mrp = $products[0]->mrp;
            $prod_disc_perc = $products[0]->discount;
            $priceval = $products[0]->sell_price;

            if($products)
            {
                $item_array = array(
                    'seller_id' => $seller_id,
                    'item_id' => $prod_id,
                    'item_name' => $prod_name,
                    'item_quantity' => $quantity,
                    'item_mrp' => $prod_mrp,
                    'item_disc_perc' => $prod_disc_perc,
                    'item_price' => $priceval,
                    'item_image' => $prod_image
                );
                $cart_data[] = $item_array;

                $item_data = json_encode($cart_data);
                $minutes = 86400 * 90;
                Cookie::queue(Cookie::make('shopping_cart', $item_data, $minutes));
                $this->databaseCart($cart_data);
                return response()->json(['status'=>'"'.$prod_name.'" Added to Cart']);
            }
        }

    }
    public function cartloadbyajax()
    {
        $cart_item="";$total=0;
        if(Cookie::get('shopping_cart'))
        {
            $cookie_data = stripslashes(Cookie::get('shopping_cart'));
            $cart_data = json_decode($cookie_data, true);
            $totalcart = count($cart_data);
            
            foreach ($cart_data as $key => $value) {
            
            $cart_item.='<li class="product-item"> <a class="product-item-photo" href="#" title="'.$value['item_name'].'"> <img class="product-image-photo" src="'.url('/').'/product_image/'.$value['item_image'].'" alt="'.$value['item_name'].'"> </a>
                          <div class="product-item-details"> <strong class="product-item-name"> <a href="#">'.$value['item_name'].'</a> </strong>
                            <div class="product-item-price"> <span class="price">'.number_format($value['item_price'],2).'</span> </div>
                            <div class="product-item-qty"> <span class="label">Qty: </span ><span class="number">'.$value['item_quantity'].'</span> </div>
                            
                          </div>
                        </li>';
                $total+=$value['item_price']*$value['item_quantity'];
            }
            echo json_encode(array('totalcart' => $totalcart,'cart_item' => $cart_item,'total' => number_format($total,2))); die;
            return;
        }
        else
        {
            $totalcart = "0";
            echo json_encode(array('totalcart' => $totalcart,'cart_item' => $cart_item,'total' => number_format($total,2))); die;
            return;
        }
    }
    public function updatetocart(Request $request)
    {
        $prod_id = $request->input('product_id');
        $quantity = $request->input('quantity');

        if(Cookie::get('shopping_cart'))
        {
            $cookie_data = stripslashes(Cookie::get('shopping_cart'));
            $cart_data = json_decode($cookie_data, true);

            $item_id_list = array_column($cart_data, 'item_id');
            $prod_id_is_there = $prod_id;

            if(in_array($prod_id_is_there, $item_id_list))
            {
                foreach($cart_data as $keys => $values)
                {
                    if($cart_data[$keys]["item_id"] == $prod_id)
                    {
                        $cart_data[$keys]["item_quantity"] =  $quantity;
                        $item_data = json_encode($cart_data);
                        $minutes = 86400 * 90;
                        Cookie::queue(Cookie::make('shopping_cart', $item_data, $minutes));
                        $this->databaseCart($cart_data);
                        return response()->json(['status'=>'"'.$cart_data[$keys]["item_name"].'" Quantity Updated']);
                    }
                }
            }
        }
    }
    public function deletefromcart(Request $request)
    {
        $prod_id = $request->input('product_id');

        $cookie_data = stripslashes(Cookie::get('shopping_cart'));
        $cart_data = json_decode($cookie_data, true);

        $item_id_list = array_column($cart_data, 'item_id');
        $prod_id_is_there = $prod_id;

        if(in_array($prod_id_is_there, $item_id_list))
        {
            foreach($cart_data as $keys => $values)
            {
                if($cart_data[$keys]["item_id"] == $prod_id)
                {
                    unset($cart_data[$keys]);
                    $item_data = json_encode($cart_data);
                    $minutes = 86400 * 90;
                    Cookie::queue(Cookie::make('shopping_cart', $item_data, $minutes));
                    $this->databaseDeleteCart($prod_id);
                    return response()->json(['status'=>'Item Removed from Cart']);
                }
            }
        }
    }
    public function clearcart()
    {
        Cookie::queue(Cookie::forget('shopping_cart'));
        $this->databaseDeleteCartAll();
        return response()->json(['status'=>'Your Cart is Cleared']);
    }

    
}

//https://www.fundaofwebit.com/post/how-to-make-shopping-cart-using-cookie-in-laravel-with-ajax-request