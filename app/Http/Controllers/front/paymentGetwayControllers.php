<?php

namespace App\Http\Controllers\front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cookie;
use App\Http\Controllers\front\customerControllers;
use Route;
use Illuminate\Support\Facades\Session;
class paymentGetwayControllers extends Controller
{
    function paymentProcess()
    {
        
        //encryption key set in the Merchant Access Portal
        $returnurl="https://grsoftsolution.com/projects/marketplace-demo/public/payget-payment-response";
        
        $encryptionKey = 'secret';

        $DateTime = date('Y-m-d H:i:s');

        $data = array(
            'PAYGATE_ID'        => 10011072130,
            'REFERENCE'         => 'pgtest_123456789',
            'AMOUNT'            => 3299,
            'CURRENCY'          => 'ZAR',
            'RETURN_URL'        => $returnurl,
            'TRANSACTION_DATE'  => $DateTime,
            'LOCALE'            => 'en-za',
            'COUNTRY'           => 'ZAF',
            'EMAIL'             => 'jnu123mkg@gmail.com',
            
        );

        $checksum = md5(implode('', $data) . $encryptionKey);

        $data['CHECKSUM'] = $checksum;

        $fieldsString = http_build_query($data);

        //open connection
        $ch = curl_init();

        //set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, 'https://secure.paygate.co.za/payweb3/initiate.trans');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_NOBODY, false);
        curl_setopt($ch, CURLOPT_REFERER, $_SERVER['HTTP_HOST']);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fieldsString);

        //execute post
        $result = curl_exec($ch);
        
        //close connection
        curl_close($ch);
        parse_str($result,$result1);
        //print_r($result1);
        return $result1;
        
    }
    function paymentQuery($req)
    {
        //encryption key set in the Merchant Access Portal
        $encryptionKey = 'secret';

        $data = array(
            'PAYGATE_ID'        => 10011072130,
            'PAY_REQUEST_ID'    => $req['PAY_REQUEST_ID'],
            'REFERENCE'         => 'pgtest_123456789'
        );

        $checksum = md5(implode('', $data) . $encryptionKey);

        $data['CHECKSUM'] = $checksum;

        $fieldsString = http_build_query($data);

        //open connection
        $ch = curl_init();

        //set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, 'https://secure.paygate.co.za/payweb3/query.trans');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_NOBODY, false);
        curl_setopt($ch, CURLOPT_REFERER, $_SERVER['HTTP_HOST']);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fieldsString);

        //execute post
        $result = curl_exec($ch);
        parse_str($result,$result1);
        return $result1;
    }
    
    function paymentResponse(Request $req)
    { 
        $qd=$this->paymentQuery($req->all());
        
        //$order_data=DB::select("SELECT unique_no FROM orders WHERE payment_id='".$req->PAY_REQUEST_ID."'");
        if(!empty($qd))
        {
            $status="";
            if($qd['TRANSACTION_STATUS']==1){
                $status="Success";
            }
            elseif($qd['TRANSACTION_STATUS']==2){
                $status="Declined";
            }
            elseif($qd['TRANSACTION_STATUS']==4){
                $status="User Cancelled";
            }
            else{
                $status="Not Done";
            }
            
            
            if($status=="Success"){ 
                $shiping_data=session()->get('shiping_data');
                $cart_item=session()->get('cart_item');
                $order_data=session()->get('order_data');
                $order_data[0]['payment_id']=$qd['TRANSACTION_ID'];
                $order_data[0]['pay_status']=$status;
                //insert data shiping, cart & orders
                $inserts=DB::table('shiping_address')->insert($shiping_data);
                $insertc=DB::table('order_item')->insert($cart_item);
                $inserto=DB::table('orders')->insert($order_data);
                //end insert data cart & orders
                session()->forget('shiping_data');
                session()->forget('cart_item');
                session()->forget('order_data');
                $obj=new customerControllers();
                $mail=$obj->sentOrdeMail($cart_item);
                
                
                //delete cart data & cookies
                $deleted = DB::table('cart')->where('cust_id', '=', session()->get('cid'))->delete();
                Cookie::queue(Cookie::forget('shopping_cart'));
                //end delete cart data & cookies
                return redirect('/shop/order/'.$order_data[0]['unique_no'])->with("success","Order created successfully");
            }
        }
        //$update=DB::select("UPDATE orders SET pay_status='".$status."' WHERE payment_id='".$req->PAY_REQUEST_ID."'");
        
        
    }
    
}
