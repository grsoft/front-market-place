<?php

namespace App\Http\Controllers\front;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cookie;
use App\Http\Controllers\front\paymentGetwayControllers;
use App\Http\Controllers\front\cartControllers;
use Session;

class customerControllers extends Controller
{
    function customerLogOut(Request $req)
    {
        session()->flush();
        return redirect('/');
    }
    function customerLogin(Request $req)
    {
        //return $req->input();
        $user=DB::table('customer')->where(['email_address'=>$req->email_address])->first();
        $pass=DB::table('customer')->where(['password'=>$req->password])->first();
        if(empty($user))
        {
            return back()->with('error', "Username is Wrong.....");
        }
        elseif(empty($pass))
        {
            return back()->with('error', "Username is Wrong.....");
        }
        else
        {
            if(session()->has('cartthru')){
                $redirect='/shop/checkout';
            }
            else{    
            $redirect='/';
            }
            session()->flush() ;
            session()->put('cid',$user->id);
            session()->put('user_id',$user->id);
            session()->put('user_f_name',$user->first_name);
            session()->put('user_name',$user->first_name." ".$user->last_name);
            $cc=new cartControllers();
            $cdata=$cc->loginTimeAddToCart();
            return redirect($redirect);
            
        }
    }
    function cust_registration(Request $req)
    {
        $req->validate([
            'first_name' => 'required|min:2',
            'last_name' => 'required|min:2',
            'email_address' => 'required|email|unique:customer',
            'password' => 'required|min:2',
            'confirm' => 'required|min:2',
            'address' => 'required|min:2',
            'city' => 'required|min:2',
            'state' => 'required|min:2',
            'postal_code' => 'required|min:2',
            'country' => 'required|min:2',
            'mobile_no' => 'required|min:10|max:10',
            'first_name_s' => 'required|min:2',
            'last_name_s' => 'required|min:2',
            'email_address_s' => 'required|email',
            'address_s' => 'required|min:2',
            'city_s' => 'required|min:2',
            'state_s' => 'required|min:2',
            'postal_code_s' => 'required|min:2',
            'country_s' => 'required|min:2',
            'mobile_no_s' => 'required|min:10|max:10',

        ],
        [
            'email_address.unique' => 'The email address has already taken.'
            
        ]);
        
        $arr=array();
        $arr=$req->all();
        unset($arr['_token']);
        unset($arr['confirm']);
        $arr['created_at']=date('Y-m-d H:i:s');
        $insert=DB::table('customer')->insert($arr);
        $lastid=DB::getPdo()->lastInsertId();
        if(session()->has('cartthru')){
            //session()->flush('cartthru');
            session()->put('cid',$lastid);
            session()->put('user_id',$lastid);
            session()->put('user_name',$arr['first_name']." ".$arr['last_name']);
            
            return redirect('/shop/checkout');
        }
        else{
            return back()->with('success',"Your registration successfully.");
        }
        
    }
    function create_order(Request $req)
    {
        $cookie_data = stripslashes(Cookie::get('shopping_cart'));
        //$cart_data = json_decode($cookie_data, true);
        $cart_data = array_values(array_filter(json_decode($cookie_data, true)));
        $master_order_number=time()+session()->get('cid');
        $customer_id=session()->get('cid');
        $shiping_data=array();
        $cart_item=array();
        $order_data=array();
        $date_time=date('Y-m-d H:i:s');
        $total=0;
        $index=0;
        
        foreach ($cart_data as $key => $value) {
            $order_number=time()+session()->get('cid')+$key;
            //create cart item in array
            $cart_item[$key]['master_order_number']=$master_order_number;
            $cart_item[$key]['seller_id']=$cart_data[$key]['seller_id'];
            $cart_item[$key]['product_id']=$cart_data[$key]['item_id'];
            $cart_item[$key]['quantity']=$cart_data[$key]['item_quantity'];
            $cart_item[$key]['item_mrp']=$cart_data[$key]['item_mrp'];
            $cart_item[$key]['item_discount']=$cart_data[$key]['item_disc_perc'];
            $cart_item[$key]['amount']=$cart_data[$key]['item_price'];
            $cart_item[$key]['order_unique_no']=$order_number;
            $cart_item[$key]['customer_id']=$customer_id;
            $cart_item[$key]['created_at']=$date_time;
            $total+=$cart_data[$key]['item_price'];
            //end create cart item array
            //create order array data
            $order_data[$key]['master_order_number']=$master_order_number;
            $order_data[$key]['order_date']=$date_time;
            $order_data[$key]['unique_no']=$order_number;
            $order_data[$key]['choose_payment']=$req->pay_method;
            $order_data[$key]['total']=$total;
            $order_data[$key]['grand_total']=$total;
            $order_data[$key]['customer_id']=$customer_id;
            $order_data[$key]['pay_status']="PLACE ORDER";
            //end create order array data
            //create delivery data
            $shiping_data[$key]['order_number']=$order_number;
            $shiping_data[$key]['first_name_s']=$req->first_name_s;
            $shiping_data[$key]['last_name_s']=$req->last_name_s;
            $shiping_data[$key]['email_address_s']=$req->email_address_s;
            $shiping_data[$key]['address_s']=$req->address_s;
            $shiping_data[$key]['city_s']=$req->city_s;
            $shiping_data[$key]['state_s']=$req->state_s;
            $shiping_data[$key]['postal_code_s']=$req->postal_code_s;
            $shiping_data[$key]['country_s']=$req->country_s;
            $shiping_data[$key]['mobile_no_s']=$req->mobile_no_s;
            $shiping_data[$key]['created_at']=$date_time;
            
            //end delivery data 
        }
        
            if($req->pay_method=='cod')
            {
                //insert data shiping, cart & orders
                $inserts=DB::table('shiping_address')->insert($shiping_data);
                $insertc=DB::table('order_item')->insert($cart_item);
                $inserto=DB::table('orders')->insert($order_data);
                //end insert data cart & orders

                //$this->sentOrdeMail($cart_item);
                
                //delete cart data & cookies
                $deleted = DB::table('cart')->where('cust_id', '=', session()->get('cid'))->delete();
                Cookie::queue(Cookie::forget('shopping_cart'));
                //end delete cart data & cookies
                return redirect('/shop/order/'.$master_order_number)->with("success","Order created successfully");
            }
            else{
                //order keep in session
                Session::put('shiping_data',$shiping_data);
                Session::put('cart_item',$cart_item);
                Session::put('order_data',$order_data);
                //end order keep in session
                $cc=new paymentGetwayControllers();
                $pay_initial=$cc->paymentProcess();
                if(!empty($pay_initial['PAY_REQUEST_ID'])>0){
                    //$update=DB::select("UPDATE orders SET payment_id='".$pay_initial['PAY_REQUEST_ID']."' WHERE unique_no='".$order_number."'");
                    return view('front/payget_payment_form')->with("option",$pay_initial);
                }
            }
        
    }
    public function sentOrdeMail($cart_item)
    {
        foreach ($cart_item as $key => $value) {
            $vendor=DB::table('seller_user')->select('email_address','shop_name')->where('id',$cart_item[$key]['seller_id'])->first();
            $vendor->email_address;
            $cust=DB::table('customer')->select('email_address','first_name')->where('id',$cart_item[$key]['customer_id'])->first();

            $cust->email_address;
            $prod=DB::table('product')->select('image','title')->where('id',$cart_item[$key]['product_id'])->first();
            $total=$cart_item[$key]['quantity']*$cart_item[$key]['amount'];
            $url= asset('product_image');
            $p_item='<table style="width:100%; border-collapse: collapse;">
                    <thead style="background:orange;">
                        <tr >
                            <th>Image</th>
                            <th>Product Name</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th>Grandtotal</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                                    
                                    
                    <tr>
                        <td style="text-align:center;border: 0.5px solid black;">
                            <img alt="Image" width="70px" src="'.$url.'/'.$prod->image.'">
                            
                        </td>
                        <td style="text-align:center;border: 0.5px solid black;">
                            <h4>
                                <a href="#">'. $prod->title.'</a>
                            </h4>
                        </td>
                        <td style="text-align:center;border: 0.5px solid black;">
                            <span> '.$cart_item[$key]['amount'].'</span>
                        </td>
                        <td style="text-align:center;border: 0.5px solid black;">
                            '.$cart_item[$key]['quantity'].'                                                    
                        </td>
                        <td style="text-align:center;border: 0.5px solid black;">
                            <span>'.$total.'</span>
                        </td>
                    </tr>
                                    
                    </tbody>
                </table>';
                $subject_text="Order details";
                $message_text="<p>Dear ".$cust->first_name."</p><p> Your order is ".$cart_item[$key]['order_unique_no']." has been accepted </p>";
                $message_text.=$p_item;
                $to="jnu123mkg@gmail.com";
                $cc="";
                $bcc="";
                    $subject=$subject_text;
                    $message=$message_text;
                    $from="jnu123mkg@gmail.com";
                    $headers=[
                        "MIME-Version: 1.0",
                        "Content-type:text/html;charset=iso-8859-1",
                        "From: ".$from,
                        "Cc: ".$cc,
                        "Bcc: ".$bcc
                            ];
                    $headers=implode("\r\n",$headers);
                    
                    if(mail($to, $subject,$message, $headers ))
                    {
                        return "Mail has been successfully sent (no errors anyway)";
                    }
        }
        /*$to="jnu123mkg@gmail.com";
        $cc="";
        $bcc="";
            $subject="Test Mail";
            $message="Hello! This is test mail from " . $_SERVER['SERVER_NAME'];
            $from="mukesh@grsoftsolution.com";
            $headers=[
                "MIME-Version: 1.0",
                "Content-type:text/html;charset=utf-8",
                "From: ".$from,
                "Cc: ".$cc,
                "Bcc: ".$bcc
                    ];
            $headers=implode("\r\n",$headers);
            
            if(mail($to, $subject,$message, $headers ))
            {
                print "Mail has been successfully sent (no errors anyway)";
            }*/
    }
    
}
