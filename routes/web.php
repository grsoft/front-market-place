<?php
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\supper_admin\adminLogin;
use App\Http\Controllers\admin\admin2Login;
use App\Http\Controllers\seller\sellerLogin;
use App\Http\Controllers\supper_admin\adminuser;
use App\Http\Controllers\supper_admin\scategory;
use App\Http\Controllers\supper_admin\banner;
use App\Http\Controllers\supper_admin\product;
use App\Http\Controllers\supper_admin\customer;
use App\Http\Controllers\supper_admin\seller;
use App\Http\Controllers\admin\a_product;
use App\Http\Controllers\seller\v_product;
use App\Http\Controllers\seller\v_order;
use App\Http\Controllers\admin\a_category;
use App\Http\Controllers\seller\v_category;
use App\Http\Controllers\front\cartControllers;
use App\Http\Controllers\front\pageControllers;
use App\Http\Controllers\front\customerControllers;
use App\Http\Controllers\front\paymentGetwayControllers;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::post('/seller/registration', [sellerLogin::class, 'registration']);
Route::get('/', [pageControllers::class, 'index']);
Route::get('/shop/detail/{id}', [pageControllers::class, 'detailspage']);
Route::get('/shop/category/{catname}/{scatname1?}/{scatname2?}/{cid?}', [pageControllers::class, 'categorypage1'])->name('categorypage1');
Route::get('/shop/order/{id}', [pageControllers::class, 'orderSummary']);
Route::get('/shop/orderlist', [pageControllers::class, 'orderList']);
Route::get('shop/seller', [pageControllers::class, 'seller_page']);

Route::get('/shop', function () {
    return view('front/index')->with('page', 'Home');
});
Route::post('/shop/shop-register-save', [customerControllers::class, 'cust_registration']);
Route::post('/shop/shop-customer-login', [customerControllers::class, 'customerLogin']);
Route::get('/shop/shop-customer-logout', [customerControllers::class, 'customerLogOut']);
Route::post('/shop/create_order', [customerControllers::class, 'create_order']);

Route::get('/shop/shop-register', [pageControllers::class, 'shop_register_page']);
Route::get('/shop/shop-login', [pageControllers::class, 'shop_login_page']);
Route::get('/shop/checkout', [pageControllers::class, 'checkout_page']);
Route::get('/shop/shoping_cart', [pageControllers::class, 'shoping_cart']);
Route::get('/shop/search', [pageControllers::class, 'shop_search_page']);

Route::post('/add-to-cart', [cartControllers::class, 'addtocart']);
Route::get('/load-cart-data', [cartControllers::class, 'cartloadbyajax']);
Route::get('/cart', [cartControllers::class, 'index']);
Route::post('/update-to-cart', [cartControllers::class, 'updatetocart']);
Route::post('/delete-from-cart', [cartControllers::class, 'deletefromcart']);
Route::get('/clear-cart', [cartControllers::class, 'clearcart']);
Route::post('/filter-left', [pageControllers::class, 'filter_left']);
Route::post('/payget-payment-response', [paymentGetwayControllers::class, 'paymentResponse']);
Route::post('/payget-payment-response-localhost', [paymentGetwayControllers::class, 'paymentResponseLocalhost']);
Route::post('/rating-add', [pageControllers::class, 'addtorating']);










Route::group(['middleware' => 'check_access'], function(){

    Route::get('/supper_admin', function () {
        if(session()->has('id'))
        {
            return redirect('/supper_admin/dashboard');
        }
        else{
            return redirect('supper_admin/index');
        }
        
    });
    Route::get('/supper_admin/index', function () {
        return view('supper_admin/index');
    });
    Route::get('/supper_admin/log_out', function () {
        session()->flush() ;
        if(session()->has('id'))
        {
            return redirect('/supper_admin/dashboard');
        }
        else{
            return redirect('supper_admin/index');
        }
        
    });

Route::post('/supper_admin/login', [adminLogin::class, 'adminLogin']);
Route::get('/supper_admin/dashboard', function () {
    return view('supper_admin/dashboard');
});
Route::get('/supper_admin/blank', function () {
    return view('supper_admin/blanck');
});
Route::any('/supper_admin/category/{action}/{id?}', [scategory::class, 'index']);
Route::post('/supper_admin/category-save', [scategory::class, 'category_save']);
Route::post('/supper_admin/category-edit', [scategory::class, 'category_edit']);
Route::post('/supper_admin/sub-category-save', [scategory::class, 'sub_category_save']);
Route::post('/supper_admin/sub-category-update', [scategory::class, 'sub_category_edit']);
Route::any('/supper_admin/product/{action}/{id?}', [product::class, 'index']);
Route::post('/supper_admin/product-save', [product::class, 'product_save']);
Route::post('/supper_admin/product-update', [product::class, 'product_update']);
Route::post('/supper_admin/product-step', [product::class, 'product_step']);
Route::get('/supper_admin/customer/{action}/{id?}', [customer::class, 'index']);
Route::get('/supper_admin/seller/{action}/{id?}', [seller::class, 'index']);
Route::get('/supper_admin/admin/{action}/{id?}', [adminuser::class, 'index']);
Route::post('/supper_admin/admin/admin-save', [adminuser::class, 'admin_save']);
Route::post('/supper_admin/admin/admin-update', [adminuser::class, 'admin_update']);
Route::get('/supper_admin/banner/{action}/{id?}', [banner::class, 'index']);
Route::post('/supper_admin/banner/home-banner-save', [banner::class, 'home_banner_save']);
Route::post('/supper_admin/banner/home-banner-update', [banner::class, 'home_banner_update']);
//Route::post('/supper_admin/product-step1', [product::class, 'product_step1']);
//Route::post('/supper_admin/process-brand', [product::class, 'update_brand']);
//Route::post('/supper_admin/process-item', [product::class, 'update_item']);
//Route::post('/supper_admin/product-step2', [product::class, 'product_step2']);

















//admin rout
    Route::get('/admin', function () {
        if(session()->has('aid'))
        {
            return redirect('/admin/dashboard');
        }
        else{
            return redirect('admin/index');
        }
        
    });
    Route::get('/admin/index', function () {
        return view('admin/index');
    });
    Route::get('/admin/log_out', function () {
        session()->flush() ;
        if(session()->has('aid'))
        {
            return redirect('/admin/dashboard');
        }
        else{
            return redirect('admin/index');
        }
        
    });

    Route::post('/admin/login', [admin2Login::class, 'adminLogin']);
    Route::get('/admin/dashboard', function () {
        return view('admin/dashboard');
    });
    Route::any('/admin/category/{action}/{id?}', [a_category::class, 'index']);
    Route::any('/admin/product/{action}/{id?}', [a_product::class, 'index']);
    Route::post('/admin/product-step', [a_product::class, 'product_step']);
    Route::post('/admin/product-save', [a_product::class, 'product_save']);
    Route::post('/admin/product-update', [a_product::class, 'product_update']);









//seller rout
    //admin rout
    Route::get('/seller', function () {
        if(session()->has('sid'))
        {
            return redirect('/seller/dashboard');
        }
        else{
            return redirect('seller/index');
        }
        
    });
    Route::get('/seller/index', function () {
        return view('seller/index');
    });
    Route::get('/seller/log_out', function () {
        session()->flush() ;
        if(session()->has('sid'))
        {
            return redirect('/seller/dashboard');
        }
        else{
            return redirect('seller/index');
        }
        
    });

    Route::post('/seller/login', [sellerLogin::class, 'sellerLogin']);
    Route::get('/seller/dashboard', function () {
        return view('seller/dashboard');
    });
    Route::get('/seller/qr-scaner', function () {
        return view('seller/v_qrcode_scan')->with('title','Scanner');
    });
    Route::any('/seller/category/{action}/{id?}', [v_category::class, 'index']);
    Route::any('/seller/product/{action}/{id?}', [v_product::class, 'index']);
    Route::post('/seller/product-step', [v_product::class, 'product_step']);
    Route::post('/seller/product-save', [v_product::class, 'product_save']);
    Route::post('/seller/product-update', [v_product::class, 'product_update']);
    Route::get('/seller/myorder', [v_order::class, 'myorder_all']);
    Route::get('/seller/delivered-order', [v_order::class, 'delivered_order_all']);
    Route::get('/seller/myorder/invoice/{invono}', [v_order::class, 'invoicePDF']);
    Route::get('/seller/myorder-view/{invono}', [v_order::class, 'invoiceView']);
    Route::get('/seller/order-action-save', [v_order::class, 'orderActionSave']);





});